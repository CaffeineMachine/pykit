# Pykit

Structured collection of common utilities and basic capabilities for python. (Alpha)

With broader usability needs in mind **Pykit**, as a fork of the [**Pyutils**](https://gitlab.com/CaffeineMachine/pyutils) lib, will use a simpler, more formal source file structure and a better commit process. 

## Tests

See test/test_pykit.ipynb.

## Creators

**Stanley Hailey**

- <http://caffeine-machine.com/>

## License
Released for free under the MIT license, which means you can use it for almost
any purpose (including commercial projects). Credit is appreciate but not required.

