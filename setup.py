from setuptools import setup

setup(name='pykit',
      version='0.0.1',
      description='Structured collection of common utilities and basic capabilities for python.',
      author='Lee Hailey',
      author_email='slhaile01@gmail.com',
      license='MIT',
      packages=['pykit'],
      zip_safe=False)