from __future__ import print_function
import itertools
# from colorama import Fore, Back, Style, init
import colorama as cm
from fabulous.color import blink, strike, underline
from dataclasses import dataclass


class DefsColorize:
	''' struct for all available color enumerations '''
	# available text colors
	names = ('black', 'white', 'red', 'magenta', 'blue', 'yellow', 'green', 'cyan')
	layer_names = (fore, back) = ('fore', 'back')
	shade_names = (normal, light) = ('normal', 'light')

	# generated from all template permutations of colors, layer={ForeG, BackG} and shades={Normal, Light}
	tags = {}						# generated abbreviated tag,colorama-value pairs like 'redl':Fore.LIGHTRED_EX, 'cyanb':Back.CYAN
	layers = {fore:{}, back:{}}		# generated same as tags but nested in a dict of groups_names
	shades = {normal:{}, light:{}}	# generated same as tags but nested in a dict of groups_names
	formatters = []					# generated python text format function (for reference only)
	debugging = False
	normal = debugging and 'normal' or cm.Fore.RESET
	normalb = debugging and 'normalb' or cm.Back.RESET
	reset_all = debugging and 'plain' or cm.Style.RESET_ALL

	@classmethod
	def init(cls, debug_tags=False):
		func_fmt = 'def {tag}(msg): return ({full}+msg).replace(Style.RESET_ALL, {full}) + Style.RESET_ALL'
		flag_fmts = (
			dict(shade=cls.normal, layer=cls.fore, tag_fmt=lambda c: c, full_fmt=lambda c: 'Fore.%s' % c.upper()),
			dict(shade=cls.normal, layer=cls.back, tag_fmt=lambda c: c+'b', full_fmt=lambda c: 'Back.%s' % c.upper()),
			dict(shade=cls.light, layer=cls.fore, tag_fmt=lambda c: c+'l', full_fmt=lambda c: 'Fore.LIGHT%s_EX' % c.upper()),
			dict(shade=cls.light, layer=cls.back, tag_fmt=lambda c: c+'lb', full_fmt=lambda c: 'Back.LIGHT%s_EX' % c.upper()),
		)

		# generate colors tags and function formatters
		for color, flag_fmt in itertools.product(cls.names, flag_fmts):
			shade, layer, tag, full = (
				flag_fmt['shade'], flag_fmt['layer'], flag_fmt['tag_fmt'](color), flag_fmt['full_fmt'](color))
			value = eval(full)

			cls.tags[tag] = value + (debug_tags and tag or '')
			cls.layers[layer][tag] = value
			cls.shades[shade][tag] = value
			cls.formatters += [func_fmt.format(tag=tag, full=full)]

		if debug_tags:
			cm.Style.RESET_ALL = cm.Style.RESET_ALL+'reset'
Defs = DefsColorize

# DefsColorize.init()


# text color formatter functions; wraps text with ansi code of given color
def color_fmt(msg, color): 	return color + str(msg) + Defs.reset_all



@dataclass(frozen=not Defs.debugging, repr=False)
class AnsiFmtr:
	code:str
	name:str = ''
	reset = Defs.reset_all
	# __slots__ = ['code', 'name']
	
	def __post_init__(self):
		if Defs.debugging:
			self.code = self.name
	def __repr__(self):
		return f'{self.__class__.__name__}({self.name or self.code})'
	def format(self, content):
		return f'{self.code}{str(content).replace(self.reset, self.code)}{self.reset}'
		# return f'{self.code}{str(content).replace(Defs.reset_all, self.code)}{Defs.reset_all}'
	
	# method aliases
	__call__ = format
	
class AnsiForeFmtr(AnsiFmtr): reset = Defs.normal
class AnsiBackFmtr(AnsiFmtr): reset = Defs.normalb

class PlainAnsiFmtr(AnsiFmtr):
	code:str = Defs.reset_all
	name:str = 'plain'
	def __init__(self):
		super().__init__(Defs.reset_all, 'plain')
		# self.code = self.name if Defs.debugging else self.code
	def format(self, content):
		return f'{self.code}{str(content)}'

_get_dark_code	= lambda layer,col: getattr(layer, f'{col.upper()}')
_get_light_code	= lambda layer,col: getattr(layer, f'LIGHT{col.upper()}_EX')

# _fore_dark_fmtr		= lambda col: AnsiFmtr(_get_dark_code(cm.Fore, col), f'{col}')
# _fore_light_fmtr	= lambda col: AnsiFmtr(_get_light_code(cm.Fore, col), f'light{col}')
# _back_dark_fmtr		= lambda col: AnsiFmtr(_get_dark_code(cm.Back, col), f'{col}bg')
# _back_light_fmtr	= lambda col: AnsiFmtr(_get_light_code(cm.Back, col), f'light{col}bg')
_fore_dark_fmtr		= lambda col: AnsiForeFmtr(_get_dark_code(cm.Fore, col), f'{col}')
_fore_light_fmtr	= lambda col: AnsiForeFmtr(_get_light_code(cm.Fore, col), f'light{col}')
_back_dark_fmtr		= lambda col: AnsiBackFmtr(_get_dark_code(cm.Back, col), f'{col}bg')
_back_light_fmtr	= lambda col: AnsiBackFmtr(_get_light_code(cm.Back, col), f'light{col}bg')


class Fore:
	class Dark:
		_all = black, white, red, magenta, blue, yellow, green, cyan = [*map(_fore_dark_fmtr, Defs.names)]
	class Light:
		_all = black, white, red, magenta, blue, yellow, green, cyan = [*map(_fore_light_fmtr, Defs.names)]
		
	# defaults
	normal = clear_col = AnsiFmtr(cm.Fore.RESET, 'normal')
	black, white, red, magenta, blue, yellow, green, cyan = Dark._all
	
class Back:
	class Dark:
		_all = black, white, red, magenta, blue, yellow, green, cyan = [*map(_back_dark_fmtr, Defs.names)]
	class Light:
		_all = black, white, red, magenta, blue, yellow, green, cyan = [*map(_back_light_fmtr, Defs.names)]
		
	# defaults
	normal = clear_col = AnsiFmtr(cm.Back.RESET, 'normal')
	black, white, red, magenta, blue, yellow, green, cyan = Dark._all


# aliases
plain = PlainAnsiFmtr()
normal, normalb = clear_col, clear_colb									= Fore.normal, Back.normal
black, white, red, magenta, blue, yellow, green, cyan					= Fore.Dark._all
blackl, whitel, redl, magental, bluel, yellowl, greenl, cyanl			= Fore.Light._all
blackb, whiteb, redb, magentab, blueb, yellowb, greenb, cyanb			= Back.Dark._all
blacklb, whitelb, redlb, magentalb, bluelb, yellowlb, greenlb, cyanlb	= Back.Light._all
gray, grayb, grayl, graylb = blackl, blacklb, white, whiteb


''' color samplers '''
def raw_color_test():
	# print(u"\x1b[30;1m A \x1b[31;1m B \x1b[32;1m C \x1b[33;1m D \x1b[34;1m E \x1b[35;1m F \x1b[36;1m G \x1b[37;1m H \x1b[0m")
	# print()
	print(u"\x1b[30m A \x1b[31m B \x1b[32m C \x1b[33m D \x1b[34m E \x1b[35m F \x1b[36m G \x1b[37m H \x1b[0m")
	print(u"\x1b[90m A \x1b[91m B \x1b[92m C \x1b[93m D \x1b[94m E \x1b[95m F \x1b[96m G \x1b[97m H \x1b[0m")
	print(u"\x1b[40m A \x1b[41m B \x1b[42m C \x1b[43m D \x1b[44m E \x1b[45m F \x1b[46m G \x1b[47m H \x1b[0m")
	print(u"\x1b[100m A \x1b[101m B \x1b[102m C \x1b[103m D \x1b[104m E \x1b[105m F \x1b[106m G \x1b[107m H \x1b[0m")

def color_sample_basic():
	print('color sample basic:')
	for layer_level in Fore.Dark, Fore.Light, Back.Dark, Back.Light:
		for ansi_fmtr in layer_level._all:
			print(ansi_fmtr(f'{ansi_fmtr.name:^{15}}'), end='')
		print()
	print()
	
	
def color_sample_swatch():
	print('color sample swatch:')
	import sys
	for i in range(0, 16):
		for j in range(0, 16):
			if not (i * 16 + j+2) % 6:
				print(u"\x1b[0m")
			code = u'\x1b[48;5;'+str(i * 16 + j)
			sys.stdout.write(code + "m " + repr(code).replace('\'', '').ljust(14))
	print(u"\x1b[0m\n")

# def strike(text):
# 	result = ''
# 	for c in text:
# 		result = result + c + '\u0336'
# 	return result
# def strike(text):
# 	return ''.join([u'\u0336{}'.format(c) for c in text])

# color_sample_basic()
# color_sample_swatch()
#
# CSI = '\033['
# blue_fore = '\033['
# Fore_BLUE_EX = CSI + '0;' + str(34) + 'm'
# Fore_LIGHTBLUE_EX = CSI + '1;' + str(34) + 'm'
# blue_back = '\044['
# Back_BLUE_EX = blue_back + '0;' + str(34) + 'm'
# # Back_BLUE_EX = '\e[44m' + str(34) + 'm'
# Back_LIGHTBLUE_EX = blue_back + '1;' + str(34) + 'm'
# # Fore_BLUE_EX = CSI + str(34) + 'm'
# c_blink = CSI + '5m'
#
# def bluel_(msg): 	return (Fore_LIGHTBLUE_EX+msg)		.replace(Style.RESET_ALL, Fore_LIGHTBLUE_EX) + Style.RESET_ALL
# def blue_(msg): 	return (Fore_BLUE_EX+msg)		.replace(Style.RESET_ALL, Fore_BLUE_EX) + Style.RESET_ALL
# def bluelb_(msg): 	return (Back_LIGHTBLUE_EX+msg+Style.RESET_ALL)
# def blueb_(msg): 	return (Back_BLUE_EX+msg+Style.RESET_ALL)
# def blink(msg): 	return (c_blink+msg)		.replace(Style.RESET_ALL, c_blink) + Style.RESET_ALL


# print(blink('foo'))
# print(bluelb_('foo'))
# print(blueb_('foo'))
# print(bluelb('foo'))
# print(blueb('foo'))
# print(cyanlb('foo'))
# print(cyanb('foo'))
# exit()

if __name__=='__main__':
	color_sample_basic()
	color_sample_swatch()
