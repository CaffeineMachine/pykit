from __future__ import print_function
import sys
import linecache
import re
# from pyutils.common.thread import *
import threading
from pprint import pprint as pp, pformat as pfmt
from pyutils.common.colorhighlight import *
from pyutils.common.evallist import *
from pyutils.utilities import srcinfoutils as tp



class TraceShow(Eval):
	fields = (
		event, file_name, module_name, class_name, func_name, line, code, thread_index, stack_depth ) = (
		'event', 'file_name', 'module_name', 'class_name', 'func_name', 'line', 'code', 'thread_index', 'stack_depth' )
	conditions = (
		contains, not_contains, either, neither, starts_with, ends_with, regex, eq, n_e, lt, l_e, gt, g_e) = range(13)
	''' (
		# sys.set_trace() routines are very sensitive to slow down
		# evaluating performance condition evaluation as lambda versus if else series
		# if else series does not push on to trace stack (very slow compared to add, multiply, or bool-op)
		
		lambda field, value: value in field,
		lambda field, value: value not in field,
		lambda field, value: field in value,
		lambda field, value: field not in value,
		lambda field, value: field.startswith(value),
		lambda field, value: field.endswith(value),
		lambda field, value: re.findall(value, field),
		lambda field, value: field == value,
		lambda field, value: field != value,
		lambda field, value: field < value,
		lambda field, value: field <= value,
		lambda field, value: field > value,
		lambda field, value: field >= value, )'''

	def __init__(self, field_name, condition, value, default_result=True):
		''' construct; TraceShow behavior reads like show line if <field_name> <condition> <value>.
			usage:
			TraceShow(code, 'val', contains).can_show('value = dict()')  => True
		'''
		Eval.__init__(self, default_result)
		self.field_name = field_name
		self.condition = condition
		self.value = value

	def can_show(self, event, file_name, module_name, class_name, func_name, line, code, thread_index, stack_depth):
		args = locals()
		result = self.default_result

		if self.field_name in args:
			# evaluating performance condition evaluation as lambda versus series of if else statements
			# result = self.condition(self.value, args[self.field])

			field = args[self.field_name]
			condition = self.condition
			
			if condition == TraceShow.contains:			result = self.value in field
			elif condition == TraceShow.not_contains:	result = self.value not in field
			elif condition == TraceShow.either:			result = field in self.value
			elif condition == TraceShow.neither:		result = field not in self.value
			elif condition == TraceShow.starts_with:	result = field.startswith(self.value)
			elif condition == TraceShow.ends_with:		result = field.endswith(self.value)
			elif condition == TraceShow.regex:			result = re.findall(self.value, field)
			elif condition == TraceShow.eq:				result = field == self.value
			elif condition == TraceShow.n_e:			result = field != self.value
			elif condition == TraceShow.lt:				result = field < self.value
			elif condition == TraceShow.l_e:			result = field <= self.value
			elif condition == TraceShow.gt:				result = field > self.value
			elif condition == TraceShow.g_e:			result = field >= self.value

		return result

	# method aliases
	eval = can_show


class TraceFormat(TraceShow):
	''' Virtually identical to TraceShow but modified to facilitate setting trace string reformatting '''
	@staticmethod
	def format(trace_line):
		result = green(trace_line)
		return result

	def __init__(self, field_name, condition, value, format_func=format):
		''' construct '''
		TraceShow.__init__(self, field_name, condition, value, default_result=format_func)



event_fmt = lambda trace: cyan('\n'+trace)


class TraceLog:
	_orig_trace = None
	_thread_indices = {}
	_thread_stack_activity = {}
	_trace_show_rules = None
	_path_sep = '/'

	@staticmethod
	def set_trace(rules=None, *, force=False, verbose=True):
		''' designate this module as trace callback handler '''
		# exit if nothing to do
		import sys
		if sys.gettrace() == TraceLog.on_trace:
			return

		# detect if callback is already defined and potentially exit
		callback = None
		for callback_getter in (sys.gettrace, sys.getprofile):
			callback = callback_getter()
			if callback is not None:
				if verbose:
					print(yellow('sys.%s() is already set to %s.\n%sOverriding.' % (callback_getter.__name__, callback, not force and 'NOT ' or '')))
				if not force:
					return False
	
		rules and TraceLog.set_show_rules(rules)

		# set trace callback to internal handler
		TraceLog._orig_trace = callback
		sys.settrace(TraceLog.on_trace)
		return True

	@staticmethod
	def on_trace(frame, event, arg):
		''' display the current frame if applicable based on configured _trace_show_rules '''

		try:
			# if event == 'line' or event == 'call':
			thread_id = threading.get_ident()
			thread_index = TraceLog._thread_indices.setdefault(thread_id, len(TraceLog._thread_indices)+1)
			if event == 'line' or event == 'call':
				module_name = frame.f_globals["__name__"]
				module_name = module_name.rsplit('.', 1)[-1]

				file_name = frame.f_code.co_filename
				# if (file_name.endswith(".pyc") or file_name.endswith(".pyo")):
				if file_name[-4:-1] == '.py' and file_name[-1] in 'co':
					file_name = file_name[:-1]  # change .pyc & .pyo extensions to .py

				func_name = frame.f_code.co_name
				class_name = 'cls?'
				clsobj = tp.clsobj(frame=frame)
				if clsobj:
					clspath = clsobj.__module__.replace('.', '/') + '.'
					class_name = clsobj.__name__ if clspath in file_name else 'cls?'
				line = frame.f_lineno
				code = linecache.getline(file_name, line)
				while code.strip().startswith('@'):
					line += 1
					code = linecache.getline(file_name, line)

				thread_count = threading.active_count()+1
				# return TraceLog.on_trace  # for benchmarking

				# count stack depth
				f = frame.f_back
				stack_depth = 1
				while f is not None:
					f = f.f_back
					stack_depth += 1
				
				# evaluate any trace_show_rules: if it evaluates to false return without displaying trace line
				# show_trace = 'dataindexer_models' in module_name
				if 'toggletimer' in file_name:
					print('.', end='')
				show_trace = TraceLog._trace_show_rules is None or (TraceLog._trace_show_rules.eval(
					event, file_name, module_name, class_name, func_name, line, code, thread_index, stack_depth))
				if not show_trace:
					return TraceLog.on_trace

				# format trace line from frame fields
				msg = '%2s %2s %2s %s:%s.%s#%s' % (thread_index, thread_count, stack_depth, module_name, class_name, func_name, line)
				msg = '%-50s|%s' % (msg, code.rstrip())
				msg = type(show_trace) == bool and bluel(msg) or show_trace(msg)
				print(msg)
				TraceLog._thread_stack_activity[thread_id] = ''

		except KeyError:	pass
		except:				raise

		return TraceLog.on_trace

	@staticmethod
	def get_show_rules():
		''' getter: trace_show_rules '''
		return TraceLog._trace_show_rules

	@staticmethod
	def set_show_rules(rules):
		''' setter: trace_show_rules '''
		TraceLog._trace_show_rules = rules
	
	# method alieases
	settrace = set_trace  # preserve convention
	

	# template show rules
	
	# template rules to show trace in pyutils modules
	_show_pyutils = EvalAnd(
		TraceShow(TraceShow.file_name,		TraceShow.contains,	'pyutils'),  )
	
	# template rules to show trace in pyutils modules
	_show_entry = EvalAnd(
		TraceShow(TraceShow.event, 			TraceShow.eq,	'call'),  )
	
	# template rules to show trace in pyutils modules
	_show_pyutils_entry = EvalAnd(
		TraceShow(TraceShow.event, 			TraceShow.eq,	'call'),
		TraceShow(TraceShow.file_name,		TraceShow.contains,	'pyutils'),  )
	
	# template rules to show trace in local workspace excluding pyutils library
	_hide_pyutils = EvalAnd(
		TraceShow(TraceShow.file_name, 		TraceShow.not_contains,	'/workspace'),
		TraceShow(TraceShow.file_name,		TraceShow.not_contains,	'pyutils'),  )

	# template rules to show func calls in local workspace
	_show_local_calls = EvalAnd(
		TraceShow(TraceShow.file_name, 		TraceShow.contains,	'/workspace'),
		TraceShow(TraceShow.event, 			TraceShow.eq,	'call'),  )

	# # override with a preconfigured template
	_trace_show_rules = _show_entry



