import re
from dataclasses import dataclass

from pyutils.defs.worddefs import relation
from pyutils.output.joins import DefsJoins
from pyutils.utilities.fmtutils import *
from pyutils.utilities.iterutils import *



def repr_obj(obj):
	result = DefsJoins.jjobj_fmt(obj.__dict__.items())
	result = result.format(obj.__class__.__name__)
	return result

def repr_anno(obj):
	obj_rels =  relation.__get__(obj)
	result = DefsJoins.jjobj_fmt(lseq(obj.__annotations__.keys(), obj_rels))
	result = result.format(obj.__class__.__name__)
	return result


class FStr(str):
	''' Assigns str.format as default call operation '''
	
	# def __new__(cls, str_fmt, *pa, **ka):
	# 	obj = str(str_fmt)
	# 	return obj
	
	# method aliases
	__call__ = str.format

class ArgFstr(FStr):
	''' Assigns format as default call operation and optionally mutates format args '''
	
	coerce_pa_ka = None
	
	def __init__(self, fmt_str, coerce_pa_ka=None):
		if coerce_pa_ka:
			self.coerce_pa_ka = coerce_pa_ka
		super().__init__(fmt_str, coerce_pa_ka)
	def format(self, *pa, **ka):
		try:
			if self.coerce_pa_ka:
				pa, ka = self.coerce_pa_ka(*pa, **ka)
			result = str.format(*pa, **ka)
		except Exception as e:
			e = e.__class__(*e.args, self, pa, ka)
			raise e
			
		return result

	# method aliases
	__call__ = format

@dataclass
class Fstr:
	# obsoleted by ArgFstr
	fmt_str: str
	coerce_pa: callable = None
	coerce_ka: callable = None
	
	def format(self, pa=None, ka=None):
		# todo: passing pa and ka unpacked is unconventional and should not be default behavior
		try:
			pa = self.coerce_pa(pa) if pa and self.coerce_pa else pa	# coerce if pa and func eval True
			pa = (pa,) if isinstance(pa, str) else pa
			ka = self.coerce_ka(ka) if ka and self.coerce_ka else ka	# coerce if ka and func eval True
			result = self.fmt_str.format(*(pa or []), **(ka or {}))
		except Exception as e:
			e = e.__class__(*e.args, self.fmt_str, pa, ka)
			raise e
			
		return result

	# method aliases
	__call__ = format
