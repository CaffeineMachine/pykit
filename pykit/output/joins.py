from dataclasses import dataclass
from pyutils.structures.strs import *
from pyutils.utilities.iterutils import *

__all__ = Strs('Join JoinStrs ReprJoin Joins Unpack DefsJoins')

class Join:
	''' Define a str.join transform on a sequence. Applies delim as separator, and apply open and close to each end.
		Coerces each item to a str by default.
	'''
	to_str = str
	
	def __init__(self, delim=None, open=None, close=None, *, sep=None, esc_fmt=False, to_str=nothing):
		''' ctor
			:param delim:
			:param open:
			:param close:
			:param sep:
			:param esc_fmt:
			:param to_str:
		'''
		if sep is not None:
			if delim or open or close:
				raise ValueError('Ambiguous arguments. sep and any in {delim, open, close}')
			sep = StrsFrom(sep)
			delim, open, close, *_ = sep + (['']*3)
		self.delim = ', ' if delim is None else delim
		self.open = '' if open is None else open
		self.close = '' if close is None else close
		self.esc_fmt = esc_fmt
		to_str is not nothing and setattr(self, 'to_str', to_str)
	def join(self, sequence):
		if self.to_str:
			sequence = list(sequence)
			sequence = (self.to_str(item) for item in sequence)
		result = ''.join([self.open, self.delim.join(sequence), self.close])
		if self.esc_fmt:
			result = result.replace('{', '{{').replace('}', '}}')
		return result
	def __iter__(self): return iter([self.open, self.delim, self.close])
	def format(self, format_str=None):
		result = f'{self.open}0{self.delim}1{self.close}'
		if format_str:
			result = '{:{}}'.format(result, format_str)
		return result
	def __repr__(self):
		result = f'{self.__class__.__name__}({str(self)!r})'
		return result
	
	# method aliases
	__call__ = join
	__str__ = __format__ = format
	
	# propeties
	__name__ = property(repr)
	
def JoinSep(sep, esc_fmt=False, to_str=nothing):
	return Join(sep=sep, esc_fmt=esc_fmt, to_str=to_str)


''' Define a str.join transform on a sequence. Applies delim as separator, and apply open and close to each end.
	Removes the default str coercion for each item making the default options slightly more performant.
	Identical to Join(..., to_str=None)
'''
class JoinStrs(Join):	to_str = None
class ReprJoin(Join):	to_str = repr


class Joins:
	''' Join any dggree of nexted iterators '''
	def __init__(self, *join_args:str, esc_fmt_index=False):
		''' ctor
			:param join_args: each join arg. any degree of nested iterators can be joined given enough join_args
			:param esc_fmt_index: join index at which to apply string format inputs, when True format last join
		'''
		self.joins = [JoinSep(join_arg) for join_arg in join_args]
		if esc_fmt_index or esc_fmt_index is 0:
			esc_fmt_index = esc_fmt_index is True and -1 or esc_fmt_index
			self.joins[esc_fmt_index].esc_fmt = True
	def __repr__(self):
		result = f'{self.__class__.__name__} {xseq(self.joins_strs, Defs.jtuple, [repr, str])}'
		return result
	def join(self, sequence, *fmt_args, **fmt_kargs):
		result = xseq(sequence, *self.joins)
		if fmt_args and fmt_kargs:
			result = result.format(*fmt_args, **fmt_kargs)
		return result
	
	# method aliases
	__call__ = join
	
	# properties
	__name__ = property(repr)

class Formatted:
	def __iter__(self, str_fmtr=None, repr_fmtr=None):
		self.str_fmtr = str_fmtr or repr_fmtr
		self.repr_fmtr = repr_fmtr or str_fmtr
	def __str__(self): pass
	def __repr__(self): pass
	

@dataclass
class Unpack:
	op: callable
	def unpack(self, args=None, kws=None):
		result = self.op(*(args or []), **(kws or {}))
		return result
	__call__ = unpack


class DefsJoins:
	j_comma		= Join(',')
	j_dot		= Join('.')
	j_space		= Join(' ')
	j_tab		= Join('\t')
	j_lines		= Join('\n')
	
	j_values	= Join(', ')
	j_tuple		= Join(', ', '(', ')')
	j_list		= Join(', ', '[', ']')
	j_set		= Join(', ', '{', '}')
	j_vector	= Join(',', '<', '>')
	
	j_valuesv	= Join(',\n')
	j_tuplev	= Join(',\n', '(', ')')
	j_listv		= Join(',\n', '[', ']')
	j_setv		= Join(',\n', '{', '}')
	j_vectorv	= Join(',\n', '<', '>')
	
	jj_keyargs	= Joins(', ', '=')
	jj_dict		= Joins('_, _dict(_)', '=')
	jj_obj_fmt	= Joins('_, _{}(_)', '=')
	
	jj_keyvals	= Joins(', ', ':')
	jj_map		= Joins('_, _{_}', ':')
	
	# aliases
	j_items		= j_values
	j_pargs		= j_values
	j_words		= j_space
	

# old aliases were jvalues without an underscore
for key, cls_mbr in [*DefsJoins.__dict__.items()]:
	if 'j_' in key:
		alias = key.replace('j_', 'j', 1)
		# DefsJoins.__dict__[alias] = cls_mbr
		setattr(DefsJoins, alias, cls_mbr)
		
Defs = DefsJoins
