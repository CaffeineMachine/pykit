import yaml
from yaml.representer import Representer
from yaml.constructor import Constructor
from copyreg import __newobj__
import json
import builtins
from dataclasses import dataclass
from pyutils.utilities.iterutils import keepfirst, xseq, lseq
from pyutils.defs import *


def reduce(obj, priorities='todo'):
	''' selects an idiomatic reduce method applicable to the instance with the highest given priority '''
	priorities = ['_reduce_encode', '__getnewargs_ex__', '__getnewargs__', '__getstate__', '__reduce_ex__', '__reduce__', '__dict__']
	reduce_method = keepfirst(lseq(priorities, lambda name: getattr(obj, name, None)))
	reduce_args = reduce_method and (reduce_method.__name__ == '__reduce_ex__' and [0] or [])
	data = reduce_method(*reduce_args)
	return data


class YamlDumper(Representer):
	def represent_any(self, data):
		if typelike(data, YamlCoder):
			if data._base_yaml_coder:
				result = self.represent_object(data)
			else:
				data_reduced = reduce(data)
				result = self.represent_data(data_reduced)
		elif data.__class__.__name__ == 'CatalogueSelection':
			result = self.represent_list(data)
		else:
			result = self.represent_object(data)
		return result
	
class UniformYamlDumper(Representer):
	def represent_any(self, data):
		try:
			data_reduced = reduce(data)
			result = self.represent_data(data_reduced)
		except:
			result = self.represent_object(data)
		return result
	
class YamlLoader(Constructor):
	def construct_python_object(self, suffix, node):
		deep = True  # hasattr(cls, '__setstate__')
		state = self.construct_mapping(node, deep=deep)
		instance = self.make_python_instance(suffix, node, kwds=state)
		yield instance


@dataclass
class YamlCoder:
	file_path: str = None
	
	_base_yaml_coder = True
	
	''' IN from file methods '''
	
	def ctor_args(self, file_path):
		args, kargs = self.read(file_path)  # todo
		return args, kargs
	@classmethod
	def deserialize(cls, file_path):
		data = cls.read(file_path)
		result = cls.coerce(data)
		return result
	@staticmethod
	def read(file_path):
		with open(file_path, 'r') as file_stream:
			data = yaml.load(file_stream)
		return data
	@classmethod
	def coerce(cls, obj):
		if typelike(obj, cls): return obj
		
		args, kargs = obj, {}
		if isinstance(obj, list) and len(obj) and clslike(obj[0], cls):
			_, args, kargs, *_ = obj + [{}]

		result = cls(*args, **kargs)
		return result
	
	
	''' OUT to file methods '''
	
	def serialize(self, file_path=None):
		file_path = file_path or self.file_path
		# data = reduce(self)
		return self.write(self, file_path)
	@staticmethod
	def write(data, file_path):
		with open(file_path, 'w') as file_stream:
			result = yaml.dump(data, file_stream)
		return result
	
	# method aliases
	load, save = deserialize, serialize
	
	# @staticmethod
	# def represent_any(rep, data):
	# 	if data.__class__.__name__ == 'CatalogueSelection':
	# 		result = rep.represent_list(data)
	# 	else:
	# 		result = rep.represent_object(data)
	# 	return result

class YamlCoderElement(YamlCoder):
	''' Implies object is an element (or subcomponent) of a YamlCoder which provides context
		for serialization.  Derives type info from owner which allows omission of an enclosing tag.
	'''
	_base_yaml_coder = False
	
def register_yaml_coders():
	Representer.add_multi_representer(object, YamlDumper.represent_any)
	Constructor.add_multi_constructor(
		'tag:yaml.org,2002:python/object:',
		YamlLoader.construct_python_object)
	
def register_yaml_coders_uniform():
	Representer.add_multi_representer(object, UniformYamlDumper.represent_any)
	Constructor.add_multi_constructor(
		'tag:yaml.org,2002:python/object:',
		YamlLoader.construct_python_object)
register_yaml_coders()

class PyJsonCoder(json.JSONEncoder):
	def __init__(self, *args, globals_map=None, **kws):
		self.globals_map = globals_map
		super().__init__(*args, **kws)
		
	def _encode(self, obj):
		try:
			return json.JSONEncoder.default(self, obj)
		except:
			result = obj
			# print(f'obj={repr(obj)}')
			if isinstance(obj, bytes):
				result =  {'__type__': 'bytes','value': list(obj)}
			elif hasattr(obj, '__getstate__'):
				# print('__getstate__')
				result = obj.__getstate__()
				result['__type__'] = obj.__class__.__name__
			elif hasattr(obj, '__reduce__') or hasattr(obj, '__reduce_ex__'):
				if hasattr(obj, '__reduce_ex__'):
					# print('__reduce_ex__')
					result = list(obj.__reduce_ex__(0))
				elif hasattr(obj, '__reduce__'):
					# print('__reduce__')
					result = list(obj.__reduce__())
				
				if result[0].__name__ == '_reconstructor':
					result[0] = result[1][0].__name__
					result[1] = None
				if isinstance(result[0], type):
					result[0] = result[0].__name__
				result = {'__reduced__': result}
			elif hasattr(obj, '__dict__'):
				# print('__dict__')
				result = obj.__dict__
				result['__type__'] = obj.__class__.__name__

			else:
				raise ValueError()
		# print(f'return {result}')
		return result
	
	def _decode(self, obj):
		# todo: refactor to eliminatee redundancies
		# print(obj)
		globals_map = self.globals_map or globals()
		result = obj
		if '__reduced__' in obj:
			# print(obj)
			obj = obj.pop('__reduced__')
			__type__ = obj[0]
			if hasattr(builtins, __type__):
				ctor = getattr(builtins, __type__)
				result = ctor(obj['value'])
			else:
				ctor = globals_map[__type__]
				result = ctor(*(obj[1:2] and obj[1] or []), **(obj[2:3] and obj[2] or {}))
		elif '__type__' in obj:
			__type__ = obj.pop('__type__')
			if hasattr(builtins, __type__):
				ctor = getattr(builtins, __type__)
				result = ctor(obj['value'])
			else:
				ctor = globals_map[__type__]
				# print(__type__)
				# print(obj)
				result = ctor(**obj)
		return result
	
	# method aliases
	default = _encode

