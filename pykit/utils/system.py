def is_jupyter_runtime():
	import os
	from pyutils.extend.datastructures import XDict
	
	result = XDict.like(os.environ, 'JUPYTER_PATH|JPY_PARENT_PID')  # check for 2 known jupyter env vars
	return bool(result)


if __name__=='__main__':
	result = is_jupyter_runtime()
	print(f'is_jupyter_runtime() == {is_jupyter_runtime()}')
	assert(result)