import itertools as it
import re

from pyutils.defs import *



__all__excludes__ = 'op it re log Defs'


chainit = it.chain.from_iterable
cycle = it.cycle
repeat = it.repeat

log = None
# log = print
if log:
	from pprint import pprint as pp
__dbg__ = False
# __dbg__ = True


def iobj(obj, limit=None):
	''' generate iterable of multiple *obj*. '''
	while True:
		if limit == 0:	break
		yield obj
		limit = limit and limit-1

def igen(obj, call=True, limit=None):
	''' generate iterable of multiple *obj*(). '''
	if not call: return iobj(obj, limit)
	
	while True:
		if limit == 0:	break
		yield obj()
		limit = limit and limit-1

class DefsIter:
	list_to_dict = lambda obj: {'[0]':obj[0]} if obj and type(obj) is list else obj
	dict_unzip = lambda d: [list(d.keys()), list(d.values())]
	dict_rezip = lambda d: list(zip(*d))
	
	class getter:
		default	= None
		key		= lambda item: item[0]
		value	= lambda item: item[1]
		item	= lambda item: item
		
		@classmethod
		def coerce(cls, getter):
			if isinstance(getter, str):
				getter = getattr(cls, getter)
			return getter
		
	# const defines for (rep)eat functions
	repinfargs = repinf, repinfind = 'inf', -1		# repeat args representing infinity
	repstarargs = repstar, repstarind = '*', -2		# repeat args representing unpacked sequence
	repargs = *repinfargs, *repstarargs
	
Defs = DefsIter


def xseq(seq, cast, *to_items, all_items=None, pre_all_items=None, nondiv=None, nested=False):
	''' Apply a set of functions or transforms to each layer of iterators.
		Is capable of processing any nested structure of iterables.
		:param seq: the sequence(s) of any depth on which to operate.
		:param cast: transform applied to top level iterator last but before resulting items are returned.
			None or ... behaves as a noop for current layer returning a generator of the iterator at current depth.
			In the case of None or ... subsequent to_items are applied as needed to nested items.
		:param to_items: sequence of transforms to apply to each nested iterator item. len(to_items) determines depth.
			(cast & to_items) is a single sequence of transforms applied to current & recursive scopes respectively.
		:param to_item_all: functions applied to each nested iterator item before item_ops. DOES NOT influence depth.
		:param nested: False if called by user. True if in recursive call of xseq.
	'''
	# get result of available to_items transforms on nested item iterator first
	items, pre_to_items = seq, pre_all_items
	if pre_to_items:
		items  = pre_to_items(items)
		
	if to_items:  # and (nondiv and not isinstance(items, nondiv)) and hasattr(items, '__iter__'):
		kws = dict(all_items=all_items, pre_all_items=pre_all_items, nondiv=None, nested=True)
		items  = [xseq(item, *to_items, **kws) for item in items]
	
	# prepare order of transforms to apply to current item iterator
	casts = []
	if cast not in (None, ..., 0):
		(type(cast) in (tuple, list) and casts.extend or casts.append)(cast)
	if all_items not in (None, ..., 0):
		(type(cast) in (tuple, list) and casts.extend or casts.append)(all_items)
		
	# apply ordered transforms to seq iterator
	for cast_n in reversed(casts):
		items = cast_n(items)

	return items
dseq, kseq, lseq, tseq = gen_coll_wraps(xseq, arg=1)
iseq = lambda seq, *pa, **ka: xseq(seq, iter, *pa, *ka)
lSeq = lseq  # lSeq is an obsolete function name

def ikeys(mapping, as_item=None, as_obj=None, unpack=False):
	if unpack and as_item:
		result = (as_item(*k) for k in mapping.keys())
	else:
		result = (k if not as_item else as_item(k) for k in mapping.keys())
	result = result if not as_obj else as_obj(result)
	return result
dkeys, _, lkeys, tkeys = gen_coll_wraps(ikeys, arg='as_obj')

def ivalues(mapping, cast=None, to_item=None, *to_items):
	result = items = mapping.values()
	if to_item and hasattr(to_item, '__call__') and not to_items:
		result = (to_item(k_v) for k_v in items)
	else:
		result = (xseq(k_v, to_item, *to_items) for k_v in items)
	result = result if not cast else cast(result)
	return result
dvalues, kvalues, lvalues, tvalues = gen_coll_wraps(ivalues, arg=1)

def iitems(mapping, cast=None, to_item=None, *to_items):
	result = items = mapping.items()
	if to_item and hasattr(to_item, '__call__') and not to_items:
		result = (to_item(k_v) for k_v in items)
	else:
		result = (xseq(k_v, to_item, *to_items) for k_v in items)
	result = result if not cast else cast(result)
	return result
ditems, kitems, litems, titems = gen_coll_wraps(iitems, arg=1)


def xstrs(seq, cast, *ops, **kargs):	return xseq(seq, cast, *ops, str, **kargs)
def istrs(seq, *ops, **kargs):			return xseq(seq, iter, *ops, str, **kargs)
dstrs, kstrs, lstrs, tstrs = gen_coll_wraps(istrs)


ire_to_found_group	= lambda item, found, *pa, **ka: found.group(*pa, *ka)
ire_to_item			= lambda item, found, *pa, **ka: item

def ire(pattern, seq, matches=True, to_str=str, to_item=None, *pa, **ka):
	to_item = to_item or (matches and ire_to_found_group or ire_to_item)
	cond = re.compile(pattern) if isinstance(pattern, str) else pattern
	for item in seq:
		item_str = to_str(item) if to_str else item
		found = cond.search(item_str)
		if found and matches or (not found and matches is False):
			item = to_item(item=item, found=found, *pa, **ka) if to_item else item
			yield item
kre, lre, tre = gen_coll_wraps(ire, colls=klt_colls)

class _IDebug:
	def __init__(self, mod=None, output=print):
		self.mod = mod
		self.output = output
	def __call__(self, item):
		self.output(item)
		if self.mod:
			item = self.mod(item)
		return item
idebug = _IDebug()



def imap__(keys, mapping, cast=None, getter=None, to_item=None, to_value=None):
	'''
		:param keys:
		:param mapping:
		:param getter:
		:param cast:
		:param to_item:
		:param to_value:
	'''
	keys = isinstance(keys, str) and keys.split(' ') or keys

	if isinstance(getter, str):
		getter = getattr(mapping, getter)
	elif getter is None and not hasattr(mapping, '__getitem__'):
		getter = getattr(mapping, '__call__', None) or getattr(mapping, '__getattribute__')
	
	if getter:
		items = (getter(key) for key in keys)
	else:
		items = (mapping[key] for key in keys)
	
	# coerce each value item to appropriate type
	if type(to_value) in (tuple, list):
		for to_value_n in reversed(to_value):
			items = (to_value_n(value) for key, value in items)
	elif to_value is not None:
		items = (to_value(value) for key, value in items)
	
	# coerce each key_value item to appropriate type
	if type(to_item) in (tuple, list):
		items = zip(keys, items)
		for to_item_n in reversed(to_item):
			items = (to_item_n(item) for item in items)
	elif to_item is item_item:
		items = zip(keys, items)  # skip extra work for identity function
	elif to_item is not None:
		items = (to_item(item) for item in zip(keys, items))
		
	# coerce items sequence to appropriate type
	if type(cast) in (tuple, list):
		for cast_n in reversed(cast):
			items = cast_n(items)
	elif cast is not None:
		items = cast(items)
		
	return items

def imap(keys, mapping, getter=None, to_item=None, to_value=None, on_miss=None, cast=None):
	''' get items in order of given *keys* from a *mapping*
		:param keys:
		:param mapping:
		:param getter:
		:param to_item:
		:param to_value:
		:param on_miss:
		:param cast:
	'''
	keys = isinstance(keys, str) and keys.split(' ') or keys

	# select correct callable mapping
	if isinstance(getter, str):
		mapping  = getattr(mapping, getter)
	elif getter is not None:
		mapping  = getter(mapping)
	elif hasattr(mapping, '__getitem__') and not hasattr(mapping, '__instancecheck__'):  # ignore classes w/ getitem
		mapping  = getattr(mapping, '__getitem__')
	else:
		mapping  = getattr(mapping, '__call__', None) or getattr(mapping, '__getattribute__')
	
	# generate values iterable for given keys
	def handle_getkeys(keys, mapping, to_item, to_value, on_miss):
		for key in keys:
			try:
				item = mapping(key)
				# coerce each value item to appropriate type
				if type(to_value) in (tuple, list):
					for to_value_n in reversed(to_value):
						item = to_value_n(item)
				elif to_value is not None:
					item = to_value(item)
				
				# coerce each key_value item to appropriate type
				if type(to_item) in (tuple, list):
					item = (key, item)
					for to_item_n in reversed(to_item):
						item = to_item_n(item)
				elif to_item is not None:
					item = to_item((key, item))
					
				yield item
			except Exception as e:
				if on_miss is None: raise
				elif on_miss is 'continue': continue
				yield on_miss(key)

	items = handle_getkeys(keys, mapping, to_item, to_value, on_miss)
	
	# coerce items sequence to appropriate type
	if cast is not None:
		items = cast(items)
		
	return items
dmap, kmap, lmap, tmap = gen_coll_wraps(imap, arg='cast')

def imapvalues(*pa, **ka):				return imap(*pa, getter='__getitem__', **ka)
def imapitems(*pa, to_item=asis, **ka):	return imap(*pa, getter='__getitem__', to_item=to_item, **ka)
def imapmbrs(*pa, **ka):				return imap(*pa, getter='__getattribute__', **ka)
def imapvars(*pa, to_item=asis, **ka):	return imap(*pa, getter='__getattribute__', to_item=to_item, **ka)


def keep(seq, cond=bool, cast=None):
	result = (item for item in seq if cond(item))
	result = result if cast is None else cast(result)
	return result
ikeep, dkeep, kkeep, lkeep, tkeep = keep, *gen_coll_wraps(keep)

def skip(seq, cond=bool, invert=True):
	result = (item for item in seq if not cond(item))
	return result
iskip, dskip, kskip, lskip, tskip = skip, *gen_coll_wraps(skip)

def keepfirst(seq, cond=bool, default=arg_missing, invert=True):
	for item in seq:
		if cond(item):
			return item
	if default is not arg_missing:
		return default
	raise ValueError()

def keeplast(seq, cond=bool, default=arg_missing, invert=True):
	return keepfirst(reversed(seq), cond, default)

class Keep:
	invert = False
	def __init__(self, cond, cast=None):
		self.cond = cond
		self.cast = cast
	def __call__(self, seq):
		items = (item for item in seq if self.cond(item) ^ self.invert)
		if self.cast:
			items = self.cast(items)
		return items
	
class Skip(Keep):
	invert = True




def keepifs(seq, *cond_args, keepmulti=any):
	for multi in seq:
		each_eval = [cond(item) for item, cond in zip(multi, cond_args)]
		if keepmulti(each_eval):
			yield multi
def keepifall(seq, *cond_args):	return keepifs(seq, *cond_args, keepmulti=all)
def keepifany(seq, *cond_args):	return keepifs(seq, *cond_args, keepmulti=any)
def skipifall(seq, *cond_args): return keepifs(seq, *cond_args, keepmulti=not_all)
def skipifany(seq, *cond_args): return keepifs(seq, *cond_args, keepmulti=not_any)




''' Nesting Iterators
'''

def walk(seq, to_items=None, least=0, limit=None, conf='vals', src_key=None):
	''' iterate over all nested values in *items*
		:param seq: container on which to walk all nested child *items*
		:param to_items: get *child* iterator of *seq* container
		:param least: minimum walk depth
		:param limit: maximum walk depth; when given None walk depth is unlimited
		:param conf: which part of the nested structure to walk; accepts: 'vals', 'seq', 'seqvals', 'valsseq'
		:param src_key: when evaluates as True yield nested items as (src_key, value); otherwise yield value
	'''
	# ensure depth *limit* is not exceeded
	limit = limit-1 if isinstance(limit, int) else limit
	if limit is 0: return
	
	# get next level of child items; exceptions are interpreted as val or childless item
	try:
		items = iter(seq if to_items is None else to_items(seq))
	except:
		if 'vals' in conf:
			yield seq if src_key is None else (src_key, seq)
		return
	
	# handle iter('a') => 'a' infinite recursion
	item = next(items, nothing)
	if item in (items, nothing): return
	
	# yield parent *seq* if *conf* includes it
	seq_last = ()
	if least <= 0 and conf.startswith('seq'):
		yield seq if src_key is None else (src_key, seq)
	elif least <= 0 and conf.endswith('seq'):
		seq_last = (seq if src_key is None else (src_key, seq),)
	least -= 1
	
	# walk deeper on each item in items
	src_key = src_key if src_key is None else seq
	yield from walk(item, to_items, least, limit, src_key, conf)
	for item in items:
		yield from walk(item, to_items, least, limit, src_key, conf)
	yield from seq_last
iwalk, dwalk, kwalk, lwalk, twalk = walk, *gen_coll_wraps(walk)

def walk_flat(content, getter=Defs.getter.item, to_dict=None, ctx_key=''):
	''' obsolete transition to walk '''
	if to_dict:
		content = to_dict(content)
	getter = Defs.getter.coerce(getter)
	ctx_key = (ctx_key and not ctx_key.endswith('.')) and (ctx_key+'.') or ctx_key
	
	for key, value in content.items():
		if to_dict:
			value = to_dict(value)
		if value and isinstance(value, dict):
			yield from walk_flat(value, getter, to_dict=to_dict, ctx_key=ctx_key+key)
		elif getter:
			yield getter((ctx_key + key, value))
		else:
			yield (ctx_key + key, value)
   
def walk_tree(content, getter=Defs.getter.item, to_dict=None):
	''' obsolete transition to walk '''
	if to_dict:
		content = to_dict(content)
	getter = Defs.getter.coerce(getter)
	
	for key, value in content.items():
		if to_dict:
			value = to_dict(value)
		if value and isinstance(value, dict):
			yield list(walk_tree(value, getter, to_dict=to_dict))
		elif getter:
			yield getter((key, value))
		else:
			yield (key, value)



def accum(seq, op, init=nothing):
	items = iter(seq)
	result = next(items) if init is nothing else init
	for item in items:
		result = op(result, item)
		yield result
		
def accumstr(seq, op=str.__add__, init=''):
	return accum(seq, op, init)

def get_path_parts(path, path_sep='/'):
	part_ptrn = r'[^{sep}]*{sep}+|[^{sep}]+{sep}*'.format(sep=path_sep)
	parts = re.findall(part_ptrn, path)
	return parts
	
def trace_path(path):
	''' frace filesystem path from root down to target suppath '''
	path_parts = get_path_parts(path)
	return accumstr(path_parts)
	
def retrace_path(path):
	''' refrace filesystem path from target path up to root '''
	while True:
		yield path
		path = re.sub(r'([^/]*/+|[^/]+/*)$', '', path)
		if path == '':
			break

''' Converter Iterators
'''

# todo: RELOCATE to pyutils.format
class Indent(str):
	def indent(self, obj, count=1):
		result = f'{self*count}{obj}'
		return result
	__call__ = indent
class IndentLines(str):
	def indent(self, text:str, depth=1, debug=''):
		prefix = str(debug)+self*depth
		result = (prefix+text).replace('\n', '\n'+prefix)
		return result
	def indent_seq(self, seq:list, depth=1):
		prefix = self*depth
		result = '\n'.join(map(str, seq))
		result = prefix+result.replace('\n', '\n'+prefix)
		return result
	__call__ = indent
	
spaced = IndentLines(' ')
spaced2 = IndentLines('  ')
spaced4 = IndentLines('    ')
tabbed = IndentLines('\t')


def rconvert(seq, to_obj=None, to_item=None, to_iter=None, get_iter=None, is_atomic=None, depth=None):
	''' a recursive converter '''
	if ((is_atomic and isinstance(is_atomic, (type, list, tuple, set)) and isinstance(seq, is_atomic))
	or (is_atomic and not is_atomic(seq))
	or (type(seq) is str and len(seq) == 1)):
		return seq
	
	try:
		items = seq
		if get_iter:
			items = getattr(items, get_iter)
		if to_iter:
			iter_func = getattr(items, to_iter) if type(to_iter) is str else to_iter
			items = iter_func()
	except:
		return seq
	
	ka = dict(depth=depth+1) if isinstance(depth, int) else {}
	if to_item:
		items = (to_item(rconvert(i, to_obj, to_item, to_iter, get_iter, is_atomic, **ka)) for i in items)
	else:
		items = (rconvert(i, to_obj, to_item, to_iter, get_iter, is_atomic, **ka) for i in items)
		
	if to_obj:
		items = to_obj(items, **ka)
	return items
def rvalues(*pa, to_iter='values', **ka):
	return rconvert(*pa, to_iter=to_iter, **ka)
def ritems(*pa, to_iter='items', **ka):
	return rconvert(*pa, to_iter=to_iter, **ka)


def iremap(mapping, to_key=asis, to_value=asis, to_item=None, to_iter=dict.items, to_obj=None):
	''' converter modifies each key-value-pair of *mapping* with transforms *to_item*((*to_key*(k), *to_value*(v))) '''
	items = to_iter(mapping)
	if to_item:
		items = (to_item((to_key(key), to_value(value))) for key, value in items)
	else:
		items = ((to_key(key), to_value(value)) for key, value in items)
	if to_obj:
		items = to_obj(items)
	return items

def irekey(mapping, to_key, *pa, **ka):		return iremap(mapping, *pa, to_key=to_key, **ka)
def ireeval(mapping, to_value, *pa, **ka):	return iremap(mapping, *pa, to_value=to_value, **ka)

dremap, kremap, lremap, tremap = gen_coll_wraps(iremap, arg='to_obj')
drekey, krekey, lrekey, trekey = gen_coll_wraps(irekey, arg='to_obj')
dreeval, kreeval, lreeval, treeval = gen_coll_wraps(ireeval, arg='to_obj')
remap, rekey, reeval = dremap, drekey, dreeval


''' Construct Iterables
'''

def isegments(iterable, lengths, repeats=False):
	''' split iterable into segments with given lengths		segments(range(10), 2, 3) --> [[0, 1], [2, 3], [4, 5]]
		:param iterable: sequence to subdivide
		:param lengths: int or or series of ints. For the case that it is a series of ints result it may exhaust before iterable
		:param repeats: number of sub iterables to produce. Defaults to infinity when omitted.
	'''
	iterable = iter(iterable)
	if repeats:
		repeats = [] if repeats is True else [repeats]
		lengths = it.repeat(lengths, *repeats)
	if isinstance(lengths, int):
		lengths = [lengths]
		
	for length in lengths:
		if isinstance(length, int):
			items = []
			try:
				for item in range(length):
					items.append(next(iterable))
			finally:
				if not items: break
				yield items
		else:
			items = list(segments(iterable, length, False))
			if not items: break
			yield items
			
def segments(iterable, lengths, repeats=False, cast=list):
	result = cast(isegments(iterable, lengths, repeats))
	return result



''' Combine Iterables
'''

def irep(part, rep):
	''' create iterable from *part* repeated *rep* times
		:param part: item to repeat in iterable
		:param rep: number of items in iterable
	'''
	if rep in Defs.repinfargs:
		while True:
			yield part			# *rep* of 'inf' or -1 appends part infinite times onto product iterable
	elif rep >= 0:
		for i in range(rep):
			yield part			# *rep* >= 0 appends this part *rep* times onto product iterable
	else:
		raise ValueError(f'invalid rep {rep}. Expected int >= 0, or a special flag from {{"inf", -1}}.')
		
def ireps(parts, reps=(1,)):
	''' combination iterable yielding each iterable or item in *parts* repeating *reps* times respectively
		:param parts:
		:param reps: number of times to repeat each arg in *parts*
	'''
	parts = tuple(parts)  # coerce to tuple to find number of parts
	reps = [reps]*len(parts) if isinstance(reps, int) or reps in Defs.repargs else list(reps)
	if len(reps) < len(parts):
		reps = list(reps) + ([reps[-1]]*len(parts))
		
	# yield each *part* by *rep* times
	for part, rep in zip(parts, reps):
		if rep in Defs.repinfargs:
			while True:
				yield part			# *rep* of 'inf' or -1 appends part infinite times onto product iterable
		elif rep in Defs.repstarargs:
			yield from part			# *rep* of '*' or -2 extends this part iterable onto product iterable
		elif rep >= 0:
			for i in range(rep):
				yield part			# *rep* >= 0 appends this part *rep* times onto product iterable
		else:
			raise ValueError(f'invalid rep {rep}. Expected int >= -2, "*" or "inf".')

def iaffix(lpart, mpart, *rparts, reps=1, mreps='*'):
	''' create iterable from *mpart* with prefix *lpart* and optional suffix(es) *rpart(s)*
		:param lpart: item to prefix to iterable
		:param mpart: iterable body to be affixed
		:param rparts: items suffixed to iterable
		:param reps: number of times to repeat each affixed arg in *lpart* and *rparts*
		:param mreps: number of times to repeat the central *mpart* iterable
	'''
	if reps == 1 and mreps == '*':  # simple flat iterable
		yield lpart
		yield from mpart
		yield from rparts
	else:							# dynamic iterable with variable number of reps
		yield from ireps((lpart, mpart)+rparts, reps=[reps, mreps, reps])
		
def isuffix(mpart, *rparts, reps=1, mreps='*'):
	''' create iterable like mpart with suffix(es) rpart(s)
		:param mpart: iterable body to be affixed
		:param rparts: items suffixed to iterable
		:param reps: number of times to repeat each suffixed arg in *rparts*
		:param mreps: number of times to repeat the central *mpart* iterable
	'''
	if reps == 1 and mreps == '*':	# simple flat iterable
		yield from mpart
		yield from rparts
	else:							# dynamic iterable with variable number of reps
		yield from ireps((mpart,)+rparts, reps=[mreps, reps])

dreps, kreps, lreps, treps = gen_coll_wraps(ireps)
affix, daffix, kaffix, laffix, taffix = iaffix, *gen_coll_wraps(iaffix)
dsuffix, ksuffix, lsuffix, tsuffix = gen_coll_wraps(isuffix)


def izip(*seqs, fill=nothing, fills=nothing):
	''' a flexible zip function capable of both zip or itertools.zip_longest given fill(s). '''
	if fill == fills == nothing:
		# yield from zip(*seqs)
		# return zip(*seqs)
		yield from zip(*seqs)
	else:
		seqs = [iter(seq) for seq in seqs]
		fills = fills or ([fill] * len(seqs))
		unfinished_seq_ind = 0
		count = 0
		while True:
			zip_item = [next(seq, nothing) for seq in seqs]
			while len(seqs) > unfinished_seq_ind and zip_item[unfinished_seq_ind] is nothing:
				unfinished_seq_ind += 1
			if unfinished_seq_ind >= len(seqs):
				break

			zip_item = tuple((fill if i is nothing else i for i, fill in zip(zip_item, fills)))
			yield zip_item
			if count > 10:
				raise ValueError()
dzip, kzip, lzip, tzip = gen_coll_wraps(izip)

def izipr(*seqs, fill=None, fills=None, maxlen=None):
	''' zip *seqs* except aligned to the end. *fill(s)* is prefixed to iterables shorter than maxlen.
		Note: Each iterable in *seqs* without a length hint will first be resolved to a list.
	'''
	seqs = [seq if hasattr(seq, '__len__') else list(seq) for seq in seqs]
	maxlen = max(*map(len, seqs)) if maxlen is None else maxlen
	fills = fills or ([fill] * len(seqs))
	
	seqs_affixed = [iaffix(fill, seq, reps=maxlen-len(seq)) for seq, fill in zip(seqs, fills)]
	yield from zip(*seqs_affixed)
dzipr, kzipr, lzipr, tzipr = gen_coll_wraps(izipr)


def ijoin(seq, depth=1):
	''' default behavior is same as toertools.chain
		:param seq:
		:param depth:
	'''
	# consider adding a skip_depth arg
	if depth == 1:
		for item in seq:
			yield from item
	elif depth > 1:
		depth -= 1
		for item in seq:
			yield from ijoin(item, depth)
	elif depth == 0:
		yield from seq
	else:
		raise ValueError(f'Given depth of {str(depth)}')
djoin, kjoin, ljoin, tjoin = gen_coll_wraps(ijoin)

def stepper(seq, starts=2, *stop_step):
	starts = list(range(starts, *stop_step) if isinstance(starts, int) else starts)
	iterators = it.tee(seq, len(starts))
	result = zip(*[it.islice(iterator, start, None) for start, iterator in zip(starts, iterators)])
	return result


''' Function based Iterables & Accumulators
Naming Convention:
- leading 'i' returns iterable of each operation called
- no leading 'i' means the last presumably aggregate result is returned
- 'calls' plural means function uses multiple operations
- 'arg' sgl. means input args do not change for each operation called
- 'args' plr. means inputs are a sequence of args corresponding to each operation called
- 'fold' means use result as input arg for each subsequent operation called

For example:
- 'icallsargs' behaves like zip but paired inputs yields call(arg) for each item rather than a tuple
- builtin reduce could be represented as callfoldargs.
- builtin map could be represented as icallargs.
'''

def icallsarg(ops, *pa, **ka):
	''' get iterable of the outputs of given pos, key args *pa*, *ka* on a sequence of operations *ops*
		:param ops: a sequence of operations to call for each item
		:param pa: position args for all operations
		:param ka: key args for all operations
	'''
	for op in ops:
		yield op(*pa, **ka)
		
def icallsargs(ops, *pas, la=(), ra=(), ka=None, **kas):
	''' get iterable of the outputs of given pos, key args *pa*, *ka* on a
		sequence of operations with inputs *ops*, *kas*, *pas*.
		:param ops: a sequence of operations to call for each item
		:param pas: a sequence of position args for each operation
		:param la: left position args for all operations
		:param ra: right position args for all operations
		:param ka: key args for all operations
		:param kas: a sequence of key args for each operation
	'''
	ka = ka or {}
	kiterargs = {key: iter(args) for key, args in kas.items()}  # iterables congruent to ops are iter(kas.values()[n])
	for op, nth_pa in zip(ops, pas):
		nth_ka = {key: next(args) for key, args in kiterargs.items()}
		yield op(*la, *nth_pa, *ra, **ka, **nth_ka)
		
def callsfold(ops, *pa, to_pkargs=None, **ka):
	''' get iterable of the outputs of given pos, key args *pa*, *ka* on a sequence of operations *ops*
		:param ops: a sequence of operations to call for each item
		:param pa: first position args to sequence of operations
		:param to_pkargs: optional result to input pos, key args converter all subsequent calls
		:param ka: first key args to sequence of operations
	'''
	for op in ops:
		result = ops(*pa, **ka)
		pa, ka = ((result,), {}) if to_pkargs is None else to_pkargs(result)
	return result

''' Mutate Iterables
'''

''' Miscellaneous Iterators
'''

# import numpy as np  # move dependent functions elsewhere; import overhead not justified
def grid_coords(shape, coord_type=None):
	''' get ordered indices for an np.array with given shape
		shape: start/stop ranges for each axis; accepts an iterable or ints or slices
		usage:
		grid_coords([3, 3])             # => [[0,0], [0,1], [0,2], [1,0], ... [2,2]]
		grid_coords([3, slice(10,13)])  # => [[0,10], [0,11], [0,12], [1,10], ... [2,12]]
	'''
	bounds = list(shape)
	# change bounds to slice ranges
	for index, bound in enumerate(bounds):
		if isinstance(bound, int):
			bounds[index] = slice(0, bound)
		elif not type(bound) is slice:
			raise TypeError()
		
	coords = np.moveaxis(np.mgrid[bounds], 0, -1)  					# grid of coords like [[[z y x], ...], ...]
	coords = coords.reshape(coords.shape[0]*coords.shape[1], -1)	# list of coords [[z y x], ...]
	
	# coerce coords to given type
	# if coord_type:
	# 	coords = np.asarray([coord_type(coords) for coord in coords])
	return coords



# todo: move code below to generators
class Buffer:
	''' Iterable sequence buffer allowing arbitrary traversal or peeking at current value '''
	def __init__(self, value, start=0, end=None):
		self.value = value
		self.start = self.pos = start
		self.end = end
		self.given = None
		self.itr = None
	def __len__(self):
		result = self.end - self.pos
		return max(result, 0)
	def next(self):
		result = self.itr.send(None)
		return result
	def peek(self, offset=0):
		result = self.value[self.pos+offset]
		return result
	def prev(self):
		result = self.itr.send(-2)
		return result
	def __iter__(self):
		''' iterable generator method for buffer

			usage:
			#              01234567890123456789012345
			sbuf = Buffer('abcdefghijklmnopqrstuwyxyz')
			si = iter(sbuf)
			print(next(si)+next(si)+next(si))			# => 'abc'
			print(si.send(0)+si.send(0)+si.send(0))		# => 'def'
			print(si.send(1)+si.send(1)+si.send(1))		# => 'hjl'
			print(si.send(-1)+si.send(-1)+si.send(-1))	# => 'lll'
			print(si.send(-2)+si.send(-2)+si.send(-2))	# => 'kji'
		'''
		# print('\n__next__()')
		ofs, end = 0, len(self.value) if self.end is None else self.end
		while 0 <= self.pos < end:
			# print(f'__next__.pos:{self.pos}')
			self.pos += 1 + (ofs or 0)
			ofs = yield self.value[self.pos-1]

	def __getitem__(self, item):
		result = self.value[self.pos:self.end]
		result = result[item]
		return result


class IterProperty:	''' properties for augmenting iterators '''
class Accumulator(IterProperty): pass
class TrailTuple(IterProperty): pass
class Until(IterProperty): pass
class RaiseOn(IterProperty): pass
class Depth(IterProperty): pass

Converter = IterProperty  # will now refer to mutation iterators as converters

# @dataclass
# class Nested:
# 	items: 'Any'
# 	def __iter__(self): return iter(self.items)
#
# @dataclass
# class Atomic:
# 	item: 'Any'
#
# class TypeWalk:
# 	_type_dict = dict
#
# 	def __init__(self, to_items=None, to_item=None, nested=None, atomic=None, type_dict=None, debug=False):
# 		type_dict = type_dict or self._type_dict
#
# 		self.to_items = type_dict()
# 		self.to_items.update(to_items or {})
# 		self.to_items.update(dict.fromkeys(nested or [], iter))
# 		self.to_items[Nested] = iter
#
# 		self.to_item = type_dict()
# 		self.to_item.update(to_item or {})
# 		self.to_item.update(dict.fromkeys(atomic or [], noop))
# 		self.to_item[Atomic] = lambda atomic: atomic.item
#
# 		self.debug = debug
# 	def iterate(self, obj):
# 		result = obj
# 		# obj_type = type(obj)
# 		to_item = self.to_item.get(type(obj))
# 		to_items = self.to_items.get(type(obj))
#
# 		if to_items:
# # 			print(obj)
# 			items = to_items(obj)
# 			# result = (self.iterate(item) for item in items)
# 			result = [self.iterate(item) for item in items]
#
# 		if to_item:
# 			result = to_item(result)
# 			# if self.debug:
# 			# 	result = list(result)
# 		return result
#
# # 	def iterate(self, obj):
# # 		result = obj
# # 		obj_type = type(obj)
# # 		if obj_type in self:
# # # 			print(obj)
# # 			result = self[obj_type](self, obj)
# # 			if self.debug:
# # 				result = list(result)
# # 		return result
# 	__call__ = iterate


from dataclasses import dataclass, field

@dataclass
class StagedSlices:
	''' Class defines how a slice changes for each frame of iteration '''
	starts: object = None
	stops: object = None
	steps: object = None
	factory: 'typing.Any'	= field(init=False, default=slice)
	ind_types: tuple		= field(init=False, default=(type(None), int))
	index: slice			= field(init=False, default=None)
	has_next: bool			= field(init=False, default=False)
	def __post_init__(self):
		for ind_name in ['starts', 'stops', 'steps']:
			if not isinstance(self.__dict__[ind_name], self.ind_types):
				if ind_name == 'stops':
					self.__dict__[ind_name] = iter(map(lambda i: i+1,  self.__dict__[ind_name]))
				else:
					self.__dict__[ind_name] = iter(self.__dict__[ind_name])
		if not any((not isinstance(ind, self.ind_types) for ind in (self.starts, self.stops, self.steps))):
			raise UnboundLocalError('Infinite iteration detected.  One of start, stop or step must not be a index.')
	def __iter__(self):
		return self
	def __next__(self):
		try:
			args = tuple(ind if isinstance(ind, self.ind_types) else next(ind) for ind in (self.starts, self.stops, self.steps))
			self.index = self.factory(*args)
			return self.index
		except Exception as e:
			self.has_next = False
			raise StopIteration
	@staticmethod
	def grow_front(*starts_args):
		starts_args = range(*starts_args)
		return StagedSlices(reversed(starts_args), starts_args.stop)
	@staticmethod
	def grow_back(*stops_args):
		return StagedSlices(0, range(*stops_args))
	@staticmethod
	def shrink_front(*starts_args):
		starts_args = range(*starts_args)
		return StagedSlices(starts_args, starts_args.stop)
	@staticmethod
	def shrink_back(*stops_args):
		return StagedSlices(0, reversed(range(*stops_args)))
	
@dataclass
class StagedSeqences:
	''' Class defines how a sequence changes for each frame of iteration '''
	seq: list
	slices: StagedSlices
	prev: list = field(default_factory=list, init=False)
	def __post_init__(self):
		self.slices = isinstance(self.slices, StagedSlices) and self.slices or StagedSlices(*self.slices)
		self.itr = Iterable(self)
	def __str__(self):
		result = f'Seqs({self.prev})'
		return result
	__repr__ = __str__
	@property
	def has_next(self):
		return self.slices.has_next
	@property
	def __len__(self):
		return len(self.prev)
	def __iter__(self):
		return self.prev
	def __next__(self):
		slic = next(self.slices)
		self.prev = self.seq[slic]
		# self.prev[:] = self.seq[slic]
		return self
	def __getitem__(self, index):
		# result = self.seq[self.slices.index]
		result = self.prev[index]
		return result
	@staticmethod
	def grow_front(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.grow_front(length))
	@staticmethod
	def grow_back(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.grow_back(length))
	@staticmethod
	def shrink_front(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.shrink_front(length))
	@staticmethod
	def shrink_back(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.shrink_back(length))
		
	
@dataclass
class Iterable:
	''' handle to an iterable instance object '''
	state: object
	def __iter__(self):
		return self
	def __next__(self):
		result = next(self.state)
		return result


__all__ = get__all__except(locals(), __all__excludes__)

if __name__=='__main__':
	print(list(segments(range(10), 2, repeats=True)))
	print(list(segments(range(20), [1, 2, 3], repeats=2)))
	print(list(segments(range(100), [[[5, 4], 3], 2, 1])))
	
	
