from datetime import datetime


class timetaken:
	''' time a context operation '''
	def __enter__(self):
		self.start = datetime.now()
		return self
	def __exit__(self, exc_type=None, exc_val=None, exc_tb=None):
		self.delta_time = datetime.now() - self.start
		print(self.delta_time)
	def __repr__(self):
		result = f'{self.__class__.__name__}({self.delta_time})'
		return result
	
timetaker = timetaken()