import re

class DefsFmtUtils:
	class var_ptrn:
		_fmt_var_ptrn	= r'{}([\w\.\'\"]+)\{}'
		
		curly			= _fmt_var_ptrn.format('{', '}')
		curlys			= _fmt_var_ptrn.format('{{', '}}')
		dollars			= _fmt_var_ptrn.format('$$', '$$')
		percents		= _fmt_var_ptrn.format('%%', '%%')
Defs = DefsFmtUtils
_c1 = Defs.var_ptrn.curly
_c2 = Defs.var_ptrn.curlys
_d2 = Defs.var_ptrn.dollars
_p2 = Defs.var_ptrn.percents


ptrn_subs = dict(
	type_to_typename	= [r"<class '([^']+\.)?(\w+)'>", r'\2'],
	inst_to_typename	= [r"<([^>]+) at 0x\w{12}>", r'\1'],
)

def type_to_typename(text):
	return re.sub(*ptrn_subs['type_to_typename'], text)

def inst_to_typename(text):
	return re.sub(*ptrn_subs['inst_to_typename'], text)

def to_typename(text):
	result = re.sub(*ptrn_subs['type_to_typename'], text)
	result = re.sub(*ptrn_subs['inst_to_typename'], result)
	return result




def fstr(locals, fmt_str, var_ptrn=_c1, sub_fmt='({} ={}); ', globals=None, **kargs):
	# var_ptrn = re.compile(var_ptrn)
	result = fmt_str
	
	var = re.search(var_ptrn, result)
	while var:
		value = eval(var[1], globals, locals)
		sub_str = sub_fmt.format(var[1], value)
		result = re.sub(var_ptrn, sub_str, result, 1)
		
		# search again for another potential pass
		var = re.search(var_ptrn, result)
	
	return result
def fstrc2(*args, **kargs):	return fstr(*args, var_ptrn=_c2, **kargs)
def fstrd2(*args, **kargs):	return fstr(*args, var_ptrn=_d2, **kargs)
def fstrp2(*args, **kargs):	return fstr(*args, var_ptrn=_p2, **kargs)


def tee(msg, out=print, cast=str):
	result = cast(msg)
	out(result)
	return result
def ftee(*args, out=print, cast=str, **kargs):		return tee(fstr(*args, **kargs), out, cast)
def fteec2(*args, out=print, cast=str, **kargs):	return tee(fstr(*args, var_ptrn=_c2, **kargs), out, cast)
def fteed2(*args, out=print, cast=str, **kargs):	return tee(fstr(*args, var_ptrn=_d2, **kargs), out, cast)
def fteep2(*args, out=print, cast=str, **kargs):	return tee(fstr(*args, var_ptrn=_p2, **kargs), out, cast)

'''
Consider the following as replacement for fstr:


class NamespaceFormatter(Formatter):
   def __init__(self, namespace={}):
       Formatter.__init__(self)
       self.namespace = namespace

   def get_value(self, key, args, kwds):
       if isinstance(key, str):
           try:
               # Check explicitly passed arguments first
               return kwds[key]
           except KeyError:
               return self.namespace[key]
       else:
           Formatter.get_value(key, args, kwds)
           
...

fmt = NamespaceFormatter(globals())

greeting = "hello"
print(fmt.format("{greeting}, world!"))
'''