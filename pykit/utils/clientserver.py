#!/usr/bin/env python
import sys
import zmq
from dataclasses import dataclass
from pyutils.common.colorhighlight import *
from pyutils.common.thread import *
from time import sleep
import json
import pickle

### defaults

default_address = 'tcp://127.0.0.1:5555'
# serialize, deserialize = json.dumps, json.loads
serialize, deserialize = pickle.dumps, pickle.loads
send, recv = zmq.Socket.send, zmq.Socket.recv


log = None
# srvlog, clilog = None, None		# set to print for debugging
# srvlog, clilog = print, sys.gettrace() and print or None
srvlog, clilog = None, None

# serialize, deserialize = lambda msg: str(pickle.dumps(msg)), lambda data: pickle.loads(data)
# send, recv = zmq.Socket.send_string, zmq.Socket.recv_string



### Server functions

class Server:
	def __init__(self, address=default_address):
		self.address = address
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.REP)
		self.socket.bind(address)
		a = 0


	# def __getattr__(self, attr):
	# 	if hasattr(self.socket, attr):
	# 		return getattr(self.socket, attr)
	# 	return self.__getattribute__(attr)
		
	def poll(self, handler):
		log and log(bluel('Starting server.'))

		running = True
		while running:
			msg = self.socket.recv_string()
			running = not handler(self.socket, msg)
			sleep(.05)
		
		log and log(cyanl('Terminating server.'))
		
def server_socket(address=default_address):
	context = zmq.Context()
	socket = context.socket(zmq.REP)
	socket.bind(address)
	return socket
	
def server(handler, address=default_address):
	log and log(bluel('Starting server.'))
	socket = server_socket(address)
	
	running = True
	while running:
		msg = socket.recv_string()
		running = not handler(socket, msg)
		sleep(.05)
		
	log and log(bluel('Terminating server.'))

def _sample_handler(socket, msg):
	log and log(bluel(msg))
	if msg == 'zeromq':
		socket.send_string('ah ha!')
		return True
	else:
		socket.send_string('...nah')
		

### Client functions

class Client:
	def __init__(self, address=default_address):
		self.address = address
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.REQ)
		self.socket.connect(address)
	
	def __getattr__(self, attr):
		if hasattr(self.socket, attr):
			return getattr(self.socket, attr)
		return self.__getattribute__(attr)
		
	def poll(self, task):
		log and log(bluel('Starting client.'))

		running = True
		while running:
			running = not task(self.socket)
			sleep(.05)
		
		log and log(cyanl('Terminating client.'))
		
		
def client_socket(address=default_address):
	context = zmq.Context()
	socket = context.socket(zmq.REQ)
	socket.connect(address)
	return socket

def client(task, address=default_address):
	log and log(bluel('Starting client.'))
	socket = client_socket(address)
	
	running = True
	while running:
		running = not task(socket)
		sleep(.05)
	
	log and log(cyanl('Terminating client.'))

@dataclass
class _SampleTask:
	query: str
	def __call__(self, socket):
		log and log(cyanl(self.query))
		socket.send_string(self.query)
		reply = socket.recv_string()
		log and log(cyanl(reply))
		return True
		





# from datetime import datetime, timedelta
# class Trigger:
# 	def __init__(self, condition):
# 		self.condition = condition
# 		self.reset()
# 	def reset(self):
# 		self.is_triggered = False
# 	def check(self, *args, **kwargs):
# 		self.is_triggered |= self.condition(*args, **kwargs)
# 		return self.is_triggered
# 	__bool__ = check
#
# class ElapseTrigger(Trigger):
# 	def __init__(self, period):
# 		self.period = timedelta(seconds=period)
# 		self.reset()
# 	def reset(self):
# 		self.is_triggered = False
# 		self.elapse_time = datetime.now() + self.period
# 	def condition(self):
# 		now = datetime.now()
# 		result = self.elapse_time < now
# 		return result
# 	def check(self, *args, **kwargs):
# 		self.is_triggered |= self.condition(*args, **kwargs)
# 		return self.is_triggered
# 	__bool__ = check
# # refactored aliases
# ElapseCondition = ElapseTrigger
# Condition = Trigger
#
# def wait_for_any(trigger, *addt_triggers, throttle=.04):
# 	triggers = [trigger] + list(addt_triggers)
#
# class WaitFor:
# 	def __init__(self, cond, *addt_conds, throttle=.04):
# 		''' Blocks thread until all conditions are satisfied then returns.
# 			Accepted conditions include:
# 			- a period of seconds to wait
# 			- a callable which return true when satisfied
# 			- an instance of Trigger
# 		'''
# 		if isinstance(cond, WaitFor):
# 			self.__dict__.update(cond.__dict__)
# 			return
#
# 		# handle args normally
# 		self.throttle = throttle
# 		self.conditions = [cond] + list(addt_conds)
# 		for index, condition in enumerate(self.conditions):
# 			if isinstance(condition, (int, float)):
# 				self.conditions[index] = ElapseCondition(condition)
# 			elif not isinstance(condition, Condition):
# 				self.conditions[index] = Condition(condition)
#
# 	def __call__(self):
# 		map(lambda cond: cond.reset(), self.conditions)
#
# 		satisfied = False
# 		while True:
# 			satisfied = True
# 			for condition in self.conditions:
# 				satisfied &= bool(condition)
# 				if not satisfied:
# 					break  # stop checking after 1st unsatisfied
#
# 			if satisfied:
# 				break  # done if satisfied
#
# 			sleep(self.throttle)
# class WaitForAny(WaitFor):
# 	def __call__(self):
# 		map(lambda cond: cond.reset(), self.conditions)
#
# 		satisfied = False
# 		while True:
# 			for condition in self.conditions:
# 				satisfied |= bool(condition)
# 				if satisfied:
# 					break  # done if satisfied
#
# 			if satisfied:
# 					break  # done if satisfied
#
# 			sleep(self.throttle)
#

# See an overview of ZeroMQ here: http://blog.pythonisito.com/2012/08/distributed-systems-with-zeromq.html




### channel comms
# request, 	reply
# session,	serve

echo = lambda request: request
never_done = lambda *results: False
user_input = lambda: input('Enter a query: ')

class ChannelServer:
	def __init__(self, address=default_address):
		self.address = address
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.REP)
		self.socket.bind(address)
		self.finished = False
	def on_request(self, handler):
		log and log('on_request')
		data_in = recv(self.socket)
		query_args, query_kws = deserialize(data_in)
		msg = handler(*query_args, **query_kws)
		data_out = serialize(msg)
		send(self.socket, data_out)
		return (query_args, query_kws), msg
	def serve(self, request_handler=echo, finish_cond=never_done):
		log and log('serve')
		self.finished = False
		while not self.finished:
			results = self.on_request(request_handler)
			self.finished = finish_cond(*results)
		log and log('serve FINISHED')

class ChannelClient:
	def __init__(self, address=default_address, ):
		self.address = address
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.REQ)
		self.socket.connect(self.address)
		self.worker = None
	def issue_request(self, *query_args, **query_kws):
		clilog and clilog('ChannelClient.issue_request()')
		data_out = serialize((query_args, query_kws))
		send(self.socket, data_out)
		data_in = recv(self.socket)
		msg = deserialize(data_in)
		return msg
	def session(self, get_next_request=user_input, finish_cond=never_done, interval=0):
		clilog and clilog('ChannelClient.session()')
		self.finished = False
		while not self.finished:
			query_args, query_kws = get_next_request()
			results = self.issue_request(*query_args, **query_kws)
			self.finished = finish_cond(results)
			sleep(interval)
		clilog and clilog('ChannelClient.session() FINISHED')
	def start_session(self, *args, **kws):
		''' make a worker running the session loop '''
		self.worker = worker(self.session, *args, **kws)
		
		
### broadcast comms
# issue, poll
# publish, subscribe

class BroadcastServer:
	def __init__(self, address=default_address):
		self.address = address
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUB)
		self.socket.bind(address)
	def issue_poll(self, msg):
		srvlog and srvlog('BroadcastServer.issue_poll()')
		data = serialize(msg)
		return send(self.socket, data)
	def publish(self, get_poll_message, wait=1):
		srvlog and srvlog('BroadcastServer.publish()')
		
		running = True
		while running:
			self.issue_poll(get_poll_message())
			srvlog and srvlog('BroadcastServer.issue_poll() done\n')
			sleep(wait)

class BroadcastClient:
	_nill_msg = None  # designated nill message returned on recv timeout
	
	def __init__(self, address=default_address):
		self.address = address
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.SUB)
		self.socket.setsockopt_string(zmq.SUBSCRIBE, '')
		self.socket.setsockopt(zmq.RCVTIMEO, 30000)
		self.socket.connect(self.address)
		self.worker = None
		self.messages = None
	def poll(self):
		clilog and clilog(clear_col('BroadcastClient.poll()'))
		try:
			data = recv(self.socket)
			# data = self.socket.recv()
			msg = deserialize(data)
			clilog and clilog(clear_col('BroadcastClient.poll() msg received'))
		except zmq.error.Again as e:
			msg = self._nill_msg
		return msg
	def subscribe(self, handle_poll=None, wait=1, *, queue_len=1):
		clilog and clilog('BroadcastClient.subscribe()')
		if handle_poll is None:
			# keep msgs in a queue when subscribe is called with no handler
			self.messages = MessageQueue(maxlen=queue_len)
			handle_poll = self.messages.queue
			wait = 0
			
		running = True
		while running:
			msg = self.poll()
			if msg != self._nill_msg:
				handle_poll(msg)
			sleep(wait)
	def start_subscription(self, *args, **kws):
		''' make a worker running the subscribe loop '''
		self.worker = worker(self.subscribe, *args, **kws)
		sleep(1)
		return self.messages


if __name__=='__main__':
	from time import sleep
	from pyutils.common.thread import *
	
	worker(server, _sample_handler)
	sleep(.2)
	worker(client, _SampleTask('test'))
	sleep(.2)
	worker(client, _SampleTask('zeromq'))
	sleep(5)
