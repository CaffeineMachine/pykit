import inspect
import tokenize
import gc
import re
import sys
from io import StringIO



def filename(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	# result = frame.f_globals.get('__file__', '???')
	result = frame.f_code.co_filename
	if result[-4:-1] == '.py' and result[-1] in 'co':
		result = result[:-1]  # change .pyc & .pyo extensions to .py
	return result

def lineno(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	return frame.f_lineno

def funcname(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	return frame.f_code.co_name

def clsobj(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	result = None
	if 'self' in frame.f_locals:
		result = frame.f_locals['self'].__class__
	elif 'cls' in frame.f_locals:
		result = frame.f_locals['cls']
	result = result if isinstance(result, type) else None
	return result

def clsname(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	cobj = clsobj(depth, frame)
	result = isinstance(cobj, type) and cobj.__name__ or '???'
	return result

def funcsig(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	cname = clsname(depth, frame)
	fname = funcname(depth, frame)
	result = '%s.%s'%(cname, fname)
	return result

def funcsigln(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	cname = clsname(depth, frame)
	fname = funcname(depth, frame)
	line  = lineno(depth, frame)
	result = '%s.%s:%s'%(cname, fname, line)
	return result

def flocals(depth=1, frame=None):
	frame = frame or sys._getframe(depth)
	flocals = frame.f_locals
	return flocals

def member_order(obj, predicate='ismethod'):
	predicate = isinstance(predicate, str) and getattr(inspect, predicate) or predicate
	members = [ (name,meth) for name, meth in inspect.getmembers(obj, predicate)]
	members = sorted(members, key=lambda t: t[1].__code__.co_firstlineno)
	return members

def method_order(obj):
	return member_order(obj, 'ismethod')

def get_func():
	frame = inspect.currentframe().f_back
	code = frame.f_code
	return [referer
		for referer in gc.get_referrers(code)
		if getattr(referer, '__code__', None) is code][0]
	
def get_closure():
	frame = inspect.currentframe().f_back
	code = frame.f_code
	return [referer
		for referer in gc.get_referrers(code)
		if getattr(referer, '__code__', None) is code and
		set(inspect.getclosurevars(referer).nonlocals.items()) <=
		set(frame.f_locals.items())][0]

def method_class(meth):
	if inspect.ismethod(meth):
		for cls in inspect.getmro(meth.__self__.__class__):
			if cls.__dict__.get(meth.__name__) is meth:
				return cls
		meth = meth.__func__  # fallback to __qualname__ parsing
		
	if inspect.isfunction(meth):
		class_name = meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[0]
		cls = getattr(inspect.getmodule(meth), class_name, None)
		if cls is None:
			cls = meth.__globals__.get(class_name)
		if isinstance(cls, type):
			return cls
	
	return None

# classsig = funcsig
# classsigline = funcsigln
# classobj = clsobj
# classname = clsname

def func_operable_code(code, verbose=False):
	''' extracts all noop lines from the given function or code string '''
	# todo: implement option to include function signature
	
	if type(code) is not str:
		code = inspect.getsource(code)               # coerce code into a string of python code
	
	NOOPS = [tokenize.COMMENT, tokenize.INDENT, tokenize.NEWLINE, tokenize.STRING, tokenize.NL]
	result_tokens, line_tokens = [], []
	is_signature, is_noop_line = False, True
	
	# pass in stringio.readline to generate_tokens
	for toktype, tokval, begin, end, line in tokenize.generate_tokens(StringIO(code).readline):
		# determine qualifiers for line given current token
		is_noop_line &= toktype in NOOPS
		is_signature |= tokval == 'def'
		if verbose:
			print(f'{line.strip():30s} noop_line:{is_noop_line:b}; signature:{is_signature:b}; '
				  f'{tokenize.tok_name[toktype]:8s}; \'{tokval.strip()}\'')
		
		# operate on token lists
		if toktype is not tokenize.COMMENT:
			line_tokens += [(toktype, tokval)]
		if toktype in [tokenize.NEWLINE, tokenize.NL]:
			if not  is_noop_line and not is_signature:
				result_tokens.extend(line_tokens)     # add token list to operable code tokens
				
			line_tokens = []                          # reset token list for next line
			is_signature, is_noop_line = False, True  # reset qulifiers for next line
			
	result = tokenize.untokenize(result_tokens)
	return result


def is_datadescriptor(obj):
	result = hasattr(obj, '__get__') and hasattr(obj, '__set__')
	return result
def cls_names(inst):
	result = [name for name, var in inst.__class__.__dict__.items() if var.__class__.__name__ == 'member_descriptor']
	return result

def cls_vars(inst):
	result = [[name, var] for name, var in inst.__class__.__dict__.items() if is_datadescriptor(var)]
	return result

def func_arg_keys(func):
	result = method_arg_keys(func) if isinstance(func, type) else func.__code__.co_varnames
	return result
	
def method_arg_keys(func):
	if isinstance(func, type):
		func = func.__init__
	result = func.__code__.co_varnames[1:]
	return result

class UnmangleMeta:
	var_fmt = r'[\w_0-9]+'
	mangled_prefix = fr'_{var_fmt}__'
	get_super_prefix = r'_?super_'  # getter prefix for mangled parent members ghosted by unmangled subclass attr
	
	def unmangle(obj, name):
		''' Get mangled attribute corresponding to the given alias.
			regain reusabilty in modules where mangling is abused.
		'''
		# important note: reaching this point in execution indicates {name} is not a member of self
		if isinstance(obj, UnmangleAdapter):
			obj = obj.mangled_obj
	
		name = re.sub(UnmangleMeta.get_super_prefix, '', name)
	
		already_mangled = re.search(UnmangleMeta.mangled_prefix+UnmangleMeta.var_fmt, name)
		if already_mangled: raise AttributeError(f'{obj} has no attr \'{name}\'')
	
		# members = tuple(obj.__dict__.keys())
		# if hasattr(obj.__class__, '__slots__'):
		# 	members += obj.__class__.__slots__
		members = ' '.join(dir(obj))
		found = re.findall(fr'{UnmangleMeta.mangled_prefix}{name}', members)
	
		if not found:
			raise AttributeError(f'{obj} has no attr \'{name}\'')
	
		result = getattr(obj, found[0])
		return result
	
	__getattr__ = unmangle

unmangle = UnmangleMeta.unmangle

class UnmangleAdapter(UnmangleMeta):
	def __init__(self, mangled_obj):
		self.mangled_obj = mangled_obj
		
# class MangledAdapter:
# 	ptrn_alias_subs = [[r'_[\w_0-9]+__([\w_0-9]+)', r'\1'] ]
# 	def __init__(self, mangled_obj):
# 		self._refs = MangledAdapter.init(self, mangled_obj, self.ptrn_alias_subs)
#
# 	@staticmethod
# 	def init(adapter, obj_handles, ptrn_alias_subs):
# 		''' add aliases to adapter for all obj attr matches of given patterns '''
# 		added_refs = {}
# 		if not isinstance(obj_handles, tuple):
# 			obj_handles = (obj_handles,)
# 		for obj_handle in obj_handles:
# 			for attr in dir(obj_handle):
# 				for ptrn, alias in ptrn_alias_subs:
# 					if isinstance(alias, str):
# 						alias_name, count = re.subn(ptrn, alias, attr)
# 						alias_name = count and alias_name
# 					else:
# 						found = re.search(ptrn, attr)
# 						alias_name = found and alias(found.groups())
#
# 					if alias_name:
# 						attr_ref = getattr(obj_handle, attr)
# 						setattr(adapter, alias_name, attr_ref)  # assign alias to reference of obj.attr
# 						added_refs.setdefault(obj_handle, []).append(alias_name)
# 						break  # default behavior is making single alias for the first valid pattern match
# 		return added_refs

class MangledAdapter:
	ptrn_alias_subs = [[r'_[\w_0-9]+__([\w_0-9]+)', r'\1'] ]
	def __init__(self, mangled_obj):
		self._refs = MangledAdapter.init(mangled_obj, self.ptrn_alias_subs)
	
	def __getitem__(self, item):
		return self._refs[item]
		
	@staticmethod
	def init(obj_handles, ptrn_alias_subs):
		''' add aliases to adapter for all obj attr matches of given patterns '''
		refs = {}
		if not isinstance(obj_handles, tuple):
			obj_handles = (obj_handles,)
		for obj_handle in obj_handles:
			for attr in dir(obj_handle):
				for ptrn, alias in ptrn_alias_subs:
					if isinstance(alias, str):
						alias_name, count = re.subn(ptrn, alias, attr)
						alias_name = count and alias_name
					else:
						found = re.search(ptrn, attr)
						alias_name = found and alias(found.groups())
						
					if alias_name:
						attr_ref = getattr(obj_handle, attr)
						refs[alias_name] = attr_ref  # assign alias to reference of obj.attr
						# added_refs.setdefault(obj_handle, []).append(alias_name)
						break  # default behavior is making single alias for the first valid pattern match
		return refs
	

class Foobar:
	
	def three(self):	self.two()
	def two(self):		self.one()
	def one(self):		self.test_src_info()
	
	def test_src_info(self):
		print(lineno())
		print(filename())
		print(funcname())
		print(clsobj())
		print(clsname())
		print(funcsig())
		print(funcsigln())

	def test_a(self):
		func = get_func()
		# curr_frame = inspect.currentframe() # get the FrameInfos
		# func = inspect.getframeinfo(curr_frame)
		# func_name = func[2]
		a = 0
		'''
		for f_idx, f_info in enumerate(frame_infos):
			frame_dict.update(f_info.frame.f_locals) # update namespace with deeper frame levels
			#Output basic Error-Information
			print(f'  File "{f_info.filename}", line {f_info.lineno}, in {f_info.function}')
			for line in f_info.code_context:
				print(f'    {line.strip()}')
		
			########################################################
			# show signature and arguments 1 level deeper
			if f_idx+1 < len(frame_infos):
				func_name = frame_infos[f_idx+1].function #name of the function
				try:
					func_ref = frame_dict[func_name] # look up in namespace
					sig = inspect.signature(func_ref) # call signature for function_reference
				except KeyError: sig = '(signature unknown)'
				print(f'    {func_name} {sig}\n')
				print(f'    {frame_infos[f_idx+1].frame.f_locals}\n')
		'''