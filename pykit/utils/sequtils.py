''' Sequence Utilities
Contains a collection of functions and classes operating on a sequence.
'''

from pyutils.utilities.iterutils import *


''' Naming Convention

# pop
take, pop, oust, remove, drop, expel, slice, cut

# insert
place, depo, add, put, apply, affix, tack, pin, give, store, push

# by index
put, cut

# by value
puteq, cuteq
puteq, take

# by id
putid, cutid

# by condition
putif, cutif
'''


def mux(seq, into) -> list:
	''' allocate items in *seq* to channels defined by cyclic indexes *into*; produce _list_ of channels. '''
	into = range(into) if isinstance(into, int) else into
	channels = [[] for ind in range(max(into))]
	for key, item in zip(seq, cycle(into)):
		channels[key].append(item)
	return channels

def muxjoin(seq, into, join=ijoin) -> list:
	''' allocate items in *seq* to channels defined by cyclic indexes *into*; produce _joined list_ of channels. '''
	channels = multiplex(seq, into)
	channels = [join(channel) for channel in range(max(into))]
	return channels

def muxmap(seq, into) -> dict:
	''' allocate items in *seq* to channels defined by cyclic keys *into*; produce _map_ of channels. '''
	channels = {key:[] for key in into}
	for key, item in zip(seq, cycle(into)):
		channels[key].append(item)
	return channels
	
def muxmapjoin(seq, into, join=ijoin) -> dict:
	''' allocate items in *seq* to channels defined by cyclic keys *into*; produce _combined map_ of channels. '''
	channels = multiplexmap(seq, into)
	channels = {key:join(channel) for key, channel in channels.items()}
	return channels

multiplex, multiplexjoin, multiplexmap, multiplexmapjoin = mux, muxjoin, muxmap, muxmapjoin


def clip(seq, ind=None, *ind_args, modify=True):
	''' return item(s) removed from *seq* at given *ind* '''
	if ind is None: return Clipper(seq)
	
	result = seq[ind]
	ind = ind if isinstance(ind, slice) else slice(ind, ind+1 or None)
	if modify and hasattr(seq, '__setitem__'):
		seq[ind] = []
	return result

def chop(seq, ind=None, *ind_args, modify=True):
	''' return item(s) removed from *seq* at given *ind* '''
	if ind is None: return Chopper(seq)
	
	remain = seq
	result = seq[ind]
	ind = ind if isinstance(ind, slice) else slice(ind, ind+1 or None)
	if modify and hasattr(seq, '__setitem__'):
		remain[ind] = []
	else:
		remain = remain[0:ind.start] + remain[ind.stop:]
	return result, remain

class Clipper:
	def __init__(self, seq):
		self.seq = seq
	def __getitem__(self, ind):
		assert isinstance(ind, (int, slice)) or hasattr(ind, '__index__'), f'Invalid *ind* {ind}. Must be indexable.'
		return clip(self.seq, ind)

class Chopper:
	def __init__(self, seq):
		self.seq = seq
	def __getitem__(self, ind):
		assert isinstance(ind, (int, slice)) or hasattr(ind, '__index__'), f'Invalid *ind* {ind}. Must be indexable.'
		return chop(self.seq, ind)

def take(seq, value, count='todo')->object:
	''' Find and remove given *value* from *seq*. Returns removed item; unlike list.remove() counterpart. '''
	result = seq.pop(seq.index(value))
	return result

def to_front(seq, values):
	seq[:0] = map(take.__get__(seq), values)
	return seq

def to_back(seq, values):
	seq[max_uint64:] = map(take.__get__(seq), values)
	return seq

def isplit(seq, sep=arg_missing, sep_cmp=arg_missing, cast_subseq=None):
	''' split given *seq*uence into subseqences delimited by each matching *sep*arator
		:param seq: sequence to split by separator
		:param sep: identifies separators as being equal to *sep* if given; takes priority over sep_cmp
		:param sep_cmp: identifies separators with bool(*sep_cmp*(item)) if given
		:param cast_subseq: used to casts each subseq if not None
	'''
	cast_subseq = None if cast_subseq == list else cast_subseq  # does not need to cast to list
	subseq = []
	for item in seq:
		is_sep_match = sep_cmp(item) if sep is arg_missing else item == sep
		if is_sep_match:
			subseq = cast_subseq(subseq) if cast_subseq else subseq
			yield subseq
			subseq = []
		else:
			subseq.append(item)
			
	subseq = cast_subseq(subseq) if cast_subseq else subseq
	yield subseq
	
def lsplit(*args, **kargs): return list(isplit(*args, **kargs))

def splitargs(seq, cast_subseq=None):
	''' split given args *seq*uence by '...' separator into largs and rargs (usually; but not limited to 1 sep) '''
	result = list(isplit(seq, ..., cast_subseq=cast_subseq))
	if len(result) < 2:
		result.append([])
	return result
	


class IndFab:
	def __getitem__(self, ind)->slice:
		return ind
slicer = Ind = IndFab()