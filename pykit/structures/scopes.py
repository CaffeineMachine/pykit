from dataclasses import dataclass, field
from typing import List, Any
from functools import reduce
from pyutils.common.collect import *

__all__ = [
	'Offset', 'Range',
	'ScopeNode', 'ScopesIterator',
	'ScopeDelta', 'Scopes', 'ScopedContent',  ]


@dataclass
class Offset:
	''' struct representing an index or offset '''
	index: int

@dataclass
class Range(Offset):
	''' struct representing start, stop bounds '''
	stop: int
	
	@property
	def start(self): return self.index
	
	def __len__(self): return self.stop-self.index
	
	def __format__(self, format_spec='v'):
		if 's' in format_spec:
			result = f'<{self.index}-{self.stop}>'
		else:
			result = f'{self.__class__.__name__}(start={self.index}, stop={self.stop})'
		return result
	__repr__ = __str__ = __format__


	
'''
ScopeNodes:
Depicted below is an example hierarchy of nested ScopeNodes. ScopeNode start/stop range is
relative to the parent.

0                   1                   2
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 	index
-------------------------------------------------------------
							   0----3             1-2	|3
			3-4 5-6   8--10    1------5   6-7   1---3	|2
	  3------------------13  14------------22 23---26	|1
0--------------------------------------------------26	|0
														|depth


Scopes, ScopeDelta:
Scopes class can be used to produce the tree of ScopeNodes above either by initializing
with tuples of ScopeDeltas(dt, ddepth) where dt and ddepth are the change in index and depth:
deltas = [	[3, 1], [3, 1], [1, 1], [1, -1], [1, 1], [2, -1],
			[2, 1], [1, -2], [1, 1], [3, 2], [1, -1], [1, -1],
			[1, 1], [1, -1], [1, -1], [1, 1], [1, 1], [1, 1], [1, -4],  ]
or by an equivalent set of pushes and pops.


ScopedContent, SegmentDelta:
Depicted below is the application of content such as the string 'abcdefghijklmnopqrstuvwxyz'
to the ScopeNodes represented above using its subclass ScopedContent.  The content can be a
string or any other iterable of arbitrary items.

0                   1                   2
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 	index
-------------------------------------------------------------
							  p q r               z		|3
			g   i     l m     ^ ^ ^ s   u       y ^		|2
	  d e f ^ h ^ j k ^ ^   o ^ ^ ^ ^ t ^ v   x ^ ^		|1
a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ n ^ ^ ^ ^ ^ ^ ^ ^ w ^ ^ ^		|0
														|depth
'''

@dataclass
class ScopeNode(Range):
	''' ScopeNode class is fundamentally a start/stop range node nested into a hierarchical structure '''
	subtree: list = None
	parent: object = None
	
	def __post_init__(self):
		self.coerce_all()
		
	def coerce_all(self):
		if self.subtree and isinstance(self.subtree[0], list):
			self.subtree = [ScopeNode(*node) for node in self.subtree]

	def __format__(self, format_spec='s'):
		''' specify print format via format_spec. By default prints recursively in class format.
			Include 'r' to print scope node recursively.
			Include 's' to print in abbreviated <a-b [...]]> where a is start and b is stop.
		'''
		show = lambda s: str(s).replace('\n', '\n  ')
		if 'r' not in format_spec:
			return super().__format__(format_spec)
		
		subtree = self.subtree and sjoin([f'{node:{format_spec}}' for node in self.subtree], '\n  ', [', ['], ' ]', show) or ''
		if 's' in format_spec:
			result = f'<{self.index}-{self.stop}{subtree}>'
		else:
			result = f'{self.__class__.__name__}(start={self.index}, stop={self.stop}{subtree}'
		return result
	@staticmethod
	def coerce(obj):
		result = ScopeNode(*obj)
		return result
	
	def __iter__(self, mode=0):
		result = iter(ScopesIterator(self, mode))
		return result
	def append(self, node):
		self.subtree = self.subtree or []
		self.subtree.append(node)
	def extend(self, nodes):
		self.subtree = self.subtree or []
		self.subtree.extend(nodes)
	__repr__ = __str__ = __format__

@dataclass
class ScopesIterator:
	''' Class defining the method of iteration over a hierarchy of scopes and outputs '''
	node: ScopeNode
	mode: int = 0
	domain: list = field(default_factory=list)
	
	depth = Depth, Flat = range(2)
	def __iter__(self):
		subdomain = self.domain + [self.node]
		real_index = reduce(lambda a, n: a+n.index, subdomain, 0) # index relative root instead of parent
		depth = len(self.domain)
		s_subdomain = sjoin(subdomain, ', ', '[', ']', lambda n: f'{n:s}')
		if depth == 0:
			# print(f'{depth}, {self.node:s}, {self.node:s}, {s_subdomain}')
			yield depth, self.node, self.node, subdomain
		depth += 1
		
		absolute = ScopeNode(0, 0)
		for relative in self.node.subtree or []:
			absolute.index, absolute.stop = real_index+relative.index, real_index+relative.stop
			# print(f'{depth}, {absolute:s}, {relative:s}, {s_subdomain}')
			yield depth, absolute, relative, subdomain
			if self.mode == ScopesIterator.Depth:
				yield from ScopesIterator(relative, self.mode, subdomain)

@dataclass
class ScopeDelta:
	''' Struct represents the relative offset (dt) of a change in depth (ddepth) '''
	dt: int
	ddepth: int
	
	def __len__(self):
		return 2
	def __iter__(self):
		return iter([self.dt, self.ddepth])

@dataclass
class Scopes:
	''' Class containing a sequence of scope deltas that represent and reproduce a scope hierarchy '''
	deltas: list = field(default_factory=list)
	nodes: list = field(init=False, default_factory=list)
	root: ScopeNode = field(init=False, default=None)
	
	def __post_init__(self):
		if self.deltas:
			self.apply(self.deltas)
	
	def apply(self, deltas):
		for dt, ddepth in deltas:
			if ddepth == 0:
				raise ValueError()
			elif ddepth > 0:
				self.push(dt, ddepth)
			else:
				self.pop(dt, -ddepth)
		return self.root

	def __iter__(self):
		return iter(self.root)
	
	def push(self, value, depth=1):
		# print('='*40 + f'\npush(value={value}, y={depth})')
		# track newly pushed node until it gets popped
		index = self.nodes and len(self.nodes[-1]) or 0
		for push_count in range(depth):
			node = ScopeNode(index, index)
			if self.nodes:
				self.nodes[-1].append(node)
			self.nodes.append(node)
			index = 0  # immediately subsequent pushed nodes have offset of 0 to its parent
		
		# extend all tracked nodes by value size
		value_size = value
		for node in self.nodes:
			node.stop += value_size
			
		self.root = self.root or self.nodes[0]
		
	def pop(self, value, depth=1):
		# print('='*40 + f'\npop(value={value}, y={depth})')
		# insert last pushed node into its parent
		for pop_count in range(depth):
			node = self.nodes.pop()
		
		# extend all tracked nodes by value size
		value_size = value
		for node in self.nodes:
			node.stop += value_size
	
	def move_push(self, value_size, depth=1):
		# print('='*40 + f'\npush(value_size={value_size}, y={depth})')
		self.move(value_size)
		self._push(depth)
	def move_pop(self, value_size, depth=1):
		# print('='*40 + f'\npop(value_size={value_size}, y={depth})')
		self.move(value_size)
		self._pop(depth)
	def _pop(self, depth=1):
		# insert last pushed node into its parent
		for pop_count in range(depth):
			node = self.nodes.pop()
	def _push(self, depth=1):
		# track newly pushed node until it gets popped
		index = self.nodes and len(self.nodes[-1])-1 or 0
		for push_count in range(depth):
			node = ScopeNode(index, index+1)
			if self.nodes:
				self.nodes[-1].append(node)
			self.nodes.append(node)
			index = 0  # immediately subsequent pushed nodes have offset of 0 to its parent
			
		self.root = self.root or self.nodes[0]
		
	def move(self, value_size):
		# extend all tracked nodes by value size
		for node in self.nodes:
			node.stop += value_size

@dataclass
class SegmentDelta(ScopeDelta):
	''' Struct represents a change in depth (ddepth) given an arbitrary segment of content
		with well defined length (dt)
	'''
	value: Any

	def __post_init__(self):
		self.dt = len(self.value)

@dataclass
class ScopedContent(Scopes):
	''' Class containing a sequence of SegmentDeltas that represent and produce a scope hierarchy
		within some arbitrary iterable content
	'''
	content: Any = None
	def __post_init__(self):
		if self.deltas:
			if self.content:
				self.apply(self.deltas)
			else:
				self.concat(self.deltas)
	def __iter__(self):
		for depth, absolute, relative , context in self.root:
			segment = self.content[slice(absolute.start, absolute.stop)]
			yield segment, depth, absolute, relative , context
	
	def concat(self, deltas):
		for delta in deltas:
			self.add(**delta.__dict__)
		return self.root
	
	def add(self, value, ddepth=0, dt=None):
		# extend content with input value and update scope bounds
		self.content.append(value)
		dt = dt if dt is not None else len(value)
		if ddepth >= 0:
			self.push(dt, ddepth)
		else:
			self.pop(dt, ddepth)

	
	
