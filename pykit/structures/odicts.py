from collections import OrderedDict
from pyutils.structures.maps import *


class AttrODict(AttrEnum, OrderedDict):
	''' Extend OrderedDict to allow access to items as members. As in d['attr'] == d.attr '''
	_init_sort = True
class FullODict(FullMapping, OrderedDict):
	''' Extend OrderedDict to allow getitem key access misses fall back to __missing__ rather than throwing exception '''
	_init_sort = True
class FillODict(FillMapping, OrderedDict):
	''' Extend OrderedDict to allow getitem key access misses fall back to __missing__ rather than throwing exception '''
	_init_sort = True
class FullAttrODict(FullMapping, AttrEnum, OrderedDict):
	''' Extend OrderedDict to allow:
		1. access to items as members. As in d['attr'] == d.attr
		2. getitem key access misses fall back to __missing__ rather than throwing exception
	'''
	_init_sort = True
class FillAttrODict(FillMapping, AttrEnum, OrderedDict):
	''' Extend OrderedDict to allow:
		1. access to items as members. As in d['attr'] == d.attr
		2. getitem key access misses fall back to __missing__ rather than throwing exception
	'''
	_init_sort = True
	
class RemapODict(ValueRemapping, OrderedDict): pass

# todo: obsolete; replace all instances of *ClassName*Dict with *ClassName*ODict as the former is a misnomer
FullDict = FullOMap = AttrODict
FillDict = FillOMap = FullODict
AttrDict = AttrOMap = FillODict
FullAttrDict = FullAttrOMap = FullAttrODict
FillAttrDict = FillAttrOMap = FillAttrODict
RemapDict = RemapOMap = RemapODict

