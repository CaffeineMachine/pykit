from collections import OrderedDict as odict
from pyutils.structures.maps import *
from pyutils.utilities.iterutils import *
from dataclasses import dataclass, field


log = None
# log = print

class OrderedSet(KeysMap, odict):
	to_key = staticmethod(asis)
	
	def __init__(self, *args, keep_first=True, **kargs):
		self.keep_first = keep_first
		super().__init__(*args, **kargs)
		
	def add(self, item):
		if item in self:
			if self.keep_first:
				return
			else:
				self.discard(item)
		super().add(item)
	

def get_inheritance(subclass):
	''' get all parent classes of a given *subclass* type '''
	type_inheritance = OrderedSet(keep_first=False)
	new_bases = OrderedSet([subclass])
	old_bases = []
	
	while new_bases:
		# update type_inheritance result and new_bases with parent classes of *new_base* (where new_base is new_bases[0])
		new_base = new_bases.pop(first(new_bases.keys()))
		type_inheritance.combine(new_base.__bases__)
		new_bases.combine(new_base.__bases__)
		
		# prepare state for next iteration
		old_bases += [new_base]
		new_bases -= old_bases

	return type_inheritance


class TypeMapping:
	# todo: move into pyutils.maps
	coerce_key = staticmethod(asis)
	
	# def __init__(self, values=None, **kargs):
	# 	super().__init__(values=values, **kargs)
	def __getitem__(self, type_key, default=arg_missing):
		log and log(f'{self.__class__.__name__}.__getitem__')
		if type_key in self:
			log and log(f'super({self.__class__.__name__}).__getitem__({type_key})')
			return super().__getitem__(type_key)
		
		super_types = get_inheritance(type_key)
		super_types = lseq(super_types, self.coerce_key)
		
		# result = firstkeep(self.items(), lambda item: item[key_ind] in super_types)
		# results = keep(self.items(), lambda item: item[key_ind] in super_types)
		results = [value for key, value in self.items() if key in super_types]
		if not len(results):
			if default is arg_missing:
				raise KeyError()
			return default
		
		result = first(results)
		return result
	def get(self, type_key, default=None):
		log and log(f'{self.__class__.__name__}.get')
		return self.__getitem__(type_key, default)
	
class TypeMap(TypeMapping, dict): pass

class TypeNameMap(TypeMapping, KeyMapping, dict):
	''' Same as TypeDict except indexes by type name to provide flexibility for comparisons.
		Useful when python environment is partially reimported.
	'''
	coerce_key = staticmethod(lambda cls: isinstance(cls, type) and cls.__name__ or cls)
	
	# __getitem__ = TypeDict.__getitem__
	# get = TypeDict.get
	
	
	
	
@dataclass
class Nested:
	items: 'Any'
	def __iter__(self): return iter(self.items)

@dataclass
class Atomic:
	item: 'Any'
	
class TypeWalk:
	# _type_dict = dict
	_type_dict = TypeNameMap
	
	def __init__(self, to_items=None, to_item=None, nested=None, atomic=None, type_dict=None, debug=False):
		type_dict = type_dict or self._type_dict
		
		self.to_items = type_dict()
		self.to_items.update(to_items or {})
		self.to_items.update(dict.fromkeys(nested or [], iter))
		self.to_items[Nested] = iter
		
		self.to_item = type_dict()
		self.to_item.update(to_item or {})
		self.to_item.update(dict.fromkeys(atomic or [], asis))
		self.to_item[Atomic] = lambda atomic: atomic.item
		print(list(self.to_items.keys()) + list(self.to_item.keys()))
		
		self.debug = debug
	def iterate(self, obj):
		print('TypeWalk.iterate')
		result = obj
		# obj_type = type(obj)
		print(f'  self.to_item.get({type(obj).__name__})')
		to_item = self.to_item.get(type(obj))
		print(to_item)
		print()
		print(f'  self.to_items.get({type(obj).__name__})')
		to_items = self.to_items.get(type(obj))
		print(to_item)
		print()
		
		if to_items:
# 			print(obj)
			items = to_items(obj)
			# result = (self.iterate(item) for item in items)
			result = [self.iterate(item) for item in items]
			
		if to_item:
			result = to_item(result)
			# if self.debug:
			# 	result = list(result)
		return result
		
# 	def iterate(self, obj):
# 		result = obj
# 		obj_type = type(obj)
# 		if obj_type in self:
# # 			print(obj)
# 			result = self[obj_type](self, obj)
# 			if self.debug:
# 				result = list(result)
# 		return result
	__call__ = iterate
