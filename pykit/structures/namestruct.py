
class NameStruct(object):
	''' Assign the variable name as a string to each public variable in a struct instance.
		Subclasses should take care to initialize this class first.
	'''
	# todo: transition this to a decorator such that it does not need to be instantiated or inherited
	def __init__(self):
		self._name_of = {}
		names = dir(self)
		for name in names:
			if not name.startswith('_'):
				value = getattr(self, name)
				try: self._name_of[value] = name
				except: pass
				setattr(self, name, name)
		self.name_of = self.__name_of

	def __name_of(self, value):
		name = self._name_of.get(value)
		return name



if __name__ == '__main__':
	class Alphas(NameStruct):
		a = None
		b = None
		c = None
	class Numbers(NameStruct):
		_enum = (
			one,
			two,
			three
		) = range(3)


	alphas = Alphas()
	print(' '.join((alphas.a, alphas.b, alphas.c)))		# => 'a b c'

	nums = Numbers()
	print(' '.join((nums.one, nums.two, nums.three)))	# => 'one two three'
	print(nums._enum)									# => '[0, 1, 2]'
