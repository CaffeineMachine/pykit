

class Bits(list):
	def __init__(self, reverse=False):
		super().__init__([ 2**bit for bit in (reverse and reversed or iter)(range(64)) ])
bits = Bits()
rbits = Bits(reverse=True)


class BitShift:
	def __init__(self, reverse=False):
		self.ind = 0
		self.bits = reverse and rbits or bits
		self._iter = self.generate()
	def next(self, size=1):
		if type(size) is int:
			result = [next(self._iter) for b in range(size)]
		else:
			result = [self.next(count) for count in size]
		return result
	def generate(self):
		for b in self.bits:
			self.ind += 1
			yield b
	def __iter__(self):
		return self._iter
	def __next__(self):
		return next(self._iter)
	def __len__(self):
		return len(self.bits) - self.ind
	def __getitem__(self, index):
		return self.bits[index + self.ind]
	__setitem__ = bits.__setitem__
	
class BitMask(dict): pass # todo


if __name__=='__main__':
	bit_shift = BitShift()
	# bits_type = (is_key, is_mouse, is_click, is_motion, is_ui, is_other) = bit_shift.next(6)
	# bits_input_state = (is_down, is_up, is_scroll) = bit_shift.next(3)
	bits_type, bits_input_state = bit_shift.next([6, 3])
	print(bits_type)
	print(bits_input_state)
	print(bit_shift[0])
	print(list(bit_shift))
	bit_sets = BitSets()
