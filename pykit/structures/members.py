import re

from pyutils.defs.worddefs import asis
from pyutils.output.reformat import *
from pyutils.structures.strs import TStrs


''' Utility Layered Accessors
synonyms: tiered, heirarchy, tree, nested, org, complex, structure, domain, division, classification, layers, strata
'''

def rget_keys(obj, keys, default=nothing, on_miss=nothing):
	''' recursively getitem(key) on *obj* for given *keys*. '''
	result = obj
	try:
		for key in keys:
			result = result[key]
			# result = dict.__getitem__(result, key)
	except KeyError:
		if nothing is default is on_miss:
			raise
		result = default if on_miss is nothing else on_miss(keys)
	return result
def has_keys(obj, keys):
	''' check if names ref exists in obj '''
	result = rget_keys(obj, keys, ...)
	return result is not ...
def rget_call_keys(obj, call, keys, *pa, default=nothing, on_miss=nothing, **ka):
	''' with lead *keys* recursively getitem(key) on *obj* for a target and with last key do *call*(target, key). '''
	result = default
	*target_keys, key = keys
	# do recursive get with leading keys
	target_obj = rget_keys(obj, target_keys, default=default, on_miss=on_miss)
	
	# perform *call* on dest_obj with last key
	# default_pa = (on_miss is nothing and (on_miss(key),)) or (default is nothing and (default,)) or ()
	dest_op = isstr(call) and getattr(target_obj.__class__, call) or call
	result = dest_op(target_obj, key, *pa, **ka)
	return result
def pop_keys(obj, keys, *pa, **ka):		return rget_call_keys(obj, 'pop', keys, *pa, **ka)
def del_keys(obj, keys):				return rget_call_keys(obj, '__delitem__', keys)


def rfill_keys(obj, keys, on_miss=nothing, default=nothing):
	''' recursively fill(key) on *obj* for given *keys*. Fill means dict.setdefault. '''
	result = obj
	for key in keys:
		value = result.get(key, default)
		if on_miss is not nothing and value is default:
			value = on_miss(key)
			result[key] = value
			# result = result.setdefault(key, value)
			# dict.__setitem__(result, key, value)
		result = value
	return result
def rfill_call_keys(obj, call, keys, *pa, on_miss=nothing, default=nothing, **ka):
	''' with lead *keys* recursively getitem(key) on *obj* for a target and with last key do *call*(target, key). '''
	result = default
	*target_keys, key = keys

	# do recursive get with leading keys
	target_obj = rfill_keys(obj, target_keys, default=default, on_miss=on_miss)
	
	# perform *call* on dest_obj with last key
	dest_op = isstr(call) and getattr(target_obj.__class__, call) or call
	result = dest_op(target_obj, key, *pa, **ka)
	return result
def set_keys(obj, keys, value, **ka):	return rfill_call_keys(obj, '__setitem__', keys, value, **ka)

def rget_attrs(obj, attrs, default=nothing):
	''' recursively getattr(key) on *obj* for given *attrs*. '''
	default_pa = (default,) if default != nothing else ()
	result = obj
	try:
		for attr in attrs:
			result = getattr(result, attr, *default_pa)
	except AttributeError:
		if default is nothing:
			raise
		result = default
	return result
def has_attrs(obj, attrs):
	''' check if *attrs* ref exists in *obj* '''
	result = rget_attrs(obj, attrs, ...)
	return result is not ...

def rget_call_attrs(obj, call, attrs, *pa, default=nothing, **ka):
	''' with lead *attrs* recursively getattr(key) on *obj* for a target and with last attr do *call*(target, attr). '''
	result = default
	*target_attrs, attr = attrs
	try:
		# do recursive getattr with leading attrs
		target_obj = rget_attrs(obj, target_attrs)
		
		# perform *call* on dest_obj with last key
		default_pa = (default,) if default != nothing else ()
		dest_op = isstr(call) and getattr(target_obj.__class__, call) or call
		result = dest_op(target_obj, attr, *default_pa)
	except AttributeError:
		if default == nothing:
			raise
	return result
def set_attrs(obj, attrs, value):		return rget_call_attrs(obj, setattr, attrs, value)


''' Member Structures
'''

class MemberArgs:
	''' User defined namespace object. Ctor keyword args can be accessed as attributes. '''
	def __init__(self, **ka):
		self.__dict__.update(ka)
		
	# method templates
	__repr__ = repr_obj
Mbrs = Members = MemberArgs


@dataclass
class ReprMbrs:
	name_seq: list = ()
	keys: set = True
	cls_name: bool = True
	obj: object = None
	
	def __post_init__(self):
		if isstr(self.name_seq):
			self.name_seq = TStrs(self.name_seq)
	
	def get_repr(self, obj=None):
		obj = obj if obj is not None else self.obj
		mbrs_fmt = ', '.join((f'{{.{name}}}' for name in self.name_seq))
		mbrs_fmt = f'{{.__class__.__name__}}({mbrs_fmt})'
		# result = f'{obj.__class__.__name__}({DefsJoins.jj_keyvals(imapitems(self.name_seq, obj))})'
		result = mbrs_fmt.format(*[obj]*(len(self.name_seq)+1))
		return result
	def __get__(self, obj, cls):
		if obj:
			result = lambda: self.get_repr(obj)
			obj.__class__.__repr__ = self
		else:
			result = self
			cls.__repr__ = self
		return result
	
	__call__ = get_repr

class AttrGetter(str): pass
class CallGetter:
	def __init__(self, callable):
		self.callable = callable
	def __call__(self, *pa, **ka):
		callable = self.callable
		return callable(*pa, **ka)

class RefsCls:
	getters = (get_index, get_attribute, get_callable) = range(1,4)
	_classes = {get_index:(int, slice, tuple, list), get_attribute:str, get_callable:object}

	def __init__(self, key=None, getter_type=get_attribute, prev_keys=()):
		key = [] if key is None else [self.coerce_key(key, getter_type)]
		self.keys = list(prev_keys) + key
	def coerce_key(self, key, getter_type=None):
		typed_key = key
		if isinstance(key, (AttrGetter, CallGetter)):
			typed_key = key
		elif getter_type is self.get_attribute or (getter_type is None and isinstance(key, str)):
			typed_key = AttrGetter(key)
		elif getter_type is self.get_callable or (getter_type is None and hasattr(key, '__call__')):
			typed_key = CallGetter(key)
		elif getter_type is self.get_index or getter_type is None:
			typed_key = key
		return typed_key
	def add(self, key, getter_type=get_attribute):
		self.keys.append(self.coerce_key(key, getter_type))
	def __repr__(self):
		result = 'Keys'
		for key in self.keys:
			getter_type = RefsCls.get_attribute if isinstance(key, AttrGetter) else RefsCls.get_index
			if getter_type is RefsCls.get_index:
				result += f'[{key}]'
			else:
				result += f'.{key}'
		return result
	def __iter__(self):
		yield from self.keys
	
	# todo: simplify by using a dedicated Attrfab as a factory
	''' Factory functions '''
	def __getattr__(self, attr):
		return self.__class__(AttrGetter(attr), prev_keys=self.keys)
	def __getitem__(self, ind):
		return self.__class__(ind, getter_type=self.get_index, prev_keys=self.keys)
	
	''' Object Reference Accessors '''
	def get(self, obj, *pa, **ka):
		result = obj
		for key in self.keys:
			if isinstance(key, AttrGetter):
				result = getattr(result, key)
			elif isinstance(key, CallGetter):
				result = key(result, *pa, **ka)
			else:
				result = result.__getitem__(key, *pa, **ka)
		return result
	def get_all(self, objs):
		yield from map(self.get, objs)
	def __get__(self, obj, cls):
		obj = cls if obj is None else obj
		result = self.get(obj)
		return result
	def __set__(self, obj, cls):
		print('RefsCls.__set__({obj}, {cls})')
		raise NotImplemented()
	
	# method aliases
	__call__ = get

class IRefsCls(RefsCls):
	__call__ = RefsCls.get_all
	
Name = Refs = RefsCls()
IName = IRefs = IRefsCls()


class _Sig(RefsCls):
	def __str__(self):
		return re.sub(r'^Keys\.?', '', super().__str__())
	__repr__ = __str__
Sig = _Sig()


''' Naming Convention:
instance.name = member
var = (name, member)

map[key] = value
item = (key, value)

Map: A collection of values each indexed with a key.
Maps at times may suffice to just implement .keys() and .__getitem__() instead of all index functions.
Note: python only requires keys() & __getitem__(), i.e. a map, for an object to be passed using star-star syntax like func(**key_args)
From these 2 functions values, items, __len__, __iter__, __contains__, __bool__ can all be derived.
all dictionaries are maps but all maps are not dictionaries. For example below cached_primes might not be
implemented as a dict.
>>> bool(cached_primes[113])

Tree: a nested data structure of Nodes
nested_map[path] = node

composite_instance.names = component
var = (name, member)

Names, Keys, Inds and any combination will qualify as a Ref
'''