import os
from filelock import FileLock
from pyutils.patterns.decor import *
from pyutils.experimental.error.exceptions import *

try:
	import fcntl
except ImportError:
	fcntl = None

__all__ = ['XFileLock', 'XFileR', 'XFileRW', 'XFile']



class XFileLock(FileLock):
	file_start_pos, file_seek_pos, file_end_pos = range(3)
	_max_preview = 1000

	def __init__(self, *file_path_parts):
		''' args: file_path_parts; ctor('path/to/file') == ctor('path', 'to', 'file') '''
		# todo: allow ([dir0, dir1, ...,] name) args and address improper dir/name detection
		self.dir = ''.join(file_path_parts[:-1])
		self.name = file_path_parts[-1]
		self.mode = 'r'
		self.stream = None
		self.size = len(self)
		self.stat = None
		self.preview = None
		self.lock = None
		FileLock.__init__(self, self.dir+self.name, timeout=0)

	def __len__(self):
		self.size = 0
		if os.path.exists(self.dir + self.name):
			with open(self.dir + self.name, 'r') as self.stream:
				self.stream.seek(0, 2)
				self.size = self.stream.tell()
		return self.size

	def __str__(self):
		''' return cached content preview of file up to _max_preview bytes '''
		stat = os.stat(self.dir+self.name)
		if self.stat == stat: return self.preview  # show cached result until file changes

		self.preview = self[:self._max_preview]
		return self.preview

	def __getitem__(self, *args):
		'''	simplified file read access using python slices convention
			usage:
			with open('test.txt','w') as f:
				f.write('hello\nworld')
			XRFile()[:5] 		# => 'hello'
			XRFile()[-5:] 		# => 'world'
			XRFile()[-100:] 	# => 'hello\nworld'
			XRFile()[100]		# => ''
		'''
		# handle exit conditions; coerce indices to start & stop
		if not os.path.exists(self.dir + self.name):
			raise OSError('File not found.')
		elif isinstance(args[0], slice):
			start, stop = args[0].start, args[0].stop
		elif isinstance(args[0], int):
			start, stop = args[0], args[0]
		else:
			raise InvalidTypeError(arg0=args[0], expected=[slice,int])

		# open and read the given slice portion of file
		result = ''
		with open(self.dir + self.name, self.mode) as self.stream:
			self.stream.seek(0, 2)
			self.size = self.stream.tell()
			# print('start, stop, size = %s' % [start, stop, self.size])
			start = start  if (start is not None)  else 0
			start = start  if (0 <= start)         else max(0, self.size+start)
			stop = stop    if (stop is not None)   else self.size
			stop = stop    if (0 <= stop)          else min(self.size, stop - start)
			read_count = min(self.size, stop - start)
			# print('start, stop, read_count = %s' % [start, stop, read_count])
			self.stream.seek(start, XFile.file_start_pos)
			result = self.stream.read(read_count)

		return result

	def test_locked(self):
		try:
			with self:
				locked = False
		except:
			locked = True

		return locked

	def _acquire(self):
		''' acquire lock on file; reimplmented using FileLock source without file truncation '''
		open_mode = os.O_RDWR | os.O_CREAT
		fd = os.open(self._lock_file, open_mode)

		try:
			fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
		except (IOError, OSError):
			os.close(fd)
		else:
			self._lock_file_fd = fd
		return None

	# method aliases
	__repr__ = __str__


class XFileR(XFileLock): pass


class XFileRW(XFileR):
	def __setitem__(self, key, value): pass
	def write(self, content):
		with open(self.dir + self.name, 'w') as self.stream:
			result = self.stream.write(content)

		return result


# class alias
XFile = XFileRW


if __name__=='__main__':
	# Usages:
	# tested & working
	with open('test.txt','w') as f:
		f.write('hello\nworld')
	f = XFile('test.txt')
	print(f)

	print('len(f) = \'%s\'' % len(f))		# => 11
	print('f[:5] = \'%s\'' % f[:5]) 		# => 'hello'
	print('f[-5:] = \'%s\'' % f[-5:]) 		# => 'world'
	print('f[:] = \'%s\'' % f[:]) 			# => 'hello\nworld'
	print('f[-100:] = \'%s\'' % f[-100:]) 	# => 'hello\nworld'
	print('f[100] = \'%s\'' % f[100]) 		# => 'hello\nworld'
