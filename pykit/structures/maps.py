from pyutils.defs import *
from copy import copy	# todo: attempt to remove dependence on this
import re

log = None
# log = print

def key_keys(key_values):
	''' Gen key => key mapping. Alias is k_k.  key_values.__class__([[k, k] for k, v in key_values]) '''
	result = list(key_values.items()) if isinstance(key_values, dict) else key_values
	result = [[key, key] for key, value in result]
	return key_values.__class__(result)

def key_items(key_values):
	''' Gen key => item mapping. Aliased to k_kv.  key_values.__class__([[k, [k, v]] for k, v in key_values]) '''
	result = list(key_values.items()) if isinstance(key_values, dict) else key_values
	result = [[key, [key, value]] for key, value in result]
	return key_values.__class__(result)

def key_links(key_values):
	''' Gen key => link mapping. Alias is k_vk.  key_values.__class__([[k, [v, k]] for k, v in key_values]) '''
	result = list(key_values.items()) if isinstance(key_values, dict) else key_values
	result = [[key, [value, key]] for key, value in result]
	return key_values.__class__(result)
	
# function aliases
k_k, k_kv, k_vk = key_keys, key_items, key_links

class DefsMaps:
	getter_precedence = ['__getitem__', '__call__', '__getattribute__']
Defs = DefsMaps

def _get_class_fields(inst):
	result = [name for name, var in inst.__class__.__dict__.items() if var.__class__.__name__ == 'member_descriptor']
	return result


def getterof(items, getter=None, getter_precedence=None):
	getter_precedence = getter_precedence or Defs.getter_precedence
	if not getter:
		# getter = first(getter_precedence, lambda getter: hasattr(items, getter))
		getter = first((getter for getter in getter_precedence if hasattr(items, getter)))
	if isinstance(getter, str):
		getter = getattr(items, getter)
	return getter

class Gettable:
	''' A set of items and its associated getter '''
	def __init__(self, items=None, getter=None):
		self.getter = getterof(items, getter)
		self.items = items
	def get(self, key, *args, **kargs):
		result = self.getter(key, *args, **kargs)
		return result
	# method aliases
	__call__ = get


''' Mappings: Interfaces for Map Behaviors
'''

class Mapping:
	''' Map a sequence of keys to an existing item accessor.  Serves to dynamically generate
		ordered values or items from reference dict or value generator.  Implements python
		mapping interface. ie: func(**mapping)
	'''
	def __init__(self, items=None, getter=None, keys=None):
		''' in this module a map is composed of 2 parts a set of keys and an item accessor
			item accessor is usually composed of a set of items and a getter
		
		:param items:
		:param keys:
		:param getter:
		'''
		self.getter = getterof(items, getter)
		
		if isinstance(keys, str):
			keys = keys.split(' ')
		if keys is None:
			# keys = 'getattr' in self._getter.__name__ and _get_class_fields or
			# keys = getattr(items, 'keys', None)
			# keys = getattr(items, 'keys', None)
			if 'getattr' in self.getter.__name__:
				keys = _get_class_fields(items)
			else:
				keys = getattr(items, 'keys', None)()
		self.ikeys = keys
		self.is_enumerable = len([key for key in self.ikeys if isinstance(key, int)]) is 0
		
	# def __iter__(self):			return iter(self.ikeys)
	def __len__(self):			return len(self.keys())
	def __getitem__(self, key):
		if isinstance(key, int) and self.is_enumerable:
			key = tuple(self.ikeys)[key]  # todo: itertools.islice
		result = self.getter(key)
		return result
	def keys(self):
		keys = self.ikeys
		# if isinstance(keys, (list, tuple)):
		# 	keys = iter(self.ikeys)
			# keys = Iter(self.ikeys)
		return keys
	def values(self):	return (self.getter(key) for key in self.ikeys)
	def items(self):	return ([key, self.getter(key)] for key in self.ikeys)
	
	# method aliases
	# __iter__ = keys
	__call__ = __getitem__


class FullMapping:
	''' Behavior emulates a dict or map that is already full with all possible keys.
		On key miss an instance will either returns a static value or generate the value with a function.
		By default an instance DOES NOT store the missing key.  So __contains__(key) can be False
		after returning a value the missing key.
	'''
	store_missing = False
	missing = nothing
	
	def __init__(self, missing=None, *pa, call_missing=None, store_missing=None, **ka):
		''' ctor
			:param missing: defines return value when getitem misses; Accepts either default value or __missing__ func
			:param call_missing: if True use *missing* as a function. Else use as a constant. default=check is_callable
			:param store_missing: when key is missing should default result be stored. default=False
		'''
		# super().__init__(*args, **kws)
		if missing is not nothing:
			self.missing = missing
			self.call_missing = call_missing if call_missing is not None else hasattr(missing, '__call__')
			if store_missing is not None: self.store_missing = store_missing

	def __missing__(self, key, *pa, **ka):
		''' key is not in this collection. Return missing value or missing(key) result. '''
		if self.missing is nothing: raise KeyError(key)
		
		if self.call_missing:
			result = self.missing(key, *pa, **ka)	# get default from *missing* function
		else:
			result = copy(self.missing)				# *missing* is the default value
		
		if self.store_missing:
			self[key] = result
			
		return result
	
class FillMapping(FullMapping):
	''' Behavior emulates a dict or map that is already full with all possible keys.
		On key miss an instance will either returns a static value or generate the value with a function.
		By default an instance DOES store the missing key.  So __contains__(key) will be True
		after returning a value the missing key.
	'''
	store_missing = True


class FullListing(FullMapping):
	''' augments lists with call to __missing__ function for out-of-range indices '''
	def __getitem__(self, index):
		''' return self[index]. in case of failure return self.__missing__(index). '''
		try:
			result = super(FullMapping, self).__getitem__(index)
		except:
			result = self.__missing__(index)
		return result

class FullKeyArg:
	def __missing__(self, key):
		return key
	

class CollMapping:
	''' Augment an items sequence with map functionality and preserve the underlying structure.
		Note: ItemsMap.__getitem__ is inefficient.  Adding a hash of keys is a possible O(1) solution.
	'''
	def __init__(self, items):
		self._items = items
	def __len__(self):		return len(self._items)
	def keys(self):			return map(item_key, self._items)
	def values(self):		return map(item_value, self._items)
	def items(self):		return self._items
	def get_paired(self, search, count=0, default=arg_missing, search_keys=True):
		''' find value for *count* occurrence of *search* key. Inefficient O(n) performance similar to list.index. '''
		# prepare items iterable
		items = self._items
		if 0 > count:
			items = reversed(self._items)
			count = ~count
		
		# find value for *count* occurrence of *search_key*
		result = arg_missing
		for key, value in items:
			found, paired = (key, value) if search_keys else (value, key)
			if search == found:
				if count is 0:
					result = paired
					break
				count -= 1
				
		if result is arg_missing:
			raise KeyError()
		return result
	# method aliases
	__iter__ = keys
	__getitem__ = get_paired


class SortMapping:
	sort_by_keys = lambda item: item[0]
	def sorted(self, factory=None, key=sort_by_keys, **sort_kws):
		sorted_items = sorted(self.items(), key=key, **sort_kws)
		factory = factory or self.__class__
		result = factory(sorted_items)
		return result
	def sort(self, **kws):
		''' Extract all items to list; Sort item list; Reinsert each item '''
		items = sorted(list(self.items()), **kws)
		self.clear()
		for k, v in items:
			self[k] = v


class KeysMapping(FullMapping):
	''' map a set of values as keys of a set given the function *to_key*
		Note: references dict-like datastructure not defined here but required by subclass.
	'''
	to_key = coerce_key = staticmethod(asis)
	set_key = lambda value, key: None
	
	def __init__(self, values=None, to_key=None, set_key=None, missing=nothing, call_missing=None, store_missing=None,
		**ka_items):
		''' ctor
			:param values: sequence of values (and/or keys) in set
			:param to_key: converter function which gets a key for each value
			:param set_key:
			:param missing: when given use as value or value getter for missing key, Else raise KeyError.
			:param call_missing: if True use *missing* as a function. Else use as a constant. default=check is_callable
			:param store_missing: when key is missing should default result be stored. default=False
		'''
		# super().__init__(**ka)
		super().__init__(missing=missing, call_missing=call_missing, store_missing=store_missing)
		if to_key is not None:
			self.to_key = to_key
		if set_key is not None:
			self.set_key = set_key
			
		values and list(map(self.add, values))
		ka_items and [self.__setitem__(*item) for item in ka_items.items()]
	def __contains__(self, value):
		return super().__contains__(self.to_key(value))
	def __setitem__(self, key, value):
		''' __setitem__ is allowed but circumvents (and can violate) internal enforcement of this mapping as a set '''
		if self.to_key(value) != key:
			self.set_key(value, key)  # conflict found; to_key(value) != key; call users set_key to resolve
			assert self.to_key(value) == key, 'Conflict in __setitem__(key, value). to_key(value) != key'
		self.add(value)
	def add(self, value):			super().__setitem__(self.to_key(value), value)
	def combine(self, values):
		_ = list(map(self.add, values))
		return self
	def union(self, values):
		result = copy(self)
		result += values
		return result
	def discard(self, value, on_miss=None):		return self.pop(self.to_key(value), on_miss)
	def subtract(self, values):
		_ = list(map(self.discard, values))
		return self
	def difference(self, values):
		result = copy(self)
		result -= values
		return result
	
	# method aliases
	__iadd__ = combine
	__add__ = union
	__isub__ = subtract
	__sub__ = difference


class KeysOrderMapping(KeysMapping):
	def __init__(self, *pa, **ka):
		self._order = []
		self._omitted = set()
		super().__init__(*pa, **ka)
	
	def _omit(self):
		if self._omitted:
			self._order = [key for key in self._order if key not in self._omitted]
			self._omitted.clear()
		return self._order
	def __setitem__(self, key, value):
		if self.to_key(value) != key:
			self.set_key(value, key)
		self.add(value)
	def add(self, value):
		if not value in self:
			self._order.append(self.to_key(value))
			super().add(value)
	def pop(self, key):
		result = super().pop(key)
		self._omitted.add(key)  # do lazy omission of key from _order; pop is still O(1); __iter__ is still O(n)
		return result
	def clear(self):
		super().clear()
		self._omitted.clear()
		
		
	def values(self):
		self._omitted and self._omit()
		return (self.__getitem__(key) for key in self._order)
	def keys(self):
		self._omitted and self._omit()
		return iter(self._order)
	
	# method aliases
	__iter__ = values


class ValuesMapping:
	''' maps each key to a value list '''
	_getter = None  # valid getters: 'last', 'first', None or callable
	
	def __init__(self, *pa, insert_op=None, getter=None, **ka):
		self._insert_op = insert_op
		self.first = Mapping(self, getter=self.get_first)
		self.last = Mapping(self, getter=self.get_last)
		getter = getter or self._getter
		if isinstance(getter, str):
			getter = self.__dict__[getter]
		elif hasattr(getter, '__call__'):
			getter = getter(self)
		self.getter = getter
		super().__init__(*pa, **ka)
		
	def __setitem__(self, key, value):
		if key not in self:
			super().__setitem__(key, [])
		values = super().__getitem__(key)
		values.append(value)
	def __getitem__(self, key, *pa, **ka):
		result = (self.getter or super()).__getitem__(key, *pa, **ka)
		return result
	def items(self, *pa, **ka):
		result = (self.getter or super()).items(*pa, **ka)
		return result
	def values(self, *pa, **ka):
		result = (self.getter or super()).values(*pa, **ka)
		return result
	def get_first(self, key, default=None):
		if key not in self : return default
		result = super().__getitem__(key)
		
		if len(result) == 0: return default
		result = super().__getitem__(key)[0]
		return result
	def get_last(self, key, default=None):
		if key not in self : return default
		result = super().__getitem__(key)
		
		if len(result) == 0: return default
		result = super().__getitem__(key)[-1]
		return result
	def update(self, items):
		if isinstance(items, dict):
			items = items.items()
		for key, value in items:
			self[key] = value
	
	# method aliases
	getl, getr = get_first, get_last


class KeyRemapping:
	''' apply a function *coerce_key* to a map which transforms keys in set & get operations.
		alters the output values for the item getter and iterator functions: get,  __getitem__, values, & items
		use super to access underlying unmodified values.
	'''
	coerce_key = staticmethod(asis)
	
	def __init__(self, *args, **kargs):
		super().__init__(*args, **kargs)
		for key, value in list(self.items()):
			coerced_key = self.coerce_key(key)
			if key != coerced_key:
				self[coerced_key] = self.pop(key)
	def __contains__(self, key):				return self.coerce_key(key) in self.keys()
	def get(self, key, *args, **kargs):
		print('KeyMap.get')
		return super().get(self.coerce_key(key), *args, **kargs)
	def __getitem__(self, key, *args, **kargs):
		log and log('KeyMap.__getitem__')
		return super().__getitem__(self.coerce_key(key), *args, **kargs)
	def __setitem__(self, key, value):
		log and log('KeyMap.__setitem__')
		super().__setitem__(self.coerce_key(key), value)
	def update(self, items):
		print('KeyMap.update')
		for key, value in items.items():
			self[key] = value


class ValueRemapping:
	''' apply a function *to_value* to a map which transforms mapped values in get operations.
		alters the output values for the item getter and iterator functions: get,  __getitem__, values, & items
		use super to access underlying unmodified values.
	'''
	class gives:
		key, value, item, item_args = 'key', 'value', 'item', 'item_args'
		arg_inds = {key:0, value:1, item:slice(None, None)}
		give_args = classmethod(lambda cls, give, item_args: item_args[cls.arg_inds[give]])
		
		
	def __init__(self, items=None, to_value=None, given=gives.value, **kargs):
		''' ctor
			:param items:
			:param to_value:
			:param given:
		'''
		self.to_value = to_value
		self.given = given == self.gives.item_args and self.gives.item or given
		self.unpack = given == self.gives.item_args
		super().__init__(items or {}, **kargs)
	def __getitem__(self, key):
		# result/value for key not needed if only key is given
		result = self.given != self.gives.key and super().__getitem__(key)
		if self.to_value:
			item_args = key, result
			given_item = self.gives.give_args(self.given, item_args)
			result = self.to_value(*given_item) if self.unpack else self.to_value(given_item)
		return result
	def values(self):
		result = given_items = getattr(super(), self.given+'s')()
		if self.to_value:
			result = (self.to_value(*v) if self.unpack else self.to_value(v) for v in given_items)
		return result
	def items(self):
		result = given_items = getattr(super(), self.given+'s')()
		if self.to_value:
			result = ((k, self.to_value(*v) if self.unpack else self.to_value(v)) for k, v in zip(self.keys(), given_items))
		return result

# todo: refactor mappings as abstract base class to enforce method implementation of keys(), etc



class StackMap:
	''' multiple maps unified to behave as a single map with a superset of keys.
		key access references the first map containing the key.
	'''
	def __init__(self, stack=None, missing=arg_missing, call_missing=None, map_hint=None):
		''' ctor
			:param stack: sequence of maps with the first taking precedence
			:param missing: fall through value result when no maps contain key
			:param call_missing:
			:param map_hint: todo: a mechanism to narrow applicable maps when accessing a key
		'''
		self.stack = stack or []
		self.fall_thru = None if missing is arg_missing else FullMap(missing, call_missing=call_missing)
		self.map_hint = map_hint
		self.known_keys = []
	def locate(self, key, allow_fall_thru=True, error=False):
		''' identify the map in which a given item resides '''
		mapping = next(iter((mapping for mapping in self.stack if key in mapping)), None)
		mapping = self.fall_thru if mapping is None and allow_fall_thru else mapping
		if error and mapping is None:
			raise KeyError(key)
		return mapping
	def get(self, key, default=arg_missing):
		mapping = self.locate(key, False)
		result = arg_missing.fillin(default) if mapping is None else mapping[key]
		return result
	def __getitem__(self, key):
		mapping = self.locate(key, error=True)
		result = mapping[key]
		return result
		
	
class StackRemap(ValueRemapping, StackMap):
	def __init__(self, stack=None, *args, to_value=None, given=ValueRemapping.gives.value, **kargs):
		ValueMapping.__init__(self, to_value, given)
		StackMap.__init__(self, stack, *args, **kargs)


class AttrEnum:
	''' Remaps attribute assignment to dict assignment such that after assignment
		attrenum.var == dict(**attrenum)['var'].  It maintains attribute vars distinct from dict items
		within instance.
	
		One significant challenge to allowing attr access pass-through to item access is that internally the
		class instance still needs to assign member vars in a way that is simple and unambiguous.
		Attribute assignment (typically via self.__dict__.__setitem__) is used if one of the following
		conditions is satisfied:
		- an existing instance attribute
		- assignments within the constructor
		- the attr name matches the self._attr_ptrn regular expression
		
		If none of these conditions are satisfied when assigning an attribute the variable is assigned in dict
		contents via self.__setitem__.
	'''
	_attr_ptrn = '_[^a-zA-Z0-9_]+'  # always apply as an attribute when matching this pattern
	_prefer_setitem = False
	_sort = False
	
	def __init__(self, *args, _attr_ptrn=None, _sort=None, **kws):
		self._attr_ptrn = _attr_ptrn or self.__class__._attr_ptrn
		super().__init__(*args, **kws)
		self._prefer_setitem = True  # after this point use __getitem__ where possible
		if self._sort if _sort is None else _sort:
			SortMapping.sort(self)
	def get_name_setter(self, name):
		prefer_setitem = self._prefer_setitem and not bool(re.search(self._attr_ptrn, name))
		setter = prefer_setitem and self.__setitem__ or super().__setattr__
		return setter
	def __setattr__(self, attr, value):
		setter = self.get_name_setter(attr)
		return setter(attr, value)
	def __getattr__(self, attr):
		getter = attr in self.__dict__ and self.__getattribute__ or self.__getitem__
		result = getter(attr)
		return result



''' Maps: Dictionary Implementations of Mapping interfaces
'''

class AttrMap(AttrEnum, dict):
	''' Extend dict to allow access to items as members. As in d['attr'] == d.attr '''
class FullMap(FullMapping, dict):
	''' Extend dict to allow getitem key access misses fall back to __missing__ rather than throwing exception '''
class FillMap(FillMapping, dict):
	''' Extend dict to allow getitem key access misses fall back to __missing__ rather than throwing exception '''
class FullAttrMap(FullMapping, AttrEnum, dict):
	''' Extend dict to allow:
		1. access to items as members. As in d['attr'] == d.attr
		2. getitem key access misses fall back to __missing__ rather than throwing exception
	'''
class FillAttrMap(FillMapping, AttrEnum, dict):
	''' Extend dict to allow:
		1. access to items as members. As in d['attr'] == d.attr
		2. getitem key access misses fall back to __missing__ rather than throwing exception
	'''
class KeysMap(KeysMapping, dict): pass
class KeysOrderMap(KeysOrderMapping, dict): pass
class ValuesMap(ValuesMapping, dict): pass
class KeyRemap(KeyRemapping, dict): pass
class ValueRemap(ValueRemapping, dict): pass



''' Class Aliases
'''

Map = Mapping
ItemsMap = CollMapping
SetMap = KeysMap
KeysOMap = KeysOrderMap
KeysOMapping = KeysOrderMapping


# todo: obsolete; replace all instances of *ClassName*dict with *ClassName*Map as the former is a misnomer
Fulldict = FullMap
Filldict = FillMap
Attrdict = AttrMap
FullAttrdict = FullAttrMap
FillAttrdict = FillAttrMap
ValuesDict = ValuesMap
Remapdict = RemapMap = ValueRemap

# todo: eliminate obsolete 'ValueMapping' aliases
ValueMapping = ValueRemapping
KeyMapping = KeyRemapping