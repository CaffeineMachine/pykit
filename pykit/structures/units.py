from __future__ import annotations

import re

from datetime import datetime as dateclock, timedelta as deltatime, date, time
from copy import copy
from dataclasses import dataclass, field
from sortedcontainers import SortedDict
from typing import Any, Callable, Union as Or

from pyutils.defs import *
from pyutils.structures.strs import Strs, TStrs
from pyutils.structures.maps import AttrMap


# todo: RELOCATE
import operator as op

''' Utility Structures
'''

@dataclass
class IterProp:
	''' Iterator from instance attribute, Immutable access. '''
	name: str
	convert: Callable = None
	
	def __get__(self, obj, cls):
		result = self if obj is None else getattr(obj, self.name)
		result = self.convert and map(self.convert, result) or iter()
		return result
	
	def __call__(self, obj):
		result = self if obj is None else getattr(obj, self.name)
		result = self.convert and map(self.convert, result) or iter()
		return result
	

@dataclass
class Operand:
	''' the quantity on which an operation is to be done '''
	value: Any = None
	operator: Callable = asis
	
	
@dataclass(repr=False)
class Operands:
	values: Any = None
	_operator: Callable = asis
	
	def __getitem__(self, ind):
		result = self.operator(self.values[ind])
		return result
	__iter__ = IterProp('values')
	# __getitem__ = property(lambda self: self.values.__getitem__)

	operator = property(lambda self: self._operator, lambda self, value: setattr(self, '_operator', value))


class Accums(Operands):
	polarity = 1
	
class Negates(Operands):
	operator = _operator = op.__neg__
	# operator = op.__neg__
	polarity = -1
	
	# def __post_init__(self):
	# 	self.__dict__.pop('operator')
	
	__iter__ = IterProp('values', operator)
	# __getitem__ = property(lambda self: [*map(self.operator, self.values)].__getitem__)


''' Defines
'''

class DefsUnits:
	present_dt = deltatime()
Defs = DefsUnits

''' Generic Unit classes
'''

@dataclass(frozen=True)
class Denom:
	''' A singular named unit and its representative instance '''
	name: str = ''
	abrv: str = ''
	base: Any = None
	metric_name: str = 'Metric'
	
	def __getattr__(self, attr):
		return getattr(self.base, attr)
	def __mul__(self, value: int) -> Unit: return self.metric.Unit(denom=self, value=value)
	def __div__(self, value: int) -> Unit: return self.metric.Unit(denom=self, value=1/value)
	def __lt__(self, other):
		return self.base < other.base
	def __dir__(self): return Strs('name abrv base')
	
	# properties
	@property
	def metric(self): return globals()[self.metric_name]
	
class Units: pass
class Metric:
	getter = lambda: Metric
	Unit = lambda *pa, **ka: Unit(*pa, **ka)
	Units = lambda *pa, **ka: Units(*pa, **ka)
	
	# sequence of immutable denominators
	denoms = []
	
	# associated unit strings as abbreviations and names and the associative map
	abrvs = []
	names = []
	
	''' Example:
	denoms = (
		i, ki, mi,  ) = (
		ion, kilo_ion, mill_ion,  ) = (
		Denom('ion', 'u', 			Decimal(10**0), getter),
		Denom('kilo_ion', 'ku', 	Decimal(10**3), getter),
		Denom('mill_ion', 'mu', 	Decimal(10**6), getter),  )
	abrvs = Strs('i ki mi')
	names = Strs('ion kilo_ion mill_ion')
	'''
	
	# dict access to unit representations by name or abrv
	denom	= AttrMap({ k: v 		for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.denom.mi == Denom('mill_ion', ...)
	name	= AttrMap({ k: v.name	for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.name.mill_ion == 'mi'
	abrv	= AttrMap({ k: v.abrv	for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.abrv.mill_ion == 'mill_ion'
	base	= AttrMap({ k: v.base	for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.base.mi == Decimal(10**6)



class Unit:
	''' A named unit, its representative instance and a quantity '''
	_metric = Metric
	_fmt_abrv = '{value}{abrv}'
	_fmt_name = '{value}{name}s'
	_unit_factory = lambda cls, name, value: cls(denom=cls._metric.denom[name], value=value)

	def __init__(self, denom, value=1, **ka):
		self.value = value
		self.denom = denom
	def format(self, fmt_spec=None, abrv=True)->str:
		fmt = abrv and self._fmt_abrv or self._fmt_name
		result = fmt.format(**self.attr_dict)
		return result
	def format_name(self) -> str:	return self.format(abrv=False)
	def __repr__(self):				return f'{self.__class__.__name__}({self.format()!r})'
	def __hash__(self): 			return hash((self.denom, self.value))
	def __getattr__(self, attr):
		''' imbue Unit classes with methods and attributes of their base_view '''
		result = getattr(self.base_view, attr)
		return result
	def __neg__(self) -> Unit:
		return self.__class__(self.denom, -self.value)
	def __lt__(self, other:Unit) -> bool:
		return isinstance(other, Unit) and self.base_view < other.base_view
	def __eq__(self, other:Unit) -> bool:
		return isinstance(other, Unit) and self.base_view == other.base_view
	def __add__(self, other:Unit) -> Unit:
		result = self.metric.Units(other)
		result += self
		return result
	def __radd__(self, other:Unit) -> Unit:
		result = other + self.base_view
		return result
	def __sub__(self, other:Unit) -> Unit:
		result = -self.metric.Units(other)
		result += self
		return result
	def __rsub__(self, other:Unit) -> Unit:
		result = other - self.base_view
		return result
	def __mul__(self, value:int) -> Unit: return self.__class__(denom=self.denom, value=self.value * value)
	def __div__(self, value:int) -> Unit: return self.__class__(denom=self.denom, value=self.value / value)
	@classmethod
	def notation_units(cls, notation, factory=None):
		units = re.findall(cls._metric.notation_ptrn, notation)
		factory = factory or cls._unit_factory
		if factory:
			units = [factory(cls, name, value) for value, name in units]
		return units
	@classmethod
	def notation_unit(cls, notation, at_index=0, factory=None):
		units = cls.notation_units(notation, factory=factory)
		unit = units[at_index]
		return unit
	def __reduce_ex__(self, protocol=None):
		''' serialize this instance '''
		return (self.__class__, (str(self),))

	# method aliases
	__rmul__ = __mul__
	__rdiv__ = __div__
	__str__ = format
	_reduce_encode = format_name
	
	# properties
	name		= property(lambda self: self.denom.name)
	abrv		= property(lambda self: self.denom.abrv)
	metric_name	= property(lambda self: self.denom.metric_name)
	base		= property(lambda self: self.denom.base)
	base_view	= property(lambda self: self.denom.base * self.value)
	metric		= property(lambda self: globals()[self.metric_name])
	@property
	def attr_dict(self):
		result = dict(**self.__dict__, **{field:self.denom.__dict__[field] for field in self.denom.__dataclass_fields__})
		return result
	attrs = attr_dict  # legacy property alias



''' DeltaTime classes
'''

class DeltaTimeUnit(Unit):
	''' A discrete delta time unit and quantity.
		An interpretation of Unit as an amount of change in time.
	'''
	denom_value_factory = lambda cls, name, value: (DeltaTime.denom[name], int(value))

	def __init__(self, unit=None, denom=None, value=1, **kws):
		if isinstance(unit, deltatime):
			unit = DeltaTimeUnits.timedelta_units(unit)[0]  # use 1st and largest unit
		
		if unit is denom is None:
			denom, value = DeltaTime.second, 0
		elif typelike(unit, Unit):
			denom, value = unit.denom, unit.value
		elif isinstance(unit, str):
			denom, value = self.notation_unit(unit, factory=self.__class__.denom_value_factory)
		elif not isinstance(unit, type(None)):
			raise ValueError(unit)
		
		super().__init__(denom=denom, value=value)
	@property
	def base_view(self): return self.base * self.value
	@property
	def metric(self): return globals()[self.metric_name]

	# method aliases
	__hash__ = Unit.__hash__

class DeltaTimeUnits(Units):
	''' An aggregate of distinct DeltaTimeUnits '''
	_fmt_abrv = '{value}{abrv}'
	_fmt_name = '{value}{name}s'
	
	joiner = lambda parts: ' '.join([f'{count}{unit}' for delta_t, unit, count in parts])  # todo: obsolete
	
	def __init__(self, units=None, **timedelta_kws):
		# coerce accepted inputs
		if  isinstance(units, DeltaTimeUnits):
			self.__dict__.update(units.__dict__)
			return
		elif isinstance(units, DeltaTimeUnit):
			units = [units]
		elif isinstance(units, Denom):
			units = [DeltaTimeUnit(denom=units)]
		elif isinstance(units, type(None)):
			units = DeltaTimeUnits.timedelta_units(deltatime(**timedelta_kws))
		elif isinstance(units, deltatime):
			units = DeltaTimeUnits.timedelta_units(units)
		elif isinstance(units, str):
			units = DeltaTimeUnit.notation_units(units)
		elif not isinstance(units, (list, tuple)):
			raise ValueError(f'units={units!r}, **{timedelta_kws}')
		
		for ind, unit in enumerate(units):
			if not isinstance(unit, DeltaTimeUnit):
				units[ind] = DeltaTimeUnit(unit)
		
		# member variable assignment
		units.sort()
		self._units = units
		self._values = SortedDict([(unit.denom, unit.value) for unit in units or []])
		self.total_tdelta = sum([denom.base * int(value) for denom, value in self.values], deltatime())
		self.polarity = 1
	def __iter__(self):		return iter(self.units)
	def total_seconds(self):	return self.total_tdelta.total_seconds()
	def __str__(self):
		result = ' '.join([self._fmt_abrv.format(**unit.attr_dict) for unit in self.units])
		return result
	def __repr__(self):
		result = f'{self.__class__.__name__}({str(self)!r})'
		return result
	def __iadd__(self, other):
		other = self.__class__(other)
		for denom, value in other.values:
			value = self._values.get(denom, 0) + value
			if value:
				self._values[denom] = value
			else:
				self._values.pop(denom)
		return self
	def __add__(self, other):
		result = copy(self)
		if isinstance(other, (dateclock, date, time)):
			result = self.total_tdelta
		result += other
		return result
	def __lt__(self, other):
		return self.total_tdelta < other.total
	@staticmethod
	def timedelta_units(t_delta, join=None):
		''' takes a timedelta and returns a list of discrete time units.  timedelta(seconds=5000)  =>  [1H. 23M. 20s] '''
		operands_fab = t_delta >= Defs.present_dt and Accums or Negates
		t_delta = abs(t_delta)
		
		units = []
		for base, abrv in zip(DeltaTime.denoms, DeltaTime.abrvs):
			if not t_delta: 		break
			if t_delta < base.base: continue
			
			count = int(t_delta / base.base)
			unit = Unit(denom=base, value=count)
			units.append(unit)
			t_delta -= count * base.base
			
		# prepare result: ensure non-empty and apply given join to unit sequence
		units = operands_fab(units or [Unit(Dt.denom.s, 0)])
		units = join(units) if join else units
		return units
	
	# method aliases
	__radd__ = __add__
	
	# properties
	units = property(lambda self: self._units)
	values = property(lambda self: self._values.items())
	seconds = property(lambda self: self.total_tdelta.total_seconds())
	

class DeltaTime(Metric):
	''' Time delta unit names and abbreviations '''
	Unit = DeltaTimeUnit
	Units = DeltaTimeUnits
	
	notation_ptrn = r'([+-]?\d*)(\w+)s?'
	fmt_abrv = '{value}{abrv}'
	fmt_name = '{value}{name}s'
	
	denoms = (
		Denom('year', 'y', 			deltatime(days=366), 'DeltaTime'),	# todo: cope with variable deltatime quantities
		Denom('month', 'm', 		deltatime(days=31), 'DeltaTime'),	# todo: cope with variable deltatime quantities
		Denom('week', 'w', 			deltatime(days=7), 'DeltaTime'),
		Denom('day', 'd', 			deltatime(days=1), 'DeltaTime'),
		Denom('hour', 'H',			deltatime(hours=1), 'DeltaTime'),
		Denom('minute', 'M',		deltatime(minutes=1), 'DeltaTime'),
		Denom('second', 's',		deltatime(seconds=1), 'DeltaTime'),
		Denom('millisecond', 'ms',	deltatime(milliseconds=1), 'DeltaTime'),
		Denom('microsecond', 'us',	deltatime(microseconds=1), 'DeltaTime'),  )
	units = (
		y, m, w, d, H, M, s, ms, us,  ) = (
		year, month, week, day, hour, minute, second, millisecond, microsecond,  ) = [*(
		DeltaTimeUnit(denom=denom) for denom in denoms)]
	
	# associated unit strings as abbreviations and names and the associative map
	abrvs = TStrs('y m w d H M s ms us')
	names = TStrs('year month week day hour minute second millisecond microsecond')
	
	# dict access to unit representations by name or abrv
	name	= AttrMap({ k: v.name	for k, v in zip(names+abrvs, denoms*2)})	# usage: Dt.name.year == 'year'
	abrv	= AttrMap({ k: v.abrv	for k, v in zip(names+abrvs, denoms*2)})	# usage: Dt.abrv.month == 'm'
	base	= AttrMap({ k: v.base	for k, v in zip(names+abrvs, denoms*2)})	# usage: Dt.base.day == timedelta(day=1)
	denom	= AttrMap({ k: v 		for k, v in zip(names+abrvs, denoms*2)})	# usage: Dt.denom.day == Denom('day', ...)
	unit	= AttrMap({ k: v 		for k, v in zip(names+abrvs, units*2)})		# usage: Dt.unit.day == Unit('day', ...)
	
	@staticmethod
	def coerce_timedelta(obj: Or[str,DeltaTimeUnit,DeltaTimeUnits,deltatime]) -> deltatime:
		''' return a timedelta coerced from possible input types: str, DeltaTimeUnit, DeltaTimeUnits, timedelta '''
		if isinstance(obj, deltatime):
			return obj
		elif isinstance(obj, DeltaTimeUnit):
			result = obj.base_view
		elif isinstance(obj, DeltaTimeUnits):
			result = obj.total_tdelta
		elif isinstance(obj, str):
			result = DeltaTimeUnit(obj).base_view
		else:
			raise TypeError(str(obj))
		return result
Dt = DeltaTime
Unit._metric = DeltaTime
Units._metric = DeltaTime


if __name__=='__main__':
	from pyutils.experimental.inspect.tracelog import *
	
	# testing DeltaTimeUnit
	print(Dt.w)                                   # unit shorthand
	print(Dt.w*2)                                 # unit multiplication
	print(dateclock.min + Dt.w)                    # unit dateclock arithmetic
	
	# testing DeltaTimeUnits
	deltat = Dt.m*1 + Dt.d*2 + Dt.H*3 + Dt.M*4    # advanced unit arithmetic
	deltat_tmin = dateclock.min + deltat           # dateclock arithmetic like timedelta
	
	print(f'{"deltat":14s} = {deltat!r:34s} <= Dt.m*1 + Dt.d*2 + Dt.H*3 + Dt.M*4')  # FIXME: units listed in reverse
	print(f'{"tmin + deltat":14s} = {deltat_tmin!r:34s} <= {deltat_tmin}')  # FIXME: didnt add hours or minutes
	
	# testing DeltaTime reprs
	str_input = '3m 4y'
	timedelta_input = deltatime(seconds=5000)
	
	str_dt = DeltaTimeUnit(str_input)
	str_dts = DeltaTimeUnits(str_input)
	td_dts = DeltaTimeUnits(timedelta_input)
	
	print(f'{str_input!r:32s} => str: {str_dt!s:10s} repr: {str_dt!r}')
	print(f'{str_input!r:32s} => str: {str_dts!s:10s} repr: {str_dts!r}')
	print(f'{timedelta_input!r:32s} => str: {td_dts!s:10s} repr: {td_dts!r}')
	

