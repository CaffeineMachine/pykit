from pyutils.structures.members import *

# from pyutils.defs.worddefs import asis
# import re
#
# class AttrGetter(str): pass
# class CallGetter:
# 	def __init__(self, callable):
# 		self.callable = callable
# 	def __call__(self, *pa, **ka):
# 		callable = self.callable
# 		return callable(*pa, **ka)
# # IndexGetter = object
#
# class RefsCls:
# 	getters = (get_index, get_attribute, get_callable) = range(1,4)
# 	_classes = {get_index:(int, slice, tuple, list), get_attribute:str, get_callable:object}
#
# 	def __init__(self, key=None, getter_type=get_attribute, prev_keys=()):
# 		key = [] if key is None else [self.coerce_key(key, getter_type)]
# 		self.keys = list(prev_keys) + key
# 	def coerce_key(self, key, getter_type=None):
# 		typed_key = key
# 		if isinstance(key, (AttrGetter, CallGetter)):
# 			typed_key = key
# 		elif getter_type is self.get_attribute or (getter_type is None and isinstance(key, str)):
# 			typed_key = AttrGetter(key)
# 		elif getter_type is self.get_callable or (getter_type is None and hasattr(key, '__call__')):
# 			typed_key = CallGetter(key)
# 		elif getter_type is self.get_index or getter_type is None:
# 			typed_key = key
# 		return typed_key
# 	def add(self, key, getter_type=get_attribute):
# 		self.keys.append(self.coerce_key(key, getter_type))
# 	def __repr__(self):
# 		result = 'Keys'
# 		for key in self.keys:
# 			getter_type = RefsCls.get_attribute if isinstance(key, AttrGetter) else RefsCls.get_index
# 			if getter_type is RefsCls.get_index:
# 				result += f'[{key}]'
# 			else:
# 				result += f'.{key}'
# 		return result
# 	def __iter__(self):
# 		yield from self.keys
#
# 	# todo: simplify by using a dedicated Attrfab as a factory
# 	''' Factory functions '''
# 	def __getattr__(self, attr):
# 		return self.__class__(AttrGetter(attr), prev_keys=self.keys)
# 	def __getitem__(self, ind):
# 		return self.__class__(ind, getter_type=self.get_index, prev_keys=self.keys)
#
# 	''' Object Reference Accessors '''
# 	def get(self, obj, *pa, **ka):
# 		result = obj
# 		for key in self.keys:
# 			if isinstance(key, AttrGetter):
# 				result = getattr(result, key)
# 			elif isinstance(key, CallGetter):
# 				result = key(result, *pa, **ka)
# 			else:
# 				result = result.__getitem__(key, *pa, **ka)
# 		return result
# 	def get_all(self, objs):
# 		yield from map(self.get, objs)
# 	def __get__(self, obj, cls):
# 		obj = cls if obj is None else obj
# 		result = self.get(obj)
# 		return result
# 	def __set__(self, obj, cls):
# 		print('RefsCls.__set__({obj}, {cls})')
# 		raise NotImplemented()
#
# 	# method aliases
# 	__call__ = get
#
# class IRefsCls(RefsCls):
# 	__call__ = RefsCls.get_all
#
# Name = Refs = RefsCls()
# IName = IRefs = IRefsCls()
#
#
# class _Sig(RefsCls):
# 	def __str__(self):
# 		return re.sub(r'^Keys\.?', '', super().__str__())
# 	__repr__ = __str__
# Sig = _Sig()
