# from pyutils.extend.datastructures import Filldict
# from pyutils.defs import *
from pyutils.structures.maps import FillMap
from pyutils.patterns.event import *
from pyutils.patterns.poly	import *
from pyutils.patterns.autoattrs import autoattrs, attrib, attrs
from pyutils.patterns.capsules import *
from pyutils.structures.strs import *
from typing import List


class DefsModel:
	delta_keys = TStrs('new old ind')
	delta_item_keys = TStrs('ind new old')
	added_keys = TStrs('new')
	dropped_keys = TStrs('old')
Defs = DefsModel

class Model: pass


@autoattrs(cmp=False)
class ModelEvent:
	_source: Model = nothing
	_origin: Model = nothing  # readonly; defaults to souce when unset
	
	# def __str__(self): return self.__class__.__name__
	def __attrs_post_init__(self):
		self._origin = self._source if self._origin is None else self._origin
	def set(self, **kargs):
		self.__dict__.update(kargs)
		return self
	def set_source(self, source:Model):
		if self._origin is nothing:
			self._origin = source
		self._source = source
		
	# properties
	source = property(Name._source, set_source)
	origin = Name._origin
	name = property(lambda self: self.__class__.__name__)
	event_phase = property(
		lambda self: getattr(self, '_event_phase', 'on_event'),
		lambda self, phase: setattr(self, '_event_phase', phase),
	)
	
@autoattrs(cmp=False)
class Delta(ModelEvent):
	new: object = nothing
	old: object = nothing
	
	count = 'singular'
	
@autoattrs(cmp=False)
class DeltaSeq(Delta):
	''' Simple nestable dataclass capable of expressing a wide variety of modifications to data structures. '''
	_ind: object = nothing
	_added: object = nothing
	_dropped: object = nothing
	
	ind, added, dropped = (nothing,)*3
	
	# def __init__(self): pass
	def __attrs_post_init__(self):
		ind = self.__dict__.pop('_ind', nothing)
		added = self.__dict__.pop('_added', nothing)
		dropped = self.__dict__.pop('_dropped', nothing)
		if not (nothing is ind is added):
			self.update(ind=ind, added=added)
		if not (nothing is ind is dropped):
			self.update(ind=ind, dropped=dropped)
		# if nothing is ind is added is dropped:
		# 	self.change(new=self.new, old=self.old)
			

	def append(self, *value, keys:List[str]='new', **kitem):
		''' append item '''
		self.extend(*value, keys=keys, multi=False, **kitem)
	def extend(self, *values, keys:List[str]='new', multi=True, **kitems):
		''' extend items '''
		# prepare inputs
		keys = isstr(keys) and keys.split(' ') or keys  # coerce arg_keys to list of strs
		kitems.update(dict(zip(keys, values)))
		'added' in kitems and kitems.__setitem__('new', kitems.pop('added'))
		'dropped' in kitems and kitems.__setitem__('old', kitems.pop('dropped'))
		
		items_lengths = 1
		filler = [nothing]
		if multi:
			# detect and ensure correct length
			# assert kitems.get('ind', nothing) is self.ind, 'has_ind must match for existing data and added data'
			items_lengths = kseq([val for val in kitems.values() if val is not nothing], len)  # set of all item lengths should be singular
			assert len(items_lengths) == 1, 'items args must have uniform length'
			items_lengths = first(items_lengths)
			filler = [nothing] * (multi and items_lengths or 1)
		
		count = (isint(self.count) and self.count or 0)
		for key in Defs.delta_item_keys:
			member, value = getattr(self, key), kitems.get(key, nothing)
			if not (nothing is value is member):
				assert not (key is 'ind' and count), 'ind cannot be made iterable after init'
				setattr(self, key, [nothing]*count if member is nothing else member)
				getattr(self, key).extend(value or filler)
			
		self.count = count + items_lengths
	
	def change(self, new=nothing, old=nothing):
		assert (new, old).count(nothing) != 2, f'non-nothing arg required {(new, old)}'
		self.new = new
		self.old = old
	def update(self, ind, new=nothing, old=nothing, added=nothing, dropped=nothing):
		''' similar to change except for an itemized DeltaSeq '''
		assert (new, old, added, dropped).count(nothing) != 4, f'non-nothing arg required {(new, old, added, dropped)}'
		assert (
			nothing in (new, added) and
			nothing in (old, dropped) and
			nothing in (added, dropped)), f'received a conflicting pair of args: {(new, old, added, dropped)}'
		
		# reinterpret and reduce DeltaSeq.added & DeltaSeq.dropped args in fundamental terms of Delta.new & Delta.old
		new = new if added is nothing else added
		old = old if dropped is nothing else dropped
		multi = any([hasattr(dseq_arg, '__iter__') for dseq_arg in [ind, added, dropped]])
		
		# invoke update
		# updater = hasattr(ind, '__iter__') and self.extend or self.append
		self.extend(ind, new, old, multi=multi, keys=Defs.delta_item_keys)
		
	
	def get_deltas(self, fab=None, cond=None, keys=Defs.delta_item_keys, key_args=True, star=True):
		deltas_args = [repeat(nothing) if arg is nothing else arg for arg in imapattrs(keys, self, cast=list)]
		for delta in zip(*deltas_args):
			if delta.count(nothing) == len(delta):
				break
			delta_ka = dict(zip(keys, delta))
			if not cond or cond(**delta_ka):
				if fab:
					if star:
						delta = fab(**delta_ka) if key_args else fab(*delta)
					else:
						delta = fab(delta_ka) if key_args else fab(delta)
				yield delta
			
	@staticmethod
	def eval_added(new, old, **ka):
		return new is not nothing and old is nothing
	@staticmethod
	def eval_dropped(new, old, **ka):
		return new is nothing and old is not nothing
	@staticmethod
	def eval_changed(new, old, **ka):
		return new is not nothing and old is not nothing
	
	# aliases
	removed = Name.dropped
	__iter__ = get_deltas
	
	# properties
	deltas_args	= property(get_deltas)
	deltas		= property(lambda self: self.get_deltas(fab=self.__class__))
	added		= property(lambda self: self.get_deltas(fab=lambda new, **ka: new, cond=self.eval_added))		# deltas having only new
	dropped		= property(lambda self: self.get_deltas(fab=lambda old, **ka: old, cond=self.eval_dropped))	# deltas having only old
	changed		= property(lambda self: self.get_deltas(cond=self.eval_changed))	# deltas having both new and old
	
	has_ind		= property(lambda self: nothing is not self.ind)
	is_added	= property(lambda self: self.new is not nothing and self.old is nothing)
	is_dropped	= property(lambda self: self.new is nothing and self.old is not nothing)
	is_changed	= property(lambda self: self.new is not nothing and self.old is not nothing)
DeltaItem = DeltaSeq
	
# @autoattrs(cmp=False)
# class DeltaItem(_DeltaItem, Delta): pass


	
# @autoattrs(cmp=False)
# class DeltaIndex(_DeltaItem):
# 	ind: object = nothing
#
# class DeltaSeq(list, Delta):
# 	pass


class EventMap(EventPublisher):
	_capture_state = dict
	
	def __init__(self):
		super().__init__()
		
	def __setitem__(self, *args, **kargs):
		return self.modify('__setitem__', *args, **kargs)
	def update(self, *args, **kargs):
		return self.modify('update', *args, **kargs)
	def clear(self, *args, **kargs):
		return self.modify('clear', *args, **kargs)
	def setdefault(self, *args, **kargs):
		return self.modify('setdefault', *args, **kargs)
	def pop(self, *args, **kargs):
		return self.modify('pop', *args, **kargs)
	
	
	def compute_deltas_general(self, previous, current=None):
		current = self if current is None else current
		previous_sig = set(((key, id(value)) for key, value in previous.items()))
		current_sig = set(((key, id(value)) for key, value in current.items()))
		
		# delta = Filldict(missing=MapDelta)	# previous tested before refactorings
		delta = FillMap(missing=DeltaSeq)		# likely substitute
		for key, sig in previous_sig:
			delta[key].removed = previous[key]
		for key, sig in current_sig:
			delta[key].added = current[key]
		return delta
		
	def modifying(self, method_name, *args, **kargs):
		# self.emit(self, method_name, *args, **kargs)
		prev_state = self._capture_state(self)
		
		func = getattr(super(), method_name)
		result = func(*args, **kargs)
		
		delta = self.compute_deltas_general(prev_state)
		self.emit(delta)
		# self.emit(self, method_name, *args, **kargs)
		return result
	
class EventDict(EventMap, dict): pass




class Binding(Handler):
	def on_event(self, model, **kargs):	raise NotImplemented()


class Model(ModelEvent, Publisher):
	''' A Model refines the event Publisher class by defining events as internal state transitions
		(referred to as a Delta) rather than any arbitrary object.
		Models are a hierarchical patterns for propagating an event to a collection of listeners
		Models can be a composite of other Models corresponding to items within is structure.
		
		Conventions:
		- Model, like Publisher, allows for many event handlers but assumes a single owner as the 1st handler.
		- since model is a ModelEvent it can emit itself as an event once any delegated modification is detected
		- propagates ModelEvents up the chain of ownership after amending event to include self as a source.
		- should implement next to traverse up the chain of ownership (yield self + yield from self.owner)
		- considering pattern of on_event(ModelEvent) -> owner & on_event(Event) -> handlers
	'''
	_call_name = 'on_event'
	
	def __init__(self, handles=None, *pa, evt_pa=None, evt_ka=None, **ka):
		''' Model also maintains a maintains a primary handler and leaves the remaining handers to Publisher '''
		handles = (None if handles is None else tuple(handles)) or (None,)	# coerce to iter; None & [] become [None]
		self._handle, *handles = handles  									# unpack (1st, *rest) into (handler, *handlers)
		
		# init
		# todo: direct pa & ka to which superclass
		Publisher.__init__(self, handles or None, *evt_pa or (), **evt_ka or {})
		ModelEvent.__init__(self, source=self)
	def hook(self, handle, validate=True):
		if validate and not hasattr(handle, self._call_name):
			raise ValueError(f'Given handler {handle} does not have expected interface for {self._call_name!r}.')
		if self._handle is None:
			self._handle = handle
		else:
			super().hook(handle)
	def unhook(self, handle):
		if self._handle is handle:
			self._handle = self._handlers.pop(handle) if self._handlers else None  # remove & replace primary handler
		else:
			super().unhook(handle)
	
	
	def notify(self, *pa, **ka):
		''' signal to handlers with undefined argument pattern '''
		if self._handle is not None:
			super().notify(*pa, **ka)
			
			
	def emit(self, event, *pa, **ka):
		''' Received an update as a handler of some nested internal model. Propagate this delta up the chain. '''
		# emit is equivalent to  notify(event)
		if isinstance(event, Delta):
			event.source = self
		elif isinstance(event, Model):
			event = ModelEvent(source=self, origin=event)
		self.notify(event, *pa, **ka)
		return event
	def updated(self):
		''' Imbue source info from this instance into newly created deltas '''
		if self._handle is not None:
			super().notify(self)
	def set_source(self, source:Model):
		raise ValueError('The source of Models used as events is always self. '
						 'Create new ModelEvent to assign a different source.')
	
	
	def wrap_emit(self, method, *pa, event, **ka):
		''' wrap an action to also trigger a model updated event '''
		result = method(*pa, **ka)
		self.emit(event)
		return result
	def wrap_updated(self, method, *pa, **ka):
		''' wrap an action to also trigger a model updated event '''
		result = method(*pa, **ka)
		self.updated()
		return result
		
	# method aliases
	on_event = emit
	__hash__ = object.__hash__
	
	issue_update = emit	# obsolete
	# updating = wrap_updated	# obsolete
	
	# properties
	@property
	def handles(self):
		if self._handle:
			yield self._handle
			yield from self._handlers and self._handlers.values() or ()
	handle = property(lambda self: self._handle)
	handler = property(lambda self: getattr(self._handle, self._call_name, None))
	handlers = property(lambda self: (getattr(hnd, self._call_name) for hnd in self.handles))


class Models(Model):
	def __init__(self, submodels, *pa, **ka):
		super().__init__(*pa, **ka)
		self.bind_submodels(*submodels)
	def bind_submodels(self, *submodels):
		for submodel in submodels:
			submodel.hook(self)
	def unbind_submodels(self, *submodels):
		for submodel in submodels:
			submodel.unhook(self)
	def replace_submodel(self, old, new):
		old.unbind(self)
		new.bind(self)





class HasModel:
	model_obj = None
	
	
def get_model(obj, default=None):
	if isinstance(obj, Model):
		return obj
	
	result = getattr(obj, 'model_obj', default)
	return result
	
def has_model(obj):
	result = isinstance(obj, Model) or hasattr(obj, 'model_obj')
	return result





class MapModel(Model):
	# obsolete; switch to MapModelCapsule
	_capture_state = dict
	
	# def __init__(self):
	# 	super().__init__()
	def __init__(self, *args, **kargs):
		super().__init__(*args, **kargs)
		
	def __setitem__(self, *args, **kargs):	return self.modifing('__setitem__', *args, **kargs)
	def update(self, *args, **kargs):		return self.modifing('update', *args, **kargs)
	def clear(self, *args, **kargs):		return self.modifing('clear', *args, **kargs)
	def setdefault(self, *args, **kargs):	return self.modifing('setdefault', *args, **kargs)
	def pop(self, *args, **kargs):			return self.modifing('pop', *args, **kargs)
	
	
	def compute_deltas_general(self, previous, current=None):
		current = self if current is None else current
		previous_sig = set(((key, id(value)) for key, value in previous.items()))
		current_sig = set(((key, id(value)) for key, value in current.items()))
		
		delta = Filldict(missing=MapDelta)
		for key, sig in previous_sig:
			delta[key].removed = previous[key]
		for key, sig in current_sig:
			delta[key].added = current[key]
		return delta
		
	def modifing(self, method_name, *args, **kargs):
		# self.emit(self, method_name, *args, **kargs)
		prev_state = self._capture_state(self)
		
		func = getattr(super(), method_name)
		result = func(*args, **kargs)
		
		delta = self.compute_deltas_general(prev_state)
		self.model_delta.modified(delta)
		# self.emit(self, method_name, *args, **kargs)
		return result


# Monitor, Bind, Consumer
class MappingNode(Model):
	''' todo: '''
	def __init__(self, *args, **kargs):
		super().__init__(*args, **kargs)
		self.item_bindings()
	def item_bindings(self):
		for model in self.values():
			if isinstance(model, Model):
				model.bind(self)
				
	# def updated(self, delta):
	# 	if delta.source is None:
	# 		for item_delta in delta.items():
	# 			if item_delta.removed is not None and :
	# 		removed = []
	# 		added = []
	# 		self.make_bindings(removed)
	# 		self.drop_bindings(added)
	#
	# 	super().updated(delta)

# todo: RELOCATE to pyutils.poly
class SuperMethod:
	def __init__(self, action:str):
		self.action = action
	def act(self, obj, *pa, **ka):
		result = super(obj, obj.__class__)(*pa, **ka)
		return result
	
class CapsuleModel(Capsule, Model):
	''' Function as both capsule and model.
		!. Augment access to an encapsulated obj
		2. Propagate events up the tree of models
	'''
	def post_func(self, *pa, **ka):
		print(f'{self.__class__.__name__}.post_func{(pa, ka)}')
		self.updated()

class SeqCapsuleModel(CapsuleModel):
	_calling = 'clear __setitem__ __iadd__ append extend'.split()
class MapCapsuleModel(CapsuleModel):
	_calling = 'clear __setitem__ update setdefault pop'.split()
	

