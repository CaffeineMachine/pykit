import re
from copy import copy



class Strings(list):
	''' Class for operating on a string as a list of strings split with the given delimiter. '''
	fmtstr = 'l\'{}\''

	def __init__(self, strings:str='', delim:str='[, ]+', fmtstr=None, to_str=str):
		self.delim = self.repr_delim = delim
		self.to_str = to_str
		if isinstance(strings, str):
			self.fmtstr = fmtstr or Strs.fmtstr
			found = re.search(r''+delim, strings)
			self.repr_delim = found.group() if found else ','
			items = strings and re.split(r''+delim, strings) or []
		else:
			items = [to_str(item) for item in strings]
			self.repr_delim = ','
		list.__init__(self, items)
	def format(self, *spec):
		return self.repr_delim.join(self)
	def __repr__(self):
		result = self.fmtstr.format(str(self), cls=self.__class__.__name__)
		return result

	def __add__(self, tokens, delim=None):
		result = copy(self).extend(tokens, delim=delim)
		return result

	def extend(self, tokens, delim=None):
		if not isinstance(tokens, Strings):
			tokens = Strings(tokens, delim=delim or self.delim)
		list.extend(self, tokens)
		return self

	# method aliases
	__str__ = __format__ = format
	__iadd__ = extend


class StrsBase:
	''' Class for operating on a string as a list of strings split with the given delimiter. '''
	fmtstr = 'l\'{}\''
	
	@classmethod
	def init(cls, attrs, strings:str='', delim:str='[, ]+', fmtstr=None, to_str=str):
		attrs.delim = attrs.repr_delim = delim
		attrs.to_str = to_str
		if isinstance(strings, str):
			attrs.fmtstr = fmtstr or cls.fmtstr
			found = re.search(r''+delim, strings)
			attrs.repr_delim = found.group() if found else ','
			items = strings and re.split(r''+delim, strings) or []
		else:
			items = [to_str(item) for item in strings]
			attrs.repr_delim = ','
		return items
	def format(self, *spec):
		return self.repr_delim.join(self)
	def __repr__(self):
		result = self.fmtstr.format(str(self), cls=self.__class__.__name__)
		return result

	def __add__(self, tokens, delim=None):	raise NotImplemented()
	def extend(self, tokens, delim=None):	raise NotImplemented()

	# method aliases
	__str__ = __format__ = format
	__iadd__ = extend

class LStrs(StrsBase, list):
	def __init__(self, *args, **kws):
		items = self.init(self, *args, **kws)
		list.__init__(self, items)
	def __add__(self, tokens, delim=None):
		result = copy(self).extend(tokens, delim=delim)
		return result
	def extend(self, tokens, delim=None):
		if not isinstance(tokens, Strings):
			tokens = Strings(tokens, delim=delim or self.delim)
		list.extend(self, tokens)
		return self

	# method aliases
	__iadd__ = extend
Strs = LStrs


class KStrs(StrsBase, set):
	fmtstr = 's\'{}\''

	def __init__(self, *args, **kws):
		items = self.init(self, *args, **kws)
		set.__init__(self, items)
SStrs = KStrs


class _Attrs: pass

class TStrs(StrsBase, tuple):
	''' Class for operating on a string as a list of strings split with the given delimiter. '''
	fmtstr = 't\'{}\''

	def __new__(cls, *args, **kws):
		attrs = _Attrs()
		items = cls.init(attrs, *args, **kws)
		self = tuple.__new__(cls, items)
		[setattr(self, k, v) for k, v in attrs.__dict__.items()]
		return self
	def __add__(self, tokens, delim=None):
		return self.__class__(list(self)+list(tokens))
	def extend(self, tokens, delim=None):
		if not isinstance(tokens, Strings):
			tokens = Strings(tokens, delim=delim or self.delim)
		list.extend(self, tokens)
		return self

	# method aliases
	__iadd__ = extend



delim_priority = ' ,_-;:/=&%#@`~'  # todo: ensure allowed chars are re compatible
def StrsFrom(delim_strings:str) -> Strs:
	''' Given *delim_strings* detect the custom delimiter by which to split it into Strs.
		The delim will be delim_strings[0] but only if it is found 2 or more times in *delim_string*
		When there is no qualifying delim (delim_strings[0] occurs only once) return Strs containing
		only 1 *delim_strings*. Emulates the unix sed application pattern replace syntax/behavior.
	
		Usage:
		StrsFrom(' abc def ')	# => Strs(['abc', 'def'])
		StrsFrom(':abc:def:')	# => Strs(['abc', 'def'])
		StrsFrom('::a:bc:def')	# => Strs(['', 'a', 'bc', 'def'])
	'''
	if not len(delim_strings):
		return Strs()

	delim, strings = delim_strings[0], delim_strings[1:]
	if not delim in strings:
		strings = delim_strings
		delim = list(set(delim_priority) - set(delim_strings))[0]
	else:
		strings = strings[:-1] if strings[-1] == delim else strings
	result = Strs(strings, delim)
	return result

def strs_from(delim_strings:str) -> list:
	result = list(StrsFrom(delim_strings))
	return result
strs_from.__doc__ = StrsFrom.__doc__.replace(' Strs', ' list of strs')

class MatchReplacer:
	def __init__(self, func):
		self.func = func
	def __call__(self, match):
		if match is None: return None
		found = match[bool(match.lastindex)]
		print(f'XStr.replace({match.groups()}, length={match.lastindex}) => {found}')
		result = self.func(found)
		return result
	
def reformat(self, reformatter):
	''' produce args and kws from a string '''
	replacer = reformatter
	if not isinstance(reformatter, str):
		replacer = MatchReplacer(reformatter)
	result = re.sub(XStr.fmt_py_group_named, replacer, self)
	return result

def unformat(self, formatted):
	''' produce args and kws from a string '''
	fmt = XStr.fmt_re_group_named.format(name=r'\1', delim='-')
	result = re.search(r''+self.reformat(fmt), formatted)
	if result:
		result = result.groupdict()
	return result

class XStr(str):
	''''''
	fmt_py_group_named = r'\{(\w+)(:[^\}]+)?\}'
	fmt_re_group_named = '(?P<{name}>[^{delim}]*)'
	fmt_re_group = '([^{delim}]*)'
	
	# todo: determine best way to subclass str
	reformat = reformat
	unformat = unformat
	
	def left(self, sep, regex=False):
		if regex:
			result = re.split(self, sep, 1)[0]
		else:
			result = self.split(sep, 1)[0]
		return result

	def right(self, sep, regex=False):
		if regex:
			result = re.split(sep, self)[-1]
		else:
			result = self.rsplit(sep, 1)[-1]
		return result
		

if __name__=='__main__':
	from pprint import pprint as pp
	
	xstr = XStr('{dir}/{date:%Y%D}-mktsum-{market}-{exch}-{interval}-{source}.csv')
	query = '/path/to/181018-mktsum-btcusd-bitstamp-15m-bitcoincharts.csv'
	delim = '-'
	fmt = XStr.fmt_re_group_named.format(name=r'\1', delim='-')
	refmt = xstr.reformat(fmt)
	print(refmt)
	
	refmt = xstr.unformat(query)
	pp(refmt)
	result = refmt == dict(
		dir='/path/to',
		date='181018',
		market='btcusd',
		exch='bitstamp',
		interval='15m',
		source='bitcoincharts',
	)
	pp(result)
	
	
	
	refmt = xstr.reformat(str.upper)
	pp(refmt)
