from pyutils.utilities.srcinfoutils import *

### properties serving as routes to nested attributes


class Fabricator: pass

def auto_kargs(attrfab, locals, prefer_self=True):
	self_attrs = {} if prefer_self is None else getattr(locals.get('self'), '__dict__', {})
	arg_keys = func_arg_keys(attrfab)
	avail_keys = tuple(locals.keys()) + tuple(self_attrs.keys())
	# miss_keys = set(avail_keys).difference(arg_keys)
	#
	# # generate KeyError since KeyErrors are not raised below
	# if miss_keys:
	# 	raise KeyError(f'Args {miss_keys} not found in locals or locals["self"] keys: {avail_keys}')
	
	# pull args from locals or locals['self']
	prime, aux = (self_attrs, locals) if prefer_self else (locals, self_attrs)
	kargs = {arg_key: prime.get(arg_key, aux.get(arg_key)) for arg_key in arg_keys}
	return kargs
def auto_apply(attrfab, locals):
	Kargs = auto_kargs(attrfab, locals)
	obj = attrfab(**Kargs)
	return obj

def _coerce_to_from_keys(to_from_keys):
	''' coerce to_from_keys as delimited str to str list then
		coerce each str in to_from_keys to (from_key, to_key) duplicate str pair
	'''
	to_from_keys = to_from_keys.split() if isinstance(to_from_keys, str) else list(to_from_keys)
	for ind, to_from_key in enumerate(to_from_keys):
		if isinstance(to_from_key, str):
			to_from_keys[ind] = (to_from_key, to_from_key)
		elif len(to_from_key) != 2:
			raise ValueError()
	return to_from_keys

class Renamer:
	''' Functions as a configurable readonly property '''
	__slots__ = ['getter', 'attr_key']
	def __init__(self, getter, attr_key):
		self.getter = getter  # todo: remove this
		self.attr_key = attr_key
	def __get__(self, obj, cls):
		if obj is None: return self
		
		got = getattr(obj, self.getter) if isinstance(self.getter, str) else self.getter(obj)
		result = getattr(got, self.attr_key)
		return result
	
class Rerouter(Renamer):
	''' Functions as a configurable read/write property '''
	def __set__(self, obj, value):
		raise NotImplementedError()

def route(cls, attrgetter, to_from_keys):
	''' add router as a property *to_obj*.*to_key* <- *from_obj*.*from_key* '''
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		prop = Rerouter(attrgetter, from_key)
		setattr(cls, to_key, prop)
	return cls
_AttrProp = Rerouter
# route = add_rerouters

def add_rerouters(cls, to_from_keys, overwrite=False):
	''' add router as a property *cls*.*to_key* = property(*from_key*) '''
	# todo: make to_key distinct from from_keys
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		if not overwrite and hasattr(cls, to_key): continue
		
		prop = Rerouter('__getattr__', from_key)
		setattr(cls, to_key, prop)
	return cls

def add_reroutes(to_obj, from_obj, to_from_keys, overwrite=False):
	''' add route as a static attribute *to_obj*.*to_key* <- *from_obj*.*from_key* '''
	# todo: make to_key distinct from from_keys
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		if not overwrite and hasattr(to_obj, to_key): continue
		
		value = getattr(from_obj, from_key)
		setattr(to_obj, to_key, value)
	return to_obj


class routes:
	''' apply properties to *cls* for each *to_from_key* route on the getter(inst) result.
		__init__ -> __orig_init__
		attrroutes_init -> __init__
		:param cls: class to augment with properties
		:param attrgetter:
		:param to_from_keys:
		:param attrfab:
		:param autoargs: when true auto init *attrfab* with locals()
	'''

	def __init__(self, attrgetter, to_from_keys):
		self.attrgetter = attrgetter
		self.to_from_keys = to_from_keys
	def __call__(self, cls):
		self.add(cls, **self.__dict__)
		return cls
	@classmethod
	def add(decorator, cls, attrgetter, to_from_keys, **_):
		to_from_keys = _coerce_to_from_keys(to_from_keys)
		for from_key, to_key in to_from_keys:
			setattr(cls, to_key, _AttrProp(attrgetter, from_key))
		return cls


class routesattr(routes):
	'''	Modify a class with a new member attribute property accessors to select sub-attributes.
		apply properties to *cls* for each *to_from_key* route on the getter(inst) result.
		__init__ -> __orig_init__
		attrroutes_init -> __init__
		:param attrgetter:
		:param to_from_keys:
		:param attrfab:
		:param autoargs: when true auto init *attrfab* with locals()
		
		:param cls: class to augment with properties
	'''

	def __init__(self, attr, to_from_keys, attrfab, autoargs=True):
		super().__init__(attr, to_from_keys)
		self.attrfab = attrfab
		self.autoargs = autoargs
	@classmethod
	def add(decorator, cls, attrgetter, to_from_keys, attrfab, autoargs=True, **_):
		attr = attrgetter
		cls.__post_init_attrs__ = decorator.post_init_attrs
		cls.__orig_init__ = cls.__init__
		cls.__init__ = decorator.attr_routes_init
		
		# store in *cls*._post_init_attrs data needed to create and apply *attr* during instantiation
		if not hasattr(cls, '_post_init_attrs'):
			cls.__post_init_attrs = {}
		cls.__post_init_attrs.setdefault(cls, ())
		cls.__post_init_attrs[cls] += ((attr, attrfab, autoargs),)
		
		# apply routes to subattrs as properties
		cls = routes.add(cls, attr, to_from_keys)
		return cls
	
	@staticmethod
	def attr_routes_init(inst, *a, **k):
		inst.__orig_init__(*a, **k)
		inst.__post_init_attrs__(*a, **k)
	@staticmethod
	def post_init_attrs(inst, *a, locals=None, **k):
		_post_init_attrs = inst.__post_init_attrs[inst.__class__]
		# construct attr given each attrfab and apply to self instance
		for attr, attrfab, autoargs in _post_init_attrs:
			if hasattr(inst, attr): continue
			
			if not autoargs:
				attr_obj = attrfab()
			elif isinstance(attrfab, Fabricator):
				attr_obj = attrfab.auto_construct(dict(self=inst, **k))
			else:
				attr_obj = auto_apply(attrfab, dict(self=inst, **k))
			setattr(inst, attr, attr_obj)


def routesubattr(cls):
	''' use subattr as fall back for missing attr '''
	return cls




class MemberAccessor:
	def __init__(self, owner_capsule, attr):
		self.owner_capsule = owner_capsule
		self.attr = attr
	# def __get__(self, instance, owner): pass
	# def __set__(self, instance, owner): pass

class MethodAccessor(MemberAccessor):
	def __init__(self, owner_capsule, method):
		if isinstance(method, str):
			attr = method
			method = None
		else:
			attr = method.__name__
		self.member = method
		super().__init__(owner_capsule, attr)
	def __call__(self, *pa, **ka):
		self.member = self.member or getattr(self.owner_capsule.obj, self.attr)
		return self.owner_capsule.func_wrapper(self.member, *pa, **ka)

class Capsule:
	''' A Capsule encapsulates an *obj* instance (usually taking its place) and preempts calling or accessing
	 	the specified internal members of *obj*. Expands on the OOP concept of encapsulation. Useful for augmenting
	 	or observing an *obj*.
	'''
	_getting, _setting, _calling, _reroutes = (None,)*4
	pre_get, post_get, pre_set, post_set, pre_func, post_func = (None,)*6
	
	def __init__(self, obj=None, *pa, getting=None, setting=None, calling=None, reroutes=None, **ka):
		self.obj = obj
		self.wrap(*(getting or self._getting or ()) + (setting or self._setting or ()))
		self.wrap(*(calling or self._calling or ()), as_method=True)
		add_reroutes(self, self.obj, reroutes or self._reroutes or ())
		super().__init__(*pa, **ka)
	def __repr__(self): return f'Capsule({self.obj!r})'
	def __str__(self): return f'<<{self.obj!r}>>'
	def __call__(self, target):
		''' Decorator assignment of an obj or class to this capsule '''
	def wrap(self, *attrs, as_method=False):
		''' Assign an obj or class to this capsule '''
		for attr in attrs:
			accessor_fab = (not as_method and isinstance(attr, str)) and MemberAccessor or MethodAccessor
			accessor = accessor_fab(self, attr)
			self.__dict__[accessor.attr] = accessor
	def get_wrapper(self, name):
		self.pre_get and self.pre_get(name)
		attr = getattr(self.obj, name)
		self.post_get and self.post_get(name)
		return attr
	def set_wrapper(self, name, value):
		self.pre_set and self.pre_set(name, value)
		result = setattr(self.obj, name, value)
		self.post_set and self.post_set(name, value)
		return result
	def func_wrapper(self, func, *pa, **ka):
		self.pre_func and self.pre_func(func, *pa, **ka)
		result = func(*pa, **ka)
		self.post_func and self.post_func(func, result, *pa, **ka)
		return result
	
	
class FullCapsule:
	''' A Capsule encapsulates an *obj* instance (usually taking its place) and preempts all calls and access
	 	to the *obj*s internal members. Expands on the OOP concept of encapsulation. Useful for augmenting
	 	or observing an *obj*.
	'''
	# todo: move to pyutils
	def __init__(self, obj=None, **kargs):
		self.__dict__.update(dict(_obj=obj, **kargs))
	def __getattr__(self, attr):
		''' redirect get access to encapsulated obj'''
		result = getattr(self._obj, attr)
		return result
	def __setattr__(self, attr, value):
		''' redirect set access to encapsulated obj '''
		setter = super().__setattr__ if attr in self.__dict__ else self._obj.__setattr__
		setter(attr, value)