''' obsolete: See
- pyutils.patterns.accessor
- pyutils.structures.maps
'''

from collections import Iterable, defaultdict as ddict
import itertools as it

import types



class Gen:
	def __init__(self, factory):
		self.gen_factory = factory
	def __getitem__(self, inds):
		if isinstance(inds, int):
			result = next(it.islice(self.gen_factory(), inds, None))
		elif isinstance(inds, slice):
			result = list(it.islice(self.gen_factory(), inds.start, inds.stop, inds.step))
		else:
			raise TypeError()
		return result
	
	

class Keys: pass

class Iters(object):
	# obsolete: See pyutils.patterns.accessor.Iters
	''' Class takes some iterable content and returns iterators of subcomponents when the iter is
		called with a subcoponent accessor. Use list constructor list(Iters()) or iter() function
		to resolve Iters() so that it can be indexed or traversed.
		Syntatic sugar equivalent of:
		map(lambda obj: obj.subitem, objs)  # or
		map(lambda obj: obj[0], objs)

		Usage:
		iobjs = Iters(objs)
		iobjs.subitem		# returns an iterator of subitem members for each obj
		iobjs[-1] 			# returns an iterator of the last item for each obj
		iobjs.subitem[-1]	# returns an iterator of the last item of the subitem member for each obj
	'''
	accessor_constraints = (getitem_only, getattr_only, other) = range(3)
	def __init__(self, content):
		self.content = content
		self._iter = iter(self.content)
	def __iter__(self):
		return self._iter
	def __next__(self):
		return next(self._iter)
	next = __next__
	def call(self, accessor):
		# todo
		if '':
			result = itertools.imap(accessor, self.content)
		if '':
			result = map(accessor, self.content)
		return result
	def __getitem__(self, index):
		return self._get_each(index, accessor_constraint=Iters.getitem_only)
	def __getattr__(self, key):
		return self._get_each(key, accessor_constraint=Iters.getattr_only)
	def _get_each(self, key, accessor_constraint=getitem_only):
		if isinstance(self.content, types.GeneratorType):
			result = Iters((item for item in self._gen_each(key, accessor_constraint)))
			return result

		if isinstance(key, Keys):
			accessor_constraint = Iters.other

		result = []
		for item in self.content:
			item_element = item
			if accessor_constraint == Iters.other:
				item = key.get(item)
			elif accessor_constraint == Iters.getitem_only:
				item = item[key]
			elif accessor_constraint == Iters.getattr_only:
				item = getattr(item, key)
			result.append(item)

		result = Iters(result)
		return result

	def _gen_each(self, key, accessor_constraint, iterator_result=False):
		if isinstance(key, Keys):
			accessor_constraint = Iters.other

		for item in self.content:
			item_element = item
			if accessor_constraint == Iters.other:
				item = key.get(item)
			elif accessor_constraint == Iters.getitem_only:
				item = item[key]
			elif accessor_constraint == Iters.getattr_only:
				item = getattr(item, key)
			yield item

class Accessor(object): pass

class FieldList(object):
	def __init__(self, accessor):
		self.accessor = accessor

	def __call__(self, *args, **kwargs):
		result = list(it.imap(self.accessor, *args, **kwargs))
		return result


class Keys(object):
	''' Class explicitly representing a list of keys for deferred access to multi-dimensional structures.
		This is used to disambiguate Keys() access definition from literal data structures.
		Usage:
		data=[ [ [6] ] ]
		data[0][0][0]						# => 6
		k = K[0][0][0]						# => Keys([0,0,0]) # shorthand constructor of Keys() for deferred data access
		k.get(struct=data)  				# => 6
		k.get(struct=( ( ('7',) ,) ,))  	# => '7'
		k.get(struct={0: {0: {0: [8] } } })	# => [8]
		k.get(struct=[ [ {0: (9,)} ] ])		# => (9,)
	'''
	members = ('_keys', '_access_type')
	access_types = (getitem, getattr) = range(2)
	_default = None
	__bool__ = __nonzero__ = lambda obj: True
	i = ddict(lambda: IndexKeys())
	@staticmethod
	def set_default():
		result = Keys._default = Keys()
		return result
	def __init__(self, *keys):
		self._keys = list(keys)
		self._access_type = [Keys.getitem]*len(self._keys)
	def __str__(self):
		return '%s(%s)' % (self.__class__.__name__ , str(self._keys))
	__repr__ = __str__
	def __getitem__(self, key):
		obj = self is not Keys._default and self or Keys()
		obj._keys.append(key)
		obj._access_type.append(Keys.getitem)
		return obj
	def __getattr__(self, key):
		if not isinstance(key, str): raise TypeError('Attr getattr key must be a string. Got %s' % key)
		if hasattr(super(Keys, self), key):
			result = getattr(self, key)
			return result

		obj = self is not Keys._default and self or Keys()
		obj._keys.append(key)
		obj._access_type.append(Keys.getattr)
		return obj

	def get(self, struct):
		if not isinstance(self._keys, Iterable):
			print('######## Keys.get(%s)#########' % str(struct))
			result = struct.value[self._keys]  # todo: verify validity of this case
		else:
			result = struct
			if isinstance(struct, Data):
				result = struct.value
			depth = 0
			for key, accessor in zip(self._keys, self._access_type):
				if accessor is Keys.getitem:
					result = result[key]
				else:
					result = getattr(result, key)
				depth += 1
		return result
K = Keys.set_default()

class IndexKeys(Keys):
	def get(self, data):
		value_index = MakeKeys.i.values().index(self)
		key_index = MakeKeys.i.keys()[value_index]
		result = data.iteration_indices[key_index]
		return result




class MakeKeys(Keys):
	# obsolete: see pyutils.structures.maps
	''' Provides the ability to express list of keys __getitem__ rather than a literal list.
		Chaining adds flexibility to construct Keys object syntactically identical to data access.
		Usage:
		print(MakeKeys[1][2][3])	# result: Keys([1,2,3])  # Create list for nested access unambigously
		K = MakeKeys()				# an alias
		print(K[1][2][3])			# result: Keys([1,2,3])
		print(K(1,2,3))				# result: Keys([1,2,3])
	'''

	@staticmethod
	def __getitem__(key):
		result = Keys()[key]
		return result
	@staticmethod
	def __call__(*keys):
		''' call'''
		result = Keys(*keys)
		return result


