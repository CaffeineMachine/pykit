from itertools import *

from pyutils.defs.colldefs import coll, gen_coll_wraps, dklt_colls
from pyutils.structures.names import *
from pyutils.utilities.sequtils import *



class PolyObj:
	''' Store an operation by name without its instance or class definition to later
		be invoked polymorphically on any instance.  Hides getattr(obj, 'operation') 
		with syntactic sugar Poly.operation
		
		Usage:
		const_op_run = Poly.run         # define operation as a constant; 'run' can be any action; no need for class or instance
		const_op_run(SomeRunnable())    # invoke const 'run' operation on arbitrary runnable instance
		const_op_run(SomeRunnable(), *[], **{})    # any other args accepted as usual
		
		Notes:
		- Normally calling a base class method on a child instance does not invoke
		any overriden methods because python does not employ polymorphism. (For example
		in static invocation with subclass type 'A.run(ab) # => A running: ...';
		see below in __main__ tests)
		- functools.partial/partialmethod also does not behave polymorphically.
		- python.ABC only enforces that subclasses implement all abstract methods.
	'''
	def __init__(self, attr=None, func=None, name=None):
		self.attr = attr
		
		# customarily defined via Polynom(a=0, ...)
		self.name = name #or f'Poly.{self.attr}(obj, ...)'
		self.func = func
		if self.func:
			self.__call__ = self.func
		
	def __repr__(self):
		result = f'Poly.{self.attr}'
		return result
	def __str__(self):
		result = self.name or object.__str__(self)
		return result
	def __getitem__(self, index):
		return self.__call__(index)
	def __call__(self, obj, *args, **kws):
		if self.func:
			result = self.func(obj, *args, **kws)
		else:
			result = getattr(obj, self.attr)(*args, **kws)
		return result
	def __get__(self, instance, owner):
		method = getattr(instance, self.attr)
		return method
	
	def resolve(self, obj):
		func = getattr(obj, self.attr)
		return func
	
	# method aliases
	# __repr__ = __str__


class PolynomObj(PolyObj):
	''' Store an operation by name with some preset arguments but without its instance
		or class definition to later be invoked polymorphically on any instance.
		Similar in concept to a polynomial as it is an operation/expression in which
		some variables are known and others are not. Hides getattr(obj, 'operation')
		with syntactic sugar Polynom.operation
		Calling with preset parameters returns an function lookup lambda that takes roughly
		(obj, ..., **presets)
		
		Usage:
		const_op_pop = Polynom.pop                      # next __call__ takes preset args and returns a lambda
		op_pop_2nd = Polynom.pop(1)                     # define operation with presets as a constant; next __call__ takes instance and invokes operation
		op_pop_2nd(['keep', 'drop', 'keep', 'keep'])    # invoke const operation on arbitrary poppable instance
	'''
	def __getitem__(self, index):
		return self.__call__(index)
	def __call__(self, *pre_args, **pre_kargs):
		name = 'Polynom.%s(%s%s)' % (
			self.attr,
			(pre_args and f'{"".join(map(str, pre_args))}') or '',
			(pre_kargs and f'{",".join([str(k)+"="+str(w) for k, w in pre_kargs.items()])}') or '')
		func = lambda obj, *args, **kws: getattr(obj, self.attr)(*pre_args, *args, **pre_kargs, **kws)
		result = PolyObj(self.attr, func, name)
		return result

class PolySeq:
	def __init__(self, func_arg, get_iter=None, coll=None, frepr=None):
		self.func_arg = func_arg
		self.frepr = frepr
		self.coll = coll
		self.get_iter = get_iter
	def apply(self, seq, *pa, **ka):
		items = seq if self.get_iter is None else self.get_iter(seq)
		func = self.func_arg
		for item in items:
			if isinstance(self.func_arg, str):
				func = getattr(item.__class__, self.func_arg)
			value = func(item, *pa, **ka)
			yield value
	def apply_coll(self, seq, *pa, **ka):
		items = self.apply(seq, *pa, **ka)
		if self.coll:
			items = self.coll(items)
		return items

	# method aliases
	__call__ = apply_coll


class Fab: pass

class AttrPreFab:
	''' Takes a *prefab*ricator function then generates fabricator for each attribute name access.
		Note: the resulting fab is expected to be callable
	'''
	
	def __init__(self, prefab, *pa_def, **ka_def):
		self.__dict__.update(dict(prefab=prefab, pa_def=pa_def, ka_def=ka_def))
	
	def prefabricate(self, attr:str) -> Fab:
		fab = self.prefab(attr, *self.pa_def, **self.ka_def)
		return fab
	
	# method aliases
	__getattr__ = prefabricate

class AttrFab:
	''' Takes a *fab*ricator function then generates objects for each attribute name access. '''
	
	def __init__(self, fab, *pa_def, **ka_def):
		self.__dict__.update(dict(fab=fab, pa_def=pa_def, ka_def=ka_def))
		
	def fabricate(self, attr:str):
		# todo: add caching by default
		# result = self.fab(attr, *pa, *self.pa_def, **ka, **self.ka_def)
		result = self.fab(attr, *self.pa_def, **self.ka_def)
		return result
	
	# method aliases
	__getattr__ = fabricate

Poly = PolyFab = AttrFab(PolyObj)			# Poly is a Fab which creates a PolyObj for the accessed attr name
Polysd, Polysk, Polysl, Polyst = [AttrFab(PolySeq, coll=coll) for coll in dklt_colls]
Polynom = PolynomFab = AttrFab(PolynomObj)	# Polynom is a Fab which creates a PolynomObj for the accessed attr name

# todo: replace references to obsolete class aliases
_Poly = PolyObj
_Polynom = PolynomObj
PolyFactory = AttrFab




def get_polynoms(keys, polynom, *args, **kws):
	if not isinstance(keys, (list, tuple)):
		if (isinstance(keys, str) and len(keys) >= 1) or isinstance(keys, list):
			pass
		else:
			keys = [keys]
	args_list = zip(args)
	kws_list = list(kws.items())
	key, value_list = range(2)
	key_values_product = list(map(lambda k: list(product([k[key]], k[value_list])), kws.items()))
	keys_args_kws = zip_longest(keys, zip(*args), zip(*key_values_product), fillvalue=[])
	result = {key:polynom(*key_args, **dict(key_kws)) for key, key_args, key_kws in keys_args_kws}
	return result



class ArgKey:
	def __init__(self, key, default=arg_missing):
		self.key = key
		self.default = default

class ArgKeys(list):
	''' simple list of ArgKey structs '''
	# properties
	keys		= property(lambda self: tuple(map(Name.key, self)))
	defaults	= property(lambda self: tuple(map(Name.default, self)))
	
	def __init__(self, args):
		args = list(map(lambda arg: ArgKey(*coll(arg)), args))
		super().__init__(args)
	

class Args:
	def __init__(self, *args, **kargs):
		if not ... in args:
			args = (...,) + args
		self.pre_largs, self.pre_rargs = splitargs(args)
		self.pre_kargs = kargs
	def __call__(self, func, *args, **kargs):
		result = func(*self.pre_largs, *args, *self.pre_rargs, **self.pre_kargs, **kargs)
		return result
	def __str__(self):
		result = f'{self.__class__.__name__}({vars(self)})'
		return result
	__repr__ = __str__


class Call(Args):
	def __init__(self, func, *args, **kargs):
		self.func = func
		super().__init__(*args, **kargs)
	def __call__(self, *args, **kargs):
		self.debug(*self.pre_largs, *args, *self.pre_rargs, **self.pre_kargs, **kargs)
		result = self.func(*self.pre_largs, *args, *self.pre_rargs, **self.pre_kargs, **kargs)
		return result
	def debug(self, *args, **kargs):
		print([args, kargs])
	
	
class Fabricator:
	''' Defines and facilitates a custom process of assignment during and after instantiation '''
	# todo: change rdefaults to defaults
	# todo: split defaults into largs, rargs
	# todo: support fields with leading pargs defines like: [None, None, 'third', 'fourth']
	# todo: support fields with leading pargs defines like: [2, 'third', 'fourth']
	# todo: consolidate overlapping functionality with Args or ArgKeys
	
	def __init__(self, ctor, fields, rdefaults=()):
		''' :param ctor: constructor to be called
			:param fields: sequence of arg names; also accepts for a space delimited str
			:param rdefaults: defaults for the right-most args
		'''
		if isinstance(fields, str):
			fields = fields.split()
		self.ctor = ctor
		self.fields = list(fields)
		self.rdefaults = rdefaults
	def __len__(self): return len(self.fields)
	def to_kargs(self, *args, **kargs):
		if len(args) > len(self):
			raise ValueError()
		
		# define arg values
		values = [None] * len(self.fields)				# init values as list of Nones of expected length
		values[-len(self.rdefaults):] = self.rdefaults	# overwrite right side with rdefaults
		values[:len(args)] = args						# overwrite left side with args
		kargs.update(zip(self.fields, values))			# define kargs as map of field keys to values
		return kargs
	def auto_kargs(self, locals, *args, **kargs):
		''''''
	def construct(self, *args, **kargs):
		if 'True; todo':
			args, kargs = (), self.to_kargs(*args, **kargs)
		obj = self.ctor(*args, **kargs)
		return obj
	def auto_construct(self, locals, *args, **kargs):
		''' construct with args extracted from locals'''
		args, kargs = (), self.auto_kargs(*args, **kargs)
		obj = self.ctor(*args, **kargs)
		return obj
	def set(self, obj, *args, **kargs):
		if 'True; todo':
			args, kargs = (), self.to_kargs(*args, **kargs)
		for field, value in kargs.items():
			setattr(obj, field, value)
	# method aliases
	__call__ = construct
Fab = Fabricator

class _WIP_Fabricator:
	'''
		Usage:
		Fab(Ctor, '-2 -1* ** * _')
		Fab(Ctor, '-2 -1* ** 0* 1', '* **')
		Fab(Ctor, '_ a _ b', )
	'''
	def __init__(self, ctor, pa_ctor, pa_set=None, setter=None, pa_ldef=None, pa_rdef=None, ka_def=None, **_ka_def):
		self.ctor = ctor
		self.setter = setter
		self.pa_ctor = pa_ctor
		self.pa_set = pa_set or pa_ctor
		self.pa_ldef = pa_ldef
		self.pa_rdef = pa_rdef
		self.ka_def = (ka_def or {}).update(_ka_def)
	def get_args(self, *pa, args_fmt='ctor', **ka):
		pa_fmt = args_fmt == 'ctor' and self.pa_ctor or self.pa_set
		return None, None  # todo
	def construct(self, *pa, **ka):
		pa, ka = self.get_args(*pa, args_fmt='ctor', **ka)
		obj = self.ctor(*pa, **ka)
		return obj
	def set(self, obj, *pa, **ka):
		pa, ka = self.get_args(*pa, args_fmt='setter', **ka)

		if self.setter:
			result = self.setter(obj, *pa, **ka)
		else:
			assert not pa, 'Cannot apply positional args without a Fabricator.setter function.'
			result = None
			for attr, value in ka.items():
				setattr(obj, attr, value)
		return result
	
	# method aliases
	__call__ = construct
	
class FabPK(_WIP_Fabricator):
	''' Same as Fab(ctor, pa_ctor='* **').	Will be removed when Fabricator can support this notation. '''
	def __init__(self, ctor, pa_ctor='* **', **_ka_def):
		super().__init__(ctor, '* **', **_ka_def)
	def get_args(self, pa, ka=None, **_):
		return pa, (ka or {})
	
class FabK(_WIP_Fabricator):
	''' Same as Fab(ctor, pa_ctor='**'). Will be removed when Fabricator can support this notation.	'''
	def __init__(self, ctor, pa_ctor='**', **_ka_def):
		super().__init__(ctor, '**', **_ka_def)
	def get_args(self, ka=None, fmt_args='ctor', **_):
		return (), (ka or {})
	

def coerce_method(instance, func):
	''' Bind the function *func* to *instance*, with either provided name *as_name*
		or the existing name of *func*. The provided *func* should accept the
		instance as the first argument, i.e. "self".
	'''
	bound_method = func.__get__(instance, instance.__class__)
	return bound_method

def bind_method(instance, func, as_name=None):
	''' Bind the function *func* to *instance*, with either provided name *as_name*
		or the existing name of *func*. The provided *func* should accept the
		instance as the first argument, i.e. "self".
	'''
	if as_name is None:
		as_name = func.__name__
	bound_method = func.__get__(instance, instance.__class__)
	setattr(instance, as_name, bound_method)
	return bound_method

if __name__=='__main__':
	''' A simple demonstration '''
	### Configuration #############################
	op01_len = Poly.__len__
	op02_pop_first = Polynom.pop(0)
	op03_pop_last = Polynom.pop(-1)
	op04_get_item = Polynom.__call__(start=0, stop=None, step=1)
	
	### Runtime ###################################
	data = [1,2,3,4,5,6,7]
	print(f'{op01_len}')
	print(f'{op02_pop_first}')
	print(f'{op03_pop_last}')
	print(f'{op04_get_item}')
	print(op01_len(data))			# => 7
	print(op02_pop_first(data))	# => 1
	print(op03_pop_last(data))	# => 7
	# print(data[op04_get_item(slice)])					# => [2, 3, 4, 5, 6]


	''' A more real-world target scenario; invoking polynoms (canned operations+parmaters) from a dict '''
	# A prototypical inheritance scheme
	class A:
		def do(self, input, other=None):
			print('A doing: %s, %s' % (input, other))
		def run(self, input, other=None):
			print('A running: %s, %s' % (input, other))

	class AA(A):
		def do(self, input, other=None):
			print('AA doing: %s, %s' % (input, other))
		def run(self, input, other=None):
			print('AA running: %s, %s' % (input, other))

	class AB(A):
		def do(self, input, other=None):
			print('AB doing: %s, %s' % (input, other))
		def run(self, input, other=None):
			print('AB running: %s, %s' % (input, other))

	class Keys:
		lrup = (Left, Right, Up, Down) = 'LRUD'
		wens = (West, East, North, South) = 'WENS'
		adws = (A, D, W, S) = 'ADWS'
		cell_nav = []

	class Scalar:
		directions = X_pos, Y_pos, X_neg, Y_neg,  = [1,0], [0,1], [-1,0], [0,-1]
		lrup = (L, R, U, D) = (Y_neg, Y_pos, X_neg, X_pos)


	### Configuration #############################
	routines_do = get_polynoms(Keys.lrup, Polynom.do, Scalar.lrup, Scalar.lrup)                # get dict of 'obj.do' polynoms given args
	routines_run = get_polynoms(Keys.lrup, Polynom.run, input=Scalar.lrup, other=Scalar.lrup)  # get dict of 'obj.run' polynoms given kws

	### Runtime ###################################
	a = A()
	aa = AA()
	ab = AB()

	routines_do['L'](a)		# => A doing: [0, -1], [0, -1]
	routines_do['L'](aa)	# => AA doing: [0, -1], [0, -1]
	routines_do['L'](ab)	# => AB doing: [0, -1], [0, -1]
	routines_run['R'](a)	# => A running: [0, 1], [0, 1]
	routines_run['U'](aa)	# => AA running: [-1, 0], [-1, 0]
	routines_run['D'](ab)	# => AB running: [1, 0], [1, 0]
	exit()


	''' Early experimentation with different methods to store functions for polymorphic behavior '''
	from functools import *
	
	def do_op(obj, *args, **kws):
		result = obj.do(*args, **kws)
		return result
	def op(obj, name, **kws):
		func = getattr(obj, name)
		result = func(obj, **kws)
		return result
	do = partial(op, name='do', input='better')
	run = partial(op, name='run', input='faster')
	
	def foobar_template(op, other=None):
		''' needing a generated lambda taking an object
			1. parameters are predefined constants
			2. this lambda generator takes a function as input
			3. obj is given at runtime
		'''
		result = lambda obj: op(obj, 'foobar_const')
		return result
	
	### Configuration #############################
	a = A()
	aa = AA()
	ab = AB()

	### Runtime ###################################
	do(A, other=0)
	do(AA, other=1)
	do(AB, other=2)
	run(A, other=3)
	run(AA, other=4)
	run(AB, other=5)
