
''' basic observer patterns using set as the collection of listeners '''
from __future__ import print_function

from pprint import pprint as pp
from collections import defaultdict as ddict
from threading import RLock

from pyutils.defs.colldefs import *
from pyutils.common.thread import atomic
from pyutils.structures.maps import KeysMap
from pyutils.utilities.sequtils import splitargs



''' Todo:
	Needing to transition to a more generic naming convention as the foundation Event/Listener design
	pattern structures for other more specialized implementations.
'''


def is_pre_event_handler_cond(publisher, handler):
	'''
		Usage:
		class SimplePreEventHandler:
			is_pre_event_handler = True
		class AdvPreEventHandler:
			is_pre_event_handler = [PreEventPubl_A(), PreEventPubl_B()].__contains__
	'''
	is_pre_event_handler = getattr(handler, 'is_pre_event_handler', False)
	if is_pre_event_handler not in (True, False):
		is_pre_event_handler = is_pre_event_handler(publisher)
	return is_pre_event_handler

def is_on_event_handler_cond(publisher, handler):
	'''
		Usage:
		class SimpleOnEventHandler:
			is_on_event_handler = True
		class AdvOnEventHandler:
			is_on_event_handler = [OnEventPubl_A(), OnEventPubl_B()].__contains__
	'''
	is_on_event_handler = getattr(handler, 'is_on_event_handler', False)
	if is_on_event_handler not in (True, False):
		is_on_event_handler = is_on_event_handler(publisher)
	return is_on_event_handler

def is_post_event_handler_cond(publisher, handler):
	'''
		Usage:
		class SimplePostEventHandler:
			is_post_event_handler = True
		class AdvPostEventHandler:
			is_post_event_handler = [PostEventPubl_A(), PostEventPubl_B()].__contains__
	'''
	is_post_event_handler = getattr(handler, 'is_post_event_handler', False)
	if is_post_event_handler not in (True, False):
		is_post_event_handler = is_post_event_handler(publisher)
	return is_post_event_handler




class Publisher:
	''' Basic class for issuing calls to handlers when triggered '''
	_cond = None
	
	def __init__(self, handlers=None, *pa, **ka):
		handlers = None if handlers is None else tuple(handlers)
		self._handlers = KeysMap(id, handlers) if handlers else None	# if not empty coerce to KeysMap, otherwise as None
		self.lpa, self.rpa = splitargs(pa)
		self.ka = ka or None
	def hook(self, handler):
		if self._handlers is None:
			self._handlers = KeysMap(id)
		self._handlers.add(handler)
	def unhook(self, handler):
		self._handlers.discard(handler)
	def notify(self, *pa, cond=None, _augment=True, **ka):
		''' broadcast action or event to registered handlers
			:param cond: condition which determines if the handlers receive notifications
			:param _augment: when true, augment args with internal publisher defaults; otherwise, do not modify args
			:param pa: position args
			:param ka: keyword args
		'''
		if _augment:
			pa = *self.lpa, *pa, *self.rpa
			ka = dict(self.ka or {}, **ka)  # overwrite self.ka with passed ka
		if cond and self._cond:
			cond = self.simplify_conditions(self._cond, cond)
			
		for handler in self.handlers:
			if not cond or cond(self, handler):  # consider allowing condition to operate on args
				handler(*pa, **ka)
	def emit(self, *pa, **ka):
		self.notify(self, *pa, **ka)
	def notify_pre_event(self, event, *pa, **ka):
		''' notify only handlers which explicitly accept pre-event given this publisher '''
		event.event_phase = 'post_event'
		self.notify(event, *pa, cond=is_pre_event_handler_cond, **ka)
	def notify_post_event(self, event, *pa, **ka):
		''' notify only handlers which explicitly accept post-event given this publisher '''
		event.event_phase = 'post_event'
		self.notify(event, *pa, cond=is_post_event_handler_cond, **ka)
	def notify_on_event(self, event, *pa, **ka):
		''' notify only handlers which explicitly accept on-event given this publisher
			Note: not the same as notify which by default will not check handlers pre/on/post event policy
		'''
		event.event_phase = 'post_event'
		self.notify(event, *pa, cond=is_on_event_handler_cond, **ka)
	def simplify_conditions(self, *conditions):
		return conditions[0]  # todo
	def sequencer(self):
		return EventSeq(self)
	
	# alias
	# handler = property(lambda self: self._handlers and list(self._handlers.values())[0])  # returns None or 1st value
	handlers = property(lambda self: iter(self._handlers and self._handlers.values() or ()))


class EventPublisher(Publisher):
	
	event_phase = property(
		lambda self: getattr(self, '_event_phase', 'on_event'),
		lambda self, phase: setattr(self, '_event_phase', phase),
	)
EventPubl = EventPublisher
Event = EventPublisher  # obsolete alias: due to potential conflicts


class EventSeq:
	''' An event sequence. Can also collect then commit events in batch as context manager for event Publishers. '''
	
	def __init__(self, events=None, publisher=None, active=False):
		self.events = events or []
		self.publisher = publisher
		if active and publisher:
			self.collect(publisher)
	def collect(self):
		''' enter event collection mode '''
		self.publisher.hook(self)
	def condition(self, handler, *pa, **ka):
		''' while collecting event sequence, do not allow other handlers to receive events '''
		return handler is self
	def add(self, *pa, **ka):
		self.events.append([pa, ka or None])
	def commit(self, exc_type=None, exc_val=None, exc_tb=None, *, publisher=None):
		''' exit event collection mode and issue events; commit() is reentrant if alternative publisher is given
			:param publisher: publisher to which collected events will be issued
		'''
		# exception occurred during 'with EventSeq():' context
		if exc_val:	raise exc_val
		
		# publish collected events to given publisher
		publisher = publisher or self.publisher
		for pa, ka in self.events:
			publisher.notify(*pa, _augment=False, **ka or {})
			
		# end event collection on self.publisher
		if self.publisher and publisher is self.publisher:
			self.publisher.unhook(self)
			self.publisher = None
			self.events = []
		
	# method aliases
	__enter__ = collect
	__exit__ = commit
	__call__ = on_event = add


		
''' The following Handler classes show the convention for patterns involving
	a Handler of a Publisher and do not require subclassing.
'''
class Handler:
	def on_event(self, event, **kargs): raise NotImplemented()
	
class PreEventHandler(Handler):		is_pre_event_handler = True
class OnEventHandler(Handler):		is_on_event_handler = True
class PostEventHandler(Handler):	is_post_event_handler = True
	



		
''' More Advanced conditional listener patterns '''


class StateUpdate:
	''' container for single state change handler '''
	# obsolete: use pyutils.Model
	def __init__(self, handler, state_updated=False):
		self.handler = handler
		self.state_updated = state_updated
	def handle(self):
		''' handle if state updated '''
		if self.state_updated:
			self.handler()
		self.state_updated = False
	def updated(self, state_updated=True):
		''' set state_updated to be handled later '''
		self.state_updated = state_updated
	def update(self):
		''' update state and handle immediately '''
		self.handler()
		self.state_updated = False

class StateUpdateCustom(StateUpdate):
	''' Container for single parameterized state change handler '''
	def __init__(self, handler, *update_args, state_updated=False, **update_kws):
		self.handler = handler
		self.state_updated = state_updated
		self.update_params = update_args, update_kws
	def handle(self):
		''' handle if state updated '''
		if self.state_updated:
			self.handler(*self.update_params[0], **self.update_params[1])
		self.state_updated = False
	def update(self):
		''' update state and handle immediately '''
		self.handler(*self.update_params[0], **self.update_params[1])
		self.state_updated = False
	def updated_params(self, *update_args, state_updated=True, **update_kws):
		''' set state_updated and update parames to be handled later '''
		self.state_updated = state_updated
		self.update_params = update_args, update_kws
	def update_params(self, *update_args, **update_kws):
		''' update state with given params and handle immediately '''
		self.update_params = update_args, update_kws
		self.handler(*self.update_params[0], **self.update_params[1])
		self.state_updated = False
		

class CondHandler:
	''' container defining the callback to an event and which events are applicable '''

	def __init__(self, callback=None, bound_types=None, condition=None, until=None):
		''' construct '''
		self.callback = callback or self.on_event
		bound_types = bound_types or [type(None)]  # null-coalesce to value for accepting all types
		bound_types = isinstance(bound_types, type) and [bound_types] or bound_types  # change single to iter
		self.bound_types = [(isinstance(t, type) and t or type(t)) for t in bound_types]
		self.condition = condition
		self.until = until

	# override these callbacks in subclasses or provide callback as constructor parameters
	def on_event(self, event_data=None, *args, **kws):
		''' invoke configured callback '''
		self.callback(event_data, *args, **kws)
	def _condition(self, event_data, as_type=None, *args, **kws):	raise NotImplemented()
	def _until(self, event_data, as_type=None, *args, **kws):		raise NotImplemented()

	# method aliases
	__call__ = on_event


class CondPublisher:
	''' container that invokes a callback configured for applicable events '''
	__bool__ = __nonzero__ = lambda x: True

	def __init__(self):
		''' construct '''
		self.event_handlers_map = ddict(lambda: [])
		self.count = 0
		self.lock = RLock()
	# def __len__(self):
	# 	return self.count
	@property
	def handlers(self):
		result = []
		for handlers in self.event_handlers_map.values():
			result.extend(handlers)
		return result

	def subscribe(self, event_handler):
		with atomic(self.lock):
			bound_types = isinstance(event_handler, EventHandler) and event_handler.bound_types or [type(None)]
			for bount_type in bound_types:
				self.event_handlers_map[bount_type].append(event_handler)
				self.count += 1
		return self

	def unsubscribe(self, event_handler):
		''' broadcast event to applicable handlers '''
		for bount_type in event_handler.bound_types:
			if event_handler in self.event_handlers_map[bount_type]:
				self.event_handlers_map[bount_type].remove(event_handler)
				self.count -= 1
				if len(self.event_handlers_map[bount_type]) == 0:
					with atomic(self.lock):
						self.event_handlers_map.pop(bount_type)

		return self

	def emit(self, event_data, as_type=None, *args, **kws):
		''' broadcast event to applicable handlers '''
		as_type = as_type or type(event_data)
		with atomic(self.lock):
			for bound_type, event_handlers in self.event_handlers_map.items():
				for event_handler in event_handlers:
					if not isinstance(event_handler, EventHandler):
						event_handler(event_data, *args, **kws)
					elif event_handler.until and event_handler.until(event_data, as_type=as_type, *args, **kws):
						self.unsubscribe(event_handler)
					elif ((event_handler.condition and event_handler.condition(event_data, as_type, *args, **kws))
					or (event_handler.condition is None and (bound_type is type(None) or issubclass(as_type, bound_type)))):
						event_handler(event_data, as_type=as_type, *args, **kws)

	# method aliases
	__iadd__ = subscribe
	__isub__ = unsubscribe
	publish = emit
	
# obsolete aliases
EventHandler = CondHandler
EventPublisher = CondPublisher

# class Subscriber(object):
# 	def __init__(self, name):
# 		self.name = name
# 	def update(self, event):
# 		print('{} received an update for item: "{}"'.format(self.name, event))

class Handlers(list):
	def __contains__(self, handler):
		result = next((item for item in self if item[0] == handler), None)
		return result is not None
	def __iand__(self, handler, condition=None, until=None):
		return self.__iadd__(handler, condition, until, allow_duplicate=False)
	def __iadd__(self, handler, condition=None, until=None, allow_duplicate=True):
		if allow_duplicate or handler not in self:
			self.append((handler, condition, until))
		return self
	def __isub__(self, handler):
		index = next((i for i, item in enumerate(self) if item[0] == handler), None)
		if index is not None:
			self.pop(index)
		return self
	def emit(self, event):
		self.emit_many([event])
	def emit_many(self, events):
		# handlers = list(self)
		for event in events:
			for handler, condition, until in self:
				if until is not None and until(event):
					self -= handler
				elif condition is None or condition(event):
					handler(event)






def test_event_subscription_basic():
	# imports
	class View(str): pass
	class Like(str): pass
	class Comment(str): pass
	class MyPublisher(EventPublisher): pass
	class MyHandler(EventHandler):
		def on_event(self, event, *args, **kws): print('{}.on_event( {}("{}") )'.format(self.__class__.__name__, event.__class__.__name__, event))
	class MyNewHandler(MyHandler): pass
	class MyImprovedHandler(MyHandler): pass

	# construct
	publ_a = MyPublisher()
	publ_b = MyPublisher()
	processor_any = MyHandler()
	processor_views = MyNewHandler(bound_types=View)
	processor_views_likes = MyImprovedHandler(bound_types=[View, Like])

	# initialize
	publ_a += processor_views
	publ_a += processor_views_likes
	publ_b += processor_any
	publ_b += processor_views_likes

	# runtime
	publ_a.emit(View('Alice'))		# View type event received 2 view processors
	publ_a.emit(Comment('Bob'))		# Comment type event ignored 2 processors
	publ_b.emit(Like('Charley'))	# Like type event received by view processor and arbitrary processor
	publ_b.emit(Comment('Doug'))	# Comment type event received by 1 arbitrary processor, ignored by other processor

	# results:
	# 	MyNewHandler.on_event( View("Alice") )
	# 	MyImprovedHandler.on_event( View("Alice") )
	# 	MyHandler.on_event( Like("Charley") )
	# 	MyImprovedHandler.on_event( Like("Charley") )
	# 	MyHandler.on_event( Comment("Doug") )
	pass


if __name__=='__main__':
	# tested & working
	test_event_subscription_basic()
