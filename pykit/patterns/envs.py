import os
import sys
import yaml
import json
import shutil
from pprint import pprint as pp, pformat as pfmt
from dataclasses import dataclass, field

from pyutils.extend.datastructures import XList, AttrMap
from pyutils.patterns.accessors import *
from pyutils.structures.attrtree import *
from pyutils.structures.strs import Strs


__all__ = Strs('DefsEnvs Envs local_envs local_src_envs '
			   'walk_dir_tree deep_keys deep_attrs walk_items')

log = None

class DefsEnvs:
	file_ptrn		= 'envs.(json|ya?ml)$'
	default_config	= '/etc/envs/'
	get_backup_path	= lambda path, index=1: path + f'.{index or "orig"}'
	fmt_str			= '{:<20}= {!r:.60}'
	getter			= AttrMap(key=lambda item: item[0], value=lambda item: item[1], item=None)
	log				= AttrMap(error=0x01, config=0x02, found=0x04, result=0x08)
	loglvls = AttrMap(
		quiet		= 0,
		error		= log.error,
		info		= log.error|log.config,
		debug		= log.error|log.config|log.found,
		trace		= log.error|log.config|log.found|log.result,
	)
Defs = DefsEnvs

def walk_dir_tree(path):
	path = path.strip().rstrip('/')+'/'
	yield path
	
	path = re.sub(r'[^/]*/*$', '', path)
	if path:
		yield from walk_dir_tree(path)
	
def deep_keys(content, sort_keys=True):
	result = []
	for key, value in content.items():
		result.append(key)
		if isinstance(value, dict):
			keys = deep_keys(value, sort_keys)
			result.append(keys)
	if sort_keys:
		result.sort()
	return result

def deep_attrs(content, sort_keys=True):
	result = []
	for key, value in content.items():
		if isinstance(value, dict):
			subkeys = deep_attrs(value, False)
			result.extend([f'{key}.{subkey}' for subkey in subkeys])
		else:
			result.append(key)
	if sort_keys:
		result.sort()
	return result


class _Envs(AttrTree):
	''' A class that aggregates a layered hierarchy of *envs.yaml or *envs.json into contextual
		environment variables.  A context's environment variables are comprised of the variables defined
		in a files ending in envs.yaml or envs.json	in the working directory or its parent directories.
		Depth most config files take precedent.
		
		Conventions:
		- methods have been prefixed with '_' to hide it from autocompletion.
		- lexicon given content like: ['envs.a.b.c', 'envs.d.e']
			- entries: ['envs.a.b.c', 'envs.d.e']
			- entry: 'envs.a.b.c'
			- key: 'a' or 'b' or 'c'
	'''
	_width_indent, _width_key, _width_val = 2, 28, 60
	_fmtr_key = lambda key, width_k: "{}{:<{w}}{}".format(' '*2, key, len(key) > width_k and '\n'+(' '*(2+width_k)) or '', w=width_k)
	_fmtr_value = lambda value, width_v: len(value) > width_v and f'{value:.{width_v}}...' or f'{value}'
	content = {}
	
	def __init__(self, context_path=None, caller_path=None, content=None, logflags=Defs.log.config, readonly=True):
		self.content = content or {}
		self.context_path = context_path or os.path.abspath('.')
		self._caller_path = caller_path and os.path.dirname(caller_path)+'/'  # allow calling source option to add envs
		self.logflags_config = logflags or 0
		self._ctx_layers = []
		self.modified = set()
		self._readonly = readonly
		super().__init__()
	def _pop(self, entry):
		content = self.content
		for key in entry[:-1]:
			content = content[key]
		result = content.pop(entry[-1])
		self.modified.add('.'.join(entry))
		return result
	def _log(self, logflags, *args, **kws):
		''' display debug messages with configured priority and higher '''
		# Note: deliberately not depending on pyutils.common.log due to limited complexity of Envs
		if (logflags or 0) & self.logflags_config:
			print('Envs:', end='')
			print(*args, *kws)
	def _configure_file(self, file_path):
		if not os.path.isfile(file_path):
			return {}
		
		result = Envs._load_yaml(file_path) or Envs._load_json(file_path)
		if result:
			self._log(Defs.log.result, f'Result from loading {file_path}:\n{pfmt(result, width=20)}')
			# self._deep_update(self, result)
			self.update_tree(result)
		return result
	def _locate_layers(self, context_path=None, logflags=None):
		if logflags is not None:
			self.logflags_config = logflags
			
		# get ordered hierarchy of directories to search
		self.context_path = context_path or self.context_path or os.path.abspath('.')
		self._log(Defs.log.config, f'configure({self.context_path})')
		config_dirs = list(walk_dir_tree(self.context_path))
		
		# add source dir at lowest priority if absent
		if self._caller_path and self._caller_path not in config_dirs:
			config_dirs.append(self._caller_path)
		
		# locate applicable ctxenv configs in directories
		self._log(Defs.log.found, f'config_dirs: {list(reversed(config_dirs))}')
		file_paths = []
		for config_dir in reversed(config_dirs):
			self._log(Defs.log.found, f'config_dir: {config_dir}')
			dir_files = os.listdir(config_dir)
			dir_files = XList.like(dir_files, Defs.file_ptrn)
			self._log(Defs.log.found, f'found: {dir_files}\n')
			file_paths.extend([config_dir+dir_file for dir_file in dir_files])
			
		return file_paths
	def _configure(self, context_path=None, logflags=None, suppress=False):
		context_path = context_path or self.context_path
		if not context_path or not os.path.exists(context_path):
			if not suppress:
				raise FileNotFoundError(context_path)
		elif os.path.isdir(context_path):
			self._ctx_layers = self._locate_layers(context_path, logflags)
		else:
			self._ctx_layers = [context_path]
		
		# apply each config file layer giving priority to the longest paths
		self._log(Defs.log.found, f'all found ctx_layers:\n{pfmt(self._ctx_layers)}\n')
		for index, file_path in enumerate(self._ctx_layers):
			self._log(Defs.log.result, f'#{index} Envs config: {file_path}')
			self._configure_file(file_path)
			self._log(Defs.log.result, f'Content after configuring {file_path}:\n{self}\n\n')
			
		if not self.logflags_config & Defs.log.result:
			self._log(Defs.log.found, f'Final content result:\n{self}\n\n')
		
		return self
	def _flush(self, layer_path=None, only_modified=True, logflags=None):
		''' commit modification or complete Envs to file '''
		# exit condition
		if only_modified and not self.modified:
			print('Envs: No modifications to flush.')
			return

		# resolve context file_path
		layer_path = -1 if layer_path is None else layer_path  # use depth-most context file_path as default
		if isinstance(layer_path, int):
			# find contributing context layers
			context_dir = '.'
			ctx_layers = self._ctx_layers or self._locate_layers(context_dir, logflags)
			layer_path = ctx_layers and ctx_layers[layer_path] or '.envs.yaml'
		
		# determine which entries to be exported to file
		if not only_modified:
			context = self
		else:
			context = Envs(layer_path, readonly=False)._configure(suppress=True)
			modified = Envs._prune_overlapping_entries(self.modified)
			for entry in modified:
				entry = entry.split('.')
				# in order to preserve original layered entry structure only appl
				if entry in self:
					context[entry] = self[entry]
				elif entry in context:
					context._pop(entry)
		
		# commit content to file
		context._write(layer_path)
		self.modified.clear()
	def _write(self, layer_path):
		if not self.content:
			print('Envs: Nothing in content to backup. Skipping.')
			return
		
		self._backup(layer_path)
		# todo: choose configured output format
		Envs._write_yaml(layer_path, self.content)
	def _backup(self, layer_path):
		if not os.path.isfile(layer_path):
			return  # skip if file not found
		
		# derive file names for original and previous copies
		orig_copy, prev_copy = [Defs.get_backup_path(layer_path, index) for index in range(2)]
		
		# create copies
		if not os.path.isfile(orig_copy):
			shutil.copy(layer_path, orig_copy)  # copy created once
		shutil.copy(layer_path, prev_copy)		# copy overwritten after each modification
	def _apply_os_env(self):
		''' incorporate os environment variables '''
		# todo
		self.update(os.environ)
	def _toggle_readonly(self, enabled=None):
		if enabled is None:
			self._readonly = not self._readonly
		else:
			self._readonly = enabled
		return self._readonly

	@staticmethod
	def _prune_overlapping_entries(entries):
		''' eliminate duplicate entries where its the parent container is also an entry '''
		entries = set(entries)
		ordered_entries = sorted(list(entries), key=lambda keys: keys.count('.'))
		for entry in ordered_entries:
			container = []
			for key in entry.split('.')[:-1]:
				container.append(key)
				if '.'.join(container) in entries:
					entries.remove(entry)  # removing duplicate entry since its container is an entry
		return entries
	@staticmethod
	def _load_yaml(file_path):
		try:
			with open(file_path, 'r') as file_stream:
				result = yaml.safe_load(file_stream)
		except Exception as e:
			print(e)
			result = None
		return result
	@staticmethod
	def _write_yaml(file_path, content):
		try:
			with open(file_path, 'w') as file_stream:
				result = yaml.dump(content, file_stream, default_flow_style=False)
		except Exception as e:
			print(e)
			result = None
		return result
	@staticmethod
	def _load_json(file_path):
		try:
			with open(file_path, 'r') as file_stream:
				result = json.load(file_stream)
		except Exception as e:
			print(e)
			result = None
		return result
	@staticmethod
	def _write_json(file_path): pass

class Envs(_Envs):
	def __dir__(self):
		return list(self.content)
	
	
@dataclass
class EnvScope(AttrTree):
	''' Predefines config subset and interfaces with Envs. Will load or supply+write defaults for missing fields. '''
	keys: list			= field(default_factory=list)
	key_defaults: dict	= field(default_factory=dict)
	# todo: consolidate with Poly.Args as there seems to be overlapping characteristics
	
	def __post_init__(self):
		super().__init__()
		self.keys = list(set(self.keys + list(self.key_defaults)))  # include key_defaults into keys
		
	def initialize(self, working_dir=None, *args, **kargs):
		self.load(working_dir, *args, **kargs)
		# # todo: write out to working_dir
		# envs = Envs()
		# envs.update(self)
		# envs._flush('.')
	def load(self, working_dir=None, resolve_vars=True):
		envs = local_envs(working_dir)
		for key in self.keys:
			if key in envs:
				value = envs[key]
			elif key in self.key_defaults:
				value = self.key_defaults[key]
			else:
				continue
				
			value = self.resolve(value, envs) if resolve_vars else value
			self[key] = value
	def resolve(self, var_str, envs, error=False):
		''' substitute values from envs into var references in given var_str '''
		if not isinstance(var_str, str): return var_str
		
		var_history = set()
		var_ptrn = r'{{([^{}]+)}}'
		
		# perform multiple passes of var substitution in case of compound vars
		while True:
			vars = set(re.findall(var_ptrn, var_str))
			unfamiliar_vars = vars - var_history
			if not any(unfamiliar_vars):
				break  #
				
			# apply envs to each occurrence of unfamiliar_vars
			for key in unfamiliar_vars:
				if key in envs:
					resolved = str(envs[key])
					var_str = var_str.replace('{{%s}}'%key, resolved)
			var_history |= unfamiliar_vars
		
		return var_str


def local_envs(*pa, **ka):
	frame = sys._getframe(1)
	caller_path = f'{frame.f_code.co_filename}#{frame.f_lineno}'
	Defs.log.config & ka.get('logflags', Defs.log.config) and print(f'\nEnvs:local_envs() <- {caller_path}')
	result = Envs(*pa, **ka)._configure()
	return result

def local_src_envs(*pa, **ka):
	''' same as local_envs but adds the calling source envs path by default.
		local_envs() is recommended as default option.
	'''
	frame = sys._getframe(1)
	caller_path = f'{frame.f_code.co_filename}#{frame.f_lineno}'
	Defs.log.config & ka.get('logflags', Defs.log.config) and print(f'\nEnvs:local_src_envs() <- {caller_path}')
	result = Envs(*pa, caller_path=caller_path, **ka)._configure()
	return result