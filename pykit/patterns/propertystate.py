''' A collection of classes that encapsulate common or fundamental variable assignment behaviors '''

from datetime import datetime, timedelta
from dataclasses import dataclass, field

from pyutils.extend.datastructures import XDict
from pyutils.defs.worddefs import *

__all__ = [
	'PropertyHistory',
	'PropertyDelta',
	'IPropertyQuery',
	'PropertyElapsed',
	'PropertyReaction',
	'CacheProperty',
]

class PropertyHistory(object): # succession, course, record, chronology, history
	def __init__(self, func=None, args=None, kwargs=None):
		self.func = func or (lambda arg: arg)
		self.args = args or []
		self.kwargs = kwargs or {}
		self.history = []

	def advance(self, *args, **kwargs):
		pass_args = list(args) + list(self.args)
		pass_kwargs = XDict(kwargs) + self.kwargs
		result = self.func(*pass_args, **pass_kwargs)

		self.history.append(result)
		return result

	__call__ = advance  # default behavior verb


class PropertyDelta(object):
	''' react to property variable updates and implement routine behaviors '''
	def set(self, value):
		updated = False
		if self.value != value:
			self.value = value
			updated = True
		return updated


class IPropertyQuery(object):
	def __call__(self, *args, **kwargs): pass
	def peak(self, *args, **kwargs): pass


class PropertyElapsed(IPropertyQuery):
	def __init__(self, period=0, immediate=True, **timedelta_kws):
		''' construct
			:param period:			amount of time to satisfy query; accepts int number of seconds or timedelta
			:param immediate:		if True property query is satisfied now; otherwise it is False until period elapses
			:param timedelta_kws:	timedelta keywords to match timedelta constructor inputs
		'''
		# Note: below '_t' indicates a point in time/datetime; '_dt' indicates a time vector/timedelta
		self.previous_t = datetime.now()
		self.period_dt = period
		if not isinstance(period, timedelta):
			self.period_dt = timedelta(seconds=timedelta_kws.get('seconds', period or 0), **timedelta_kws)
		if immediate:
			self.previous_t = self.previous_t - self.period_dt

	def __call__(self, period=None, peak=False, **timedelta_kws):
		''' evaluate and return elapsed condition; when true update property for next query unless peaking '''
		now_t = datetime.now()
		period_dt = self.period_dt
		if period or timedelta_kws:
			period_dt = period if isinstance(period, timedelta) else (
				timedelta(seconds=timedelta_kws.get('seconds', period or 0), **timedelta_kws))
		next_t = self.previous_t + period_dt

		# evaluate elapsed property
		if now_t < next_t:
			return False

		# elapsed query will return true; dont dont make changes if peak is True
		if not peak:
			self.__init__(period_dt, immediate=False)
		return True

	def peak(self, period=None, **timedelta_kws):
		''' evaluate and return elapsed condition only; dont modify query '''
		return self(period, peak=True, **timedelta_kws)

@dataclass
class PropertyReaction:
	on_update: callable
	value: 'Any' = None
	pass_args: bool = False
	pass_inst: bool = True
	
	def __get__(self, instance, owner):
		return self.value
	def __set__(self, instance, value):
		self.value = value
		kws = self.pass_args and dict(value=value, prop=self) or {}
		self.on_update(instance, **kws)


class CacheProperty:
	sign_seq_first = lambda seq: (len(seq), seq and hash(seq[1]))
	sign_seq_last = lambda seq: (len(seq), seq and hash(seq[-1]))
	
	def __init__(self, evaluate, sign_state, initial=nothing):
		self._evaluate = evaluate
		self._sign_state = sign_state
		self._cached = initial
		self._sig = initial
	def __get__(self, instance, owner):
		sig = self._sign_state(instance)
		if self._sig != sig:
			print('cache miss')
			self._cached = self._evaluate(instance)
			self._sig = sig
		else:
			print('cache hit')
		return self._cached
	def clear(self):
		self._cached = nothing
	



if __name__=='__main__':
	def changed(item):
		print('handling updated item:%s' % item)

	f = PropertyDelta(0, changed)
	if f.set(1):
		changed(f)
	f.set(2, changed)
	f.update(3)


