import itertools
# from collections import Iterable
from dataclasses import dataclass
# from pyutils.structures.names import *
from pyutils.structures.members import *


class DataAccess:
	def __init__(self, name=None, **kws):
		self.name = name
	def renew(self, **kws):
		return self
	def __str__(self):
		return self.name
	@property
	def key(self):
		return self.name
	def get(self, data):
		''' get field series from a dataset corresponding to self.name '''
		result = data.get(self)
		return result
	def evaluate(self, data): raise NotImplementedError()
	def __add__(self, other):
		result = Evaluator(op.add, self, other)
		return result
	def __sub__(self, other):
		result = Evaluator(op.sub, self, other)
		return result
	def __mul__(self, other):
		result = Evaluator(op.mul, self, other)
		return result
	__call__ = get
	__repr__ = __str__

	
class Accessor(DataAccess):
	def __init__(self, attr, window=None, reverse=True, **kws):
		self.attr = attr
		self.window = window
		self.reverse = reverse
		super().__init__(**kws)
	def __str__(self):
		result = self.name or f'{self.__class__.__name__}.{self.attr}{self.window or ""}'
		return result
	def __getitem__(self, index):
		result =  Accessor(self.attr, Window(index, self.reverse), self.reverse)
		return result
	@property
	def key(self):
		return self.name or self.attr
	def evaluate(self, data, **kws):
		# result = getattr(data, self.attr)
		result = data[self.attr]
		if self.window:
			result = self.window.get(result, **kws)
		else:
			result = result[self.reverse and -1 or 0]
		return result
	__repr__ = __str__
	
@dataclass
class _AccessorFactory:
	reverse: bool = True
	def __getattr__(self, attr):
		return Accessor(attr, reverse=self.reverse)

Head = _AccessorFactory(reverse=False)
Tail = _AccessorFactory()




class Iters:
	''' Class takes some iterable content and returns iterators of subcomponents when the iter is
		called with a subcoponent accessor. Use list constructor list(Iters()) or iter() function
		to resolve Iters() so that it can be indexed or traversed.
		Syntatic sugar equivalent of:
		map(lambda obj: obj.subitem, objs)  # or
		map(lambda obj: obj[0], objs)

		Usage:
		iobjs = Iters(objs)
		iobjs.subitem		# returns an iterator of subitem members for each obj
		iobjs[-1] 			# returns an iterator of the last item for each obj
		iobjs.subitem[-1]	# returns an iterator of the last item of the subitem member for each obj
	'''
	accessor_constraints = (getitem_only, getattr_only, other) = range(3)
	def __init__(self, content):
		self.content = content
		self._iter = iter(self.content)
	def __iter__(self):
		return self._iter
	def __next__(self):
		return next(self._iter)
	next = __next__
	def call(self, accessor):
		# todo
		if '':
			result = itertools.imap(accessor, self.content)
		if '':
			result = map(accessor, self.content)
		return result
	def __getitem__(self, index):
		return self._get_each(index, accessor_constraint=Iters.getitem_only)
	def __getattr__(self, key):
		return self._get_each(key, accessor_constraint=Iters.getattr_only)
	def _get_each(self, key, accessor_constraint=getitem_only):
		if isinstance(self.content, types.GeneratorType):
			result = Iters((item for item in self._gen_each(key, accessor_constraint)))
			return result

		if isinstance(key, Keys):
			accessor_constraint = Iters.other

		result = []
		for item in self.content:
			item_element = item
			if accessor_constraint == Iters.other:
				item = key.get(item)
			elif accessor_constraint == Iters.getitem_only:
				item = item[key]
			elif accessor_constraint == Iters.getattr_only:
				item = getattr(item, key)
			result.append(item)

		result = Iters(result)
		return result

	def _gen_each(self, key, accessor_constraint, iterator_result=False):
		if isinstance(key, Keys):
			accessor_constraint = Iters.other

		for item in self.content:
			item_element = item
			if accessor_constraint == Iters.other:
				item = key.get(item)
			elif accessor_constraint == Iters.getitem_only:
				item = item[key]
			elif accessor_constraint == Iters.getattr_only:
				item = getattr(item, key)
			yield item



def mirror_index(index):
	if isinstance(index, int):
		index = -index - 1
	else:
		cls = type(index)
		index = cls(-index.start -1, -index.stop -1, -index.step)
	return index

class Sampler:
	''' Represents a slice for a named data series.  Reinterprets slice inputs as interpolation rather than a range.
		step: interpolation step size. default=1.
		stop: Number of interpolations. default=1.
		start: where to start interpolation. default=0.
	'''
	def __init__(self, ind, reverse=False):
		self.number_index = type(ind) is int
		self.reverse = reverse
		
		# out of bound int index is processed as an empty list rather to avoid exceptions
		ind = slice(ind,ind+1 or None) if type(ind) is int else ind  # coerce to slice
		self.size = ind.stop or 1
		self.ind = Sampler.decode(ind)
		if self.reverse:
			self.ind = mirror_index(self.ind)
	@property
	def mirror(self):
		result = Sampler(self.ind, reverse=True)
		return result
	def __str__(self):
		if isinstance(self.ind, slice):
			result = f'[{self.ind.start or ""}:{self.ind.stop or ""}:{self.ind.step or ""}]'
		else:
			result = f'[{self.ind}])'
		return result
	@staticmethod
	def decode(index):
		''' given a slice for interpolation return a normal slice that produces the same result '''
		cls = type(index)
		start = index.start or 0
		step = index.step or 1
		stop = (index.stop or 1) * step  # as sample, interpret stop as total samples
		result = cls(start,stop,step)
		return result
	def get(self, data, default=0., fill=True):
		''' get field(s) from a dataset corresponding to self.ind slice '''
		result = data[self.ind]
		if fill and len(result) < self.size:
			result[10000:] = [default] * (self.size - len(result))
		if self.number_index:
			result = result[0]
		return result
	__repr__ = __str__
	__call__ = get
Window = Sampler
