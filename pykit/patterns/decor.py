import functools
import dataclasses as _dc

class staticmethod_self_or_none(object):
	''' decorated method can be used as a static method with self == None in addition to the expected
		instance method behavior.
		usage:
		class Resource(object):
			@staticmethod_self_or_none
			def lock(self):
				self = self if self is not None else Resource('System')
				...
		Resource('InstanceId').lock()	# instance method is still a valid
		Resource.lock()					# static method also valid (normally throws exception)
	'''
	def __init__(self, func):
		self.func = func

	def __get__(self, instance, owner):
		''' produces static method prepends None as first/self arg or
			produces instance method with no modifications to arguments or behavior
		'''
		result = functools.partial(self.func, instance)
		return result


class staticmethod_optional_self(object):
	''' decorator in essence makes self arg passable on class methods.  Decorated method can be used
		as a static method with self input arg in addition to the expected instance method behavior.
		usage:
		class MyDict(dict):
			@staticmethod_optional_self
			def get(self, key):
				...
		a = MyDict({0:10}).get(0)		# instance method is still a valid
		b = MyDict.get({0:10}, 0)		# static method also valid (normally throws exception)
		print(a == b)					# => True
	'''
	def __init__(self, func):
		self.func = func

	def __get__(self, instance, owner):
		''' produces instance & static method with arguments unmodified instead of None as self arg.
			None prepended to arg list by functools.partial() is undone.
		'''
		args = instance is not None and [instance] or []
		result = functools.partial(self.func, *args)
		return result


class staticmethod_self_or_single(object):
	''' decorated method can be used as a static method with self == None in addition to the expected
		instance method behavior.
		usage:
		class Resource(object):
			@staticmethod_self_or_none
			def lock(self):
				self = self if self is not None else Resource('System')
				...
		Resource('InstanceId').lock()	# instance method is still a valid
		Resource.lock()					# static method also valid (normally throws exception)
	'''
	def __init__(self, func, singleton_member='_inst'):
		self.func = func
		self.singleton_member = singleton_member

	def __get__(self, instance, owner):
		''' produces static method prepends None as first/self arg or
			produces instance method with no modifications to arguments or behavior
		'''
		inst = getattr(owner, self.singleton_member)
		result = functools.partial(self.func, instance if instance is not None else getattr(owner, self.singleton_member))
		return result


### prepdataclass functions

field = _dc.field

def _wrap_init(self, *args, **kws):
	''' call user defined __prep_args__ before passing args to standard dataclass generated __init__ '''
	prep_args, prep_kws = self.__prep_args__(*args, **kws)
	self.__datacls_init__(*prep_args, **prep_kws)

def _wrap_process_class(cls, init, repr, eq, order, unsafe_hash, frozen):
	''' Behaves same as _process_class() performing standard dataclass operations.
		Additionally substitutes generated __init__ for _wrap_init
	'''
	# generate dataclass functions for cls as expected
	result =  _dc._process_class(cls, init, repr, eq, order, unsafe_hash, frozen)
	
	# move __init__ > __datacls_init__ and _wrap_init > __init__
	cls.__datacls_init__ = cls.__init__
	cls.__init__ = _wrap_init
	
	# provide noop __prep_args__ if missing
	if '__prep_args__' not in vars(cls):
		cls.__prep_args__ = lambda self, *args, **kws: (args, kws)
		
	return result

def prepdataclass(_cls=None, *, init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False):
	''' identical to dataclass() decorator except with _wrap_process_class(...)
		substituted for dataclasses._process_class(...)
	'''
	def wrap(cls):
		return _wrap_process_class(cls, init, repr, eq, order, unsafe_hash, frozen)

	# See if we're being called as @dataclass or @dataclass().
	if _cls is None:
		# We're called with parens.
		return wrap

	# We're called as @dataclass without parens.
	return wrap(_cls)


def declclass(arg0):
	''' call a function to modify decorated class. when function arg0 is missing use __post_def__() '''
	def _declclass(cls, post_decl_func=arg0):
		for base in cls.__mro__:
			if post_decl_func:
				break
			post_decl_func = '__post_decl__' in base.__dict__ and base.__post_decl__.__func__
		post_decl_func(cls)
		return cls
	
	if isinstance(arg0, type):
		return _declclass(arg0, post_decl_func=None)
	return _declclass


""" Moved into pyutils.patterns.capsules

### properties serving as routes to nested attributes

class Fabricator: pass

def auto_kargs(attrfab, locals, prefer_self=True):
	self_attrs = {} if prefer_self is None else getattr(locals.get('self'), '__dict__', {})
	arg_keys = func_arg_keys(attrfab)
	avail_keys = tuple(locals.keys()) + tuple(self_attrs.keys())
	# miss_keys = set(avail_keys).difference(arg_keys)
	#
	# # generate KeyError since KeyErrors are not raised below
	# if miss_keys:
	# 	raise KeyError(f'Args {miss_keys} not found in locals or locals["self"] keys: {avail_keys}')
	
	# pull args from locals or locals['self']
	prime, aux = (self_attrs, locals) if prefer_self else (locals, self_attrs)
	kargs = {arg_key: prime.get(arg_key, aux.get(arg_key)) for arg_key in arg_keys}
	return kargs
def auto_apply(attrfab, locals):
	Kargs = auto_kargs(attrfab, locals)
	obj = attrfab(**Kargs)
	return obj

def _coerce_to_from_keys(to_from_keys):
	''' coerce to_from_keys as delimited str to str list then
		coerce each str in to_from_keys to (from_key, to_key) duplicate str pair
	'''
	to_from_keys = to_from_keys.split() if isinstance(to_from_keys, str) else list(to_from_keys)
	for ind, to_from_key in enumerate(to_from_keys):
		if isinstance(to_from_key, str):
			to_from_keys[ind] = (to_from_key, to_from_key)
		elif len(to_from_key) != 2:
			raise ValueError()
	return to_from_keys

class _AttrProp:
	__slots__ = ['getter', 'attr_key']
	def __init__(self, getter, attr_key):
		self.getter = getter
		self.attr_key = attr_key
	def __get__(self, obj, cls):
		if obj is None: return self
		
		got = getattr(obj, self.getter) if isinstance(self.getter, str) else self.getter(obj)
		result = getattr(got, self.attr_key)
		return result

def route(cls, attrgetter, to_from_keys):
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		setattr(cls, to_key, _AttrProp(attrgetter, from_key))
	return cls

class routes:
	''' apply properties to *cls* for each *to_from_key* route on the getter(inst) result.
		__init__ -> __orig_init__
		attrroutes_init -> __init__
		:param cls: class to augment with properties
		:param attrgetter:
		:param to_from_keys:
		:param attrfab:
		:param autoargs: when true auto init *attrfab* with locals()
	'''

	def __init__(self, attrgetter, to_from_keys):
		self.attrgetter = attrgetter
		self.to_from_keys = to_from_keys
	def __call__(self, cls):
		self.add(cls, **self.__dict__)
		return cls
	@classmethod
	def add(decorator, cls, attrgetter, to_from_keys, **_):
		to_from_keys = _coerce_to_from_keys(to_from_keys)
		for from_key, to_key in to_from_keys:
			setattr(cls, to_key, _AttrProp(attrgetter, from_key))
		return cls


class routesattr(routes):
	'''	Modify a class with a new member attribute property accessors to select sub-attributes.
		apply properties to *cls* for each *to_from_key* route on the getter(inst) result.
		__init__ -> __orig_init__
		attrroutes_init -> __init__
		:param attrgetter:
		:param to_from_keys:
		:param attrfab:
		:param autoargs: when true auto init *attrfab* with locals()
		
		:param cls: class to augment with properties
	'''

	def __init__(self, attr, to_from_keys, attrfab, autoargs=True):
		super().__init__(attr, to_from_keys)
		self.attrfab = attrfab
		self.autoargs = autoargs
	@classmethod
	def add(decorator, cls, attrgetter, to_from_keys, attrfab, autoargs=True, **_):
		attr = attrgetter
		cls.__post_init_attrs__ = decorator.post_init_attrs
		cls.__orig_init__ = cls.__init__
		cls.__init__ = decorator.attr_routes_init
		
		# store in *cls*._post_init_attrs data needed to create and apply *attr* during instantiation
		if not hasattr(cls, '_post_init_attrs'):
			cls.__post_init_attrs = {}
		cls.__post_init_attrs.setdefault(cls, ())
		cls.__post_init_attrs[cls] += ((attr, attrfab, autoargs),)
		
		# apply routes to subattrs as properties
		cls = routes.add(cls, attr, to_from_keys)
		return cls
	
	@staticmethod
	def attr_routes_init(inst, *a, **k):
		inst.__orig_init__(*a, **k)
		inst.__post_init_attrs__(*a, **k)
	@staticmethod
	def post_init_attrs(inst, *a, locals=None, **k):
		_post_init_attrs = inst.__post_init_attrs[inst.__class__]
		# construct attr given each attrfab and apply to self instance
		for attr, attrfab, autoargs in _post_init_attrs:
			if hasattr(inst, attr): continue
			
			if not autoargs:
				attr_obj = attrfab()
			elif isinstance(attrfab, Fabricator):
				attr_obj = attrfab.auto_construct(dict(self=inst, **k))
			else:
				attr_obj = auto_apply(attrfab, dict(self=inst, **k))
			setattr(inst, attr, attr_obj)


def routesubattr(cls):
	''' use subattr as fall back for missing attr '''
	return cls
"""
### function activity

def show_args(func):
	def decorated(*args, **kws):
		print(f'args={args} kws={kws}')
		return func(*args, **kws)
	return decorated

def show_return(func):
	def decorated(*args, **kws):
		result = func(*args, **kws)
		print(f'return={result}')
		return result
	return decorated
