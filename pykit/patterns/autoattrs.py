''' Minor addition to attrs module to support creation of classes from annotations '''

from attr import *


def autoattrs(*args, **kargs):
	return attrs(*args, auto_attribs=True, **kargs)