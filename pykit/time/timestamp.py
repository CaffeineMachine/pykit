from datetime import datetime, timedelta
from datetime import datetime as dateclock, timedelta as deltatime

from pyutils.patterns.decor import staticmethod_optional_self
from pyutils.structures.maps import AttrMap

import dateutil.parser

__all__ = ['DefsTime', 'Timestamp']


class DefsTime:
	class CFmt:
		# standard formats
		weekdayid			= '%A'	# e.g. Sunday, Monday, …, Saturday
		weekdayid_abrv		= '%a'	# e.g. Sun, Mon, …, Sat
		monthid				= '%B'	# e.g. January, February, …, December
		monthid_abrv		= '%b'	# e.g. Jan, Feb, …, Dec
		
		weekday				= '%w'	# e.g. 0, 1, …, 6
		month				= '%m'	# e.g. 01, 02, …, 12
		year				= '%Y'	# year with century.  e.g. 0001, …, 2013, 2014, …, 9999
		year_abrv			= '%y'	# Year without century.  e.g. 00, 01, …, 99
		locale_dateclock	= '%c'  # e.g. "Tue Aug 16 21:30:00 1988" (en_US); "Di 16 Aug 21:30:00 1988" (de_DE);
		locale_date			= '%x'	# e.g. "08/16/88" (None); "08/16/1988" (en_US); "16.08.1988" (de_DE);
		locale_clock		= '%X'	# e.g. "21:30:00" (en_US & de_DE)
		microsecond			= '%f'	# e.g. 000000, 000001, …, 999999
		timestamp			= '%s'	# unix timestamp.  e.g. "1578872931"
		day					= '%j'  # day of the year.  e.g. 001, 002, …, 366
		week_sunday			= '%U'	# week number of the year from Sunday.  e.g. 00, 01, …, 53
		week_monday			= '%W'	# week number of the year from Monday.  e.g. 00, 01, …, 53
		date				= '%Y-%m-%d'
		clock				= '%H:%M:%S'
		# composite formats
		clock_ms			= f'{clock}.{microsecond}'
		dateclock_iso		= f'{date}T{clock}'
		dateclock_ms_iso	= f'{date}T{clock}.{microsecond}'
		
		# non-standard formats
		dateclock			= f'{date}+{clock}'
		dateclock_ms		= f'{date}+{clock}.{microsecond}'
		
		# format aliases
		(	wd, mon, yyyy, yy, week,
			ms, t,
			dc, d, c,
			locale_dc, locale_d, locale_c,
		) = (
			weekdayid_abrv, monthid_abrv, year, year_abrv, week_sunday,
			microsecond, timestamp,
			dateclock, date, clock,
			locale_dateclock, locale_date, locale_clock,
		)
		
	Fmt = type('Fmt', (CFmt,), {name:'{:%s}' % value for name, value in vars(CFmt).items() if not name.startswith('_')})
	Fmtr = type('Fmtr', (CFmt,), {name:value.format for name, value in vars(Fmt).items() if not name.startswith('_')})
	
	# not name.startswith('_') and setattr(DefsTime.Fmt, name, f'{{:{value}}}')
Defs = DefsTime.Fmt

# for name, value in vars(DefsTime.CFmt).items():
# 	not name.startswith('_') and setattr(DefsTime.Fmt, name, f'{{:{value}}}')
# for name, value in vars(DefsTime.Fmt).items():
# 	not name.startswith('_') and setattr(DefsTime.Fmtr, name, value.format)

''' To be added:
Time zone name (empty string if the object is naive).
%Z
(empty), UTC, EST, CST
'''


class Timestamp:
	@staticmethod_optional_self
	def as_dateclock(obj=None):
		if isinstance(obj, datetime):
			return obj
		
		if isinstance(obj, (int, float)):
			result = datetime.fromtimestamp(obj)
		elif isinstance(obj, timedelta):
			result = datetime.now() + obj
		elif isinstance(obj, str):
			result = dateutil.parser.parse(obj)
		elif obj is None:
			result = datetime.now()
		else:
			raise TypeError('Expected data param to be int datetime or timedelta.  Got %s' % type(obj).__name__)
		return result
	@staticmethod_optional_self
	def as_int(obj=None):
		if isinstance(obj, int): return obj
		
		dc = Timestamp.as_dateclock(obj)
		result = int(datetime.timestamp(dc))
		return result
	@staticmethod_optional_self
	def dt_to_float(dt):
		if isinstance(dt, float): return dt
		
		t_float = dt.total_seconds()
		return t_float
	@staticmethod_optional_self
	def to_float(obj=None):
		if isinstance(obj, float):
			return obj
		
		dc = Timestamp.as_dateclock(obj)
		t_float = dateclock.timestamp(dc)
		return t_float
	
	@staticmethod_optional_self
	def to_ns(obj=None, prec=9):
		if isinstance(obj, int): return obj
		
		t = Timestamp.as_dateclock(obj)
		prec = min(prec, 9)
		result = int(f'{dateclock.timestamp(t):.0{prec}f}'.replace('.', ''))
		return result
	
	@staticmethod_optional_self
	def ns_to_dc(t_ns):
		t_ns_str = str(t_ns)
		t = float(f'{t_ns_str[:10]}.{t_ns_str[10:]}')
		t = t_ns
		result = dateclock.fromtimestamp(t)
		return result

	@staticmethod_optional_self
	def is_today(obj):
		result = datetime.now().date() == Timestamp.as_datetime(obj).date()
		return result

	@staticmethod_optional_self
	def locale_format(obj=None):
		if not isinstance(obj, datetime):
			obj = Timestamp.as_datetime(obj)
		result = obj.strftime(Defs.locale_dateclock)
		return result

	@staticmethod_optional_self
	def clock_format(obj=None):
		if not isinstance(obj, datetime):
			obj = Timestamp.as_datetime(obj)
		result = obj.strftime(Defs.clock)
		return result

	@staticmethod_optional_self
	def date_format(obj=None):
		if not isinstance(obj, datetime):
			obj = Timestamp.as_datetime(obj)
		result = obj.strftime(Defs.date)
		return result

	@staticmethod_optional_self
	def dateclock_format(obj=None):
		if not isinstance(obj, datetime):
			obj = Timestamp.as_datetime(obj)
		result = obj.strftime(Defs.dateclock)
		return result
	
	as_datetime = as_dateclock
