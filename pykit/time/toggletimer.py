'''
# Toggle Timer

Provide a convenient way to construct, combine and react to multiple variations of timers.


## Time Related Naming Conventions

Terms:
- *date* (d) will will remain unchanged referring to calendar units of time
- *clock* (c) substitutes for references to python standard module time (addt. datetime => dateclock (dc))
- *time* (t) can now be used generally for any/all units of time (including date, clock, dateclock, ... other)
- *deltatime* (dt) substitutes for references to python standard module timedelta

This naming scheme is more consistent and efficient:
- it disambiguates the term *time* (a date is also a time).
- acronyms *t* and *dt* now mean what most people (who may or may not know python) would expect.

'''

from time import sleep
from datetime import datetime as dateclock, timedelta as deltatime
from pyutils import ftee, worker
from pyutils.structures.units import DeltaTimeUnit
from pyutils.patterns.event import Event
# from pyutils.common.thread import worker
from pyutils.patterns.accessors import Name
from pyutils.utilities.typeutils import *



DC = dateclock
DT = deltatime
DTU = DeltaTimeUnit
instant = deltatime()

def dt_until(time, now=None):
	''' get deltatime from *now* until *time) '''
	dt = time - (now or DC.now())
	return dt

def dtu_until(time, now=None):
	''' get DeltaTimeUnit from *now* until *time) '''
	dt = DeltaTimeUnit(time - (now or DC.now())())
	return dt

def wait_until(time, now=None):
	''' wait until a given *time) from *now*;  returns immediately if past *time*. '''
	dt = time - (now or DC.now())
	dt > instant and sleep(dt.total_seconds())


class OnTime(Event):
	_consume_count = None
	
	def __init__(self, period, handlers=None, *, repeats=True):
		self._periods = [DT(seconds=period), instant]  # [off_dt, on_dt]
		
class OnTimer(Event):
	_consume_count = None
	
	def __init__(self, periods, handlers=None, *, repeats=True):
		self._periods = [DT(seconds=period) for period in periods]
		self._max_wait = DT(seconds=max(.05, min(*[s*.5 for s in map(DT.total_seconds, self._periods) if s > 0] + [10])))
		self.repeats = repeats if repeats in (True, False) else repeats * len(self._periods)
		self.cycles = 0
		self.total_cycles = 0
		self._period_ind = 0
		self._is_on = False
		self._prev_toggle_t = None
		self._next_toggle_t = None
		self._running = False
		super().__init__(handlers)
	def signal(self, from_t=None):  # trigger, stimulate, prompt, cue, affect, enact, induce
		''' delay next_toggle_t by period '''
		# self._next_toggle_t = (time or dateclock.now()) + self.period  # delay next_toggle_t by period
		from_t = from_t or DC.now()
		if self._next_toggle_t and (instant > self.period or from_t >= self._next_toggle_t):
			self._period_ind = (self._period_ind + 1) % len(self._periods)
		self._next_toggle_t = DC.max if instant > self.period else from_t + self.period
		print(f'\nOnTimer.signal();  period={self.period};  next={self._next_toggle_t}')
		# self._next_toggle_t = DC.max if instant > self.period else (from_t or DC.now()) + self.period
	def consume(self, count=None):
		consume_count = min(self.cycles, count or self._consume_count or self.cycles)
		self.cycles = max(0, self.cycles - consume_count)
		print(f'{consume_count} <= consume()')
		return consume_count
	def idle(self):
		now = DC.now()
		ftee(locals(), '{now} {self._next_toggle_t}')
		while self._next_toggle_t >= now:
			wait_until(min(self._next_toggle_t, now+self._max_wait))
			now = DC.now()
		self._prev_toggle_t = self._next_toggle_t
	def start(self):
		''' start timer asyncronously '''
		worker(self.run)
		return self
	def run(self):
		''' run timer blocking '''
		self._running = True
		while self.repeats:
			# set starting state and block for timer period
			self.signal(self._prev_toggle_t)
			self.idle()
			
			# signal timer event to listeners
			self._is_on = not self._is_on
			self.cycles += self._is_on
			self.emit()
			
			# set states for next iteration
			
			self.repeats = self.repeats if self.repeats in (True, False) else self.repeats - 1
			print(f'OnTimer.run();  toggled:{self._is_on and "on" or "off"};  repeats:{self.repeats}')
		self._running = False
		print(f'OnTimer.run();  EXIT')

	# method aliases
	
	# properties
	is_on		= Name._is_on
	is_off		= property(lambda self: not self._is_on)
	period		= property(lambda self: self._periods[self._period_ind])
	t_start_on	= Name._next_toggle_t


'''
## Design Brainstorm

### Potential Naming Schemes

			| backoff	| timeouts
			| starts	| stops
-			| - 		| -
once		| delay		| wait
recur		| throttle	| heartbeat


active		| passive
on_event	| on_elapse

### Intended Usage
1. Basic timer which signals once after a 10s
> on_time('10s')
or
> on_period('10s')

2. Repeat timer which signals after every 10s
> on_timer('10s')

3. Timeout timer which signals if external event was not detected within 10s
> timer = off_time('10s')  # todo
> timer.consume()

4. Heartbeat timer which signals if external event was not detected for 10s
> timer = off_time('10s')  # todo
> timer.reset()

5. Throttle timer which signals on demand but no more than once ever 10s
> timer = on_timer('10s')  # todo
> timer.consume()  # todo

6. Idle timer which signals 10s after the most recent external event
> timer = on_timer('10s')  # todo
> timer.reset()

7. Combination timers which signals at most once every 10s but only after external event has been idle for 500ms
> on_timer('10s') and on_timer('500ms', reset_on=external_event)  # todo

8. Setup and operate timer events
> timer.hook(timer_handler)
> external_event.hook(timer.reset_on)
> timer.start()
>
> if timer.reset():
>	act()
> if timer.consume():
> 	act()

'''
