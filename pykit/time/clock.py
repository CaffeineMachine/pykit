from datetime import datetime, timedelta
from time import sleep

from pyutils.common.colorhighlight import *
from pyutils.patterns.event import *
from pyutils.structures.units import *


__all__ = Strs('min_timestamp_datetime prev_tick next_tick prev_clock_time next_clock_time '
			   'Clock sleep')

min_timestamp_datetime = datetime(1970,1,1)

def prev_clock_time(clock_tdelta:timedelta, from_time:datetime=None, clock_offset:datetime=None):
	''' '''
	# coerce and resolve all inputs as needed
	clock_tdelta = Dt.coerce_timedelta(clock_tdelta)
	clock_offset = clock_offset or min_timestamp_datetime
	if not isinstance(clock_offset, datetime):
		clock_offset = min_timestamp_datetime + Dt.coerce_timedelta(clock_offset)
	from_time = from_time or datetime.now()
	
	# calc previous clock time from timestamp deltas
	clock_timestamp = int(clock_tdelta.total_seconds())
	from_timestamp = (from_time - clock_offset).total_seconds()
	timestamp = (from_timestamp // clock_timestamp) * clock_timestamp  # round timestamp to whole interval
	result = clock_offset + timedelta(seconds=timestamp)
	return result

def next_clock_time(clock_tdelta:timedelta, from_time:datetime=None, clock_offset:datetime=None):
	''' '''
	result = prev_clock_time(clock_tdelta, from_time, clock_offset)
	result += DeltaTime.coerce_timedelta(clock_tdelta)
	return result

prev_tick, next_tick = prev_clock_time, next_clock_time


show_upd = show = None
show_orig = lambda func, *other, : print(f'{func and func.__name__+"():":<20s} {",  ".join(Strs(other))}')
show = lambda func, clock, *other, color=clear_col: print(
	color(f'{func and func.__name__+"():":<20s} {id(clock)},   {",  ".join(Strs(other))}'))
show_upd = show


		
class Clock(Publisher):
	# def __init__(self, interval, offset=None, starting=None, debug=False, pa=None, ka=None):
	def __init__(self, interval, *publ_pa, offset=None, starting=None, debug=False, **publ_ka):
		self.interval = DeltaTimeUnit(interval)
		self.offset = offset and DeltaTimeUnit(offset)
		self.next_elapse = next_clock_time(interval, clock_offset=offset)
		self.prev_elapse = self.next_elapse - self.interval
		self.starting = starting or datetime.now()
		self.elapsed_cycles = 0
		self.total_cycles = 0
		self.debug = debug
		self.debug and print(self.next_elapse)
		# self._handler_tids = None
		super().__init__(*publ_pa, **publ_ka)
	def __format__(self, fmt_spec='', omit=None):
		result = '{}({})'.format(
			self.__class__.__name__,
			', '.join(Strs([self.interval] + (self.offset and [self.offset] or []), to_str="'{}'".format)))
		return result
	def clone(self, copy_state=False, **kws):
		result = self.__class__(self.interval, starting=self.starting, **kws)
		return result
	
	def countdown(self, make_current=False):
		if make_current:
			while self.check_elapse(): pass
		result = self.next_elapse - datetime.now()
		return result
	def check_elapse(self, dateclock=None, *pa, **ka):
		# todo
		dateclock = dateclock or datetime.now()
		if self.next_elapse < dateclock:
			return self.elapse()
	def elapse(self, *pa, **ka):
		self.emit(*pa, **ka)
		# self.notify(*pa, **ka)
		self.next_elapse += self.interval
		return self.next_elapse
	def reset(self): pass
	def __iter__(self): pass
	def update(self):
		''' calculate elapsed_cycles given datetime.now() '''
		with atomic(self, blocking=False) as locked:
			if not locked: return False  # does not matter which thread invokes update; return immediately if locked
			now = datetime.now()
			if now < self.next_elapse:
				return False
			self.debug and show(Clock.update, self, self.next_elapse, color=green)
			
			# calc elapse timers
			elapse_timedelta = now - self.next_elapse
			elapse_count = int(elapse_timedelta / self.interval.base_view) + 1
			self.next_elapse += elapse_count * self.interval  # release lock as soon as possible
			self.prev_elapse = self.next_elapse - self.interval
			
			# calc elapsed_cycles
			self.elapsed_cycles += elapse_count
			self.total_cycles += elapse_count
		return True
	def consume(self, n_cycles=1):
		''' Decrement elapsed_cycles by n_cycles immediately but not below 0. (non-blocking) '''
		self.debug and show(Clock.consume, self)
		self.update()
		consumed = self.elapsed_cycles if n_cycles <= -1 else min(n_cycles, self.elapsed_cycles)
		self.elapsed_cycles -= consumed
		next, start, stop = (self.next_elapse, self.prev_elapse, self.next_elapse, )
		return consumed, self.elapsed_cycles, self.total_cycles, next, start, stop
	def flush(self):
		''' Reduces elapsed_cycles to 0. Same as consume(-1). (non-blocking) '''
		return self.consume(-1)
	def wait(self, n_cycles=1, throttle=.01, log=True):
		''' Wait for duration of next n_cycles to elapse. Preserve total elapsed cycles. (blocking) '''
		consumed, start, release_cycle = 0, self.prev_elapse, self.total_cycles+n_cycles
		self.debug and show_upd and show_upd(Clock.wait, self, start.second, 'ENTER')
		while True:
			self.update()
			if  release_cycle <= self.total_cycles:
				break
			sleep(not self.debug and throttle or .1)
		next, stop = self.next_elapse, self.prev_elapse
		self.debug and show_upd and show_upd(Clock.wait, self, start.second, 'EXIT')
		return consumed, self.elapsed_cycles, self.total_cycles, next, start, stop
	def cycle(self, n_cycles=1):
		''' Consume immediately n_cycles up to available elapsed_cycles.
			Wait for any remaining n_cycles. (conditional blocking)
		'''
		self.debug and show(Clock.cycle, self, datetime.now(), 'A', self.elapsed_cycles)
		consumed_cycles, _, _, _, start, _ = self.consume(n_cycles)
		wait_cycles = n_cycles - consumed_cycles
		self.debug and show(Clock.cycle, self, datetime.now(), 'B', self.elapsed_cycles, consumed_cycles)
		_, _, _, next, _, stop = self.wait(wait_cycles)
		self.elapsed_cycles -= wait_cycles
		self.debug and show(Clock.cycle, self, datetime.now(), 'C', self.elapsed_cycles, wait_cycles)
		return (consumed_cycles + wait_cycles), self.elapsed_cycles, self.total_cycles, next, start, stop
	def cycles(self, n_cycles=-1):
		last_cycle = None
		while True:
			if n_cycles == 0: break
			n_cycles -= 1
			last_cycle = self.cycle()
			update_cycles = yield last_cycle
			if update_cycles is None:
				last_cycle = None
			else:
				n_cycles = update_cycles
				yield last_cycle
			
			
	# @property
	# def handler_tids(self):
	# 	try:
	# 		return self._handler_tids or [handler.tid for handler in self.handlers]
	# 	except:
	# 		raise
	__str__ = __format__
# Clock = ClockCycle

