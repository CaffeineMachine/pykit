''' Collection Defines:
	Provides defines pertaining to primative collections.
	Requires no external dependencies and incurs virtually no overhead.
'''

empty = isempty = lambda seq: len(seq) == 0
nonempty = lambda seq: len(seq) != 0  # [nonempty([0]), any([0])] => [True, False]


# 'k' will be used to signify a key set
dklt_colls = dict, set, list, tuple
dklt_clabels = tuple('d' 'k' 'l' 't')
dklt_coll_map = {*zip(dklt_clabels, dklt_colls)}

klt_colls = (set, list, tuple)
klt_clabels = tuple('k' 'l' 't')
klt_coll_map = {*zip(klt_clabels, klt_colls)}

std_colls, std_clabels = dklt_colls, dklt_clabels
std_non_colls = str, bytes

# class CollWrap:
# 	__slots__ = ['func', 'coll', 'arg_id']
#
# 	def __init__(self, func, coll, arg_id=None):
# 		self.func = func
# 		self.coll = coll
# 		self.arg_id = arg_id
# 	def __call__(self, *pa, **ka):
# 		if isinstance(self.arg_id, str):
# 			ka[self.arg_id] = self.coll
# 		elif isinstance(self.arg_id, int):
# 			pa = list(pa).insert(self.arg_id, self.coll)
# 		elif self.arg_id is not None:
# 			raise ValueError(f'Invalid arg_id:{self.arg_id}. Expected a None, str or int')
#
# 		result = self.func(*pa, **ka)
# 		if self.arg_id is None:
# 			result = self.coll(result)
# 		return result

def gen_coll_wraps(func, colls=std_colls, arg=None, **kdef):
	wraps = []
	for coll in colls:
		if arg is None:
			wrap = lambda *pa, _func=func, _coll=coll, _kd=kdef, **ka: _coll(_func(*pa, **_kd, **ka))
		elif isinstance(arg, str):
			wrap = lambda *pa, _func=func, _coll=coll, _key=arg, _kd=kdef, **ka: _func(*pa, **{arg:_coll}, **_kd, **ka)
		elif isinstance(arg, int):
			wrap = lambda *pa, _func=func, _coll=coll, _pos=arg, _kd=kdef, **ka: _func(*pa[:_pos], _coll, *pa[_pos:], **_kd, **ka)
		else:
			raise ValueError()
		wraps.append(wrap)
	return wraps

def def_coll_wraps(func, fmt:str=None, colls=std_colls, arg=None, labels=std_clabels, safe=True, **kdef):
	names = list(map((fmt or func.__name__).format, labels))
	assert safe and set(names).isdisjoint(globals()), f'formatted name in {names} collides with namespace'
	wraps = gen_coll_wraps(func, colls, arg, **kdef)
	named_wraps = tuple(zip(names, wraps))
	
	globals().update(dict(named_wraps))
	return named_wraps


def iscoll(obj, coll_types=std_colls, noncoll_types=std_non_colls, allow_iter=False):
	''' test if obj is a collection (a non-str sequence type) '''
	coll_methods = ['__iter__'] if allow_iter else ['__len__', '__getitem__']
	result = (
		not isinstance(obj, noncoll_types) and
		(isinstance(obj, coll_types) or all([hasattr(obj, name) for name in coll_methods]))
	)
	return result

def coll(obj, cast=tuple, empties=(), empty_types=(), *args, **kargs):
	''' Always return a collection by 1st coercing *obj* to collection items and 2nd casting the items if needed
		1. check that *obj* qualifies as collection or collection-iterable. otherwise coerce it as *coll_items*=[*obj*]
		2. check that *coll_items* qualifies as collection cast type. otherwise cast it as *cast*(*coll_items*)
		where collection is a non-str sequence type (ie: list, tuple, dict, set, ...).
	'''
	if not isinstance(obj, cast):
		if obj in empties or isinstance(obj, empty_types):
			obj = cast(())
		elif iscoll(obj, *args, **kargs):
			obj = cast(obj)
		else:
			obj = cast((obj,))
	return obj
kcoll, lcoll, _ = gen_coll_wraps(coll, colls=klt_colls, arg=1)
kyieldcoll, lyieldcoll, tyieldcoll = gen_coll_wraps(coll, colls=klt_colls, arg=1, allow_iter=True)
# def kcoll(obj, *args, **kargs):			return coll(obj, set, *args, **kargs)
# def lcoll(obj, *args, **kargs):			return coll(obj, list, *args, **kargs)
# def kyieldcoll(obj, *args, **kargs):	return coll(obj, set, *args, allow_iter=True, **kargs)
# def tyieldcoll(obj, *args, **kargs):	return coll(obj, tuple, *args, allow_iter=True, **kargs)
# def lyieldcoll(obj, *args, **kargs):	return coll(obj, list, *args, allow_iter=True, **kargs)
tcoll = coll

