''' Numeric Defines:
	Defines numeric constants.
	Requires no external dependencies and incurs virtually no overhead.
'''

# int size constants; useful even with python bigint support
_sizes = [8, 16, 24, 32, 64]
max_uint8, max_uint16, max_uint24, max_uint32, max_uint64	= (int('1'*exp, 2) for exp in _sizes)
max_int8, max_int16, max_int24, max_int32, max_int64		= (int('1'*(exp-1), 2) for exp in _sizes)
min_int8, min_int16, min_int24, min_int32, min_int64		= (~int('1'*(exp-1), 2) for exp in _sizes)

pinf = inf = float('inf')
ninf = -float('inf')
nan = float('nan')