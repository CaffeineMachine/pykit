''' Type Defines:
	Provides type related defines.
	Requires no external dependencies and incurs no overhead.
'''


isstr		= lambda obj: isinstance(obj, str)
isbytes		= lambda obj: isinstance(obj, bytes)
isbool		= lambda obj: isinstance(obj, bool)
isint		= lambda obj: isinstance(obj, int)
isfloat		= lambda obj: isinstance(obj, float)
iscomplex	= lambda obj: isinstance(obj, complex)
isdict		= lambda obj: isinstance(obj, dict)
islist		= lambda obj: isinstance(obj, list)
istuple		= lambda obj: isinstance(obj, tuple)
isset		= lambda obj: isinstance(obj, set)
isnone		= lambda obj: isinstance(obj, type(None))

isntstr		= lambda obj: not isinstance(obj, str)
isntbytes	= lambda obj: not isinstance(obj, bytes)
isntbool	= lambda obj: not isinstance(obj, bool)
isntint		= lambda obj: not isinstance(obj, int)
isntfloat	= lambda obj: not isinstance(obj, float)
isntcomplex	= lambda obj: not isinstance(obj, complex)
isntdict	= lambda obj: not isinstance(obj, dict)
isntlist	= lambda obj: not isinstance(obj, list)
isnttuple	= lambda obj: not isinstance(obj, tuple)
isntset		= lambda obj: not isinstance(obj, set)
isntnone	= lambda obj: not isinstance(obj, type(None))


def qualname(cls):
	module = cls.__module__
	if module is None or module == str.__class__.__module__:
		return cls.__name__
	return module + '.' + cls.__name__

def objqualname(obj):
	module = obj.__class__.__module__
	if module is None or module == str.__class__.__module__:
		return obj.__class__.__name__
	return module + '.' + obj.__class__.__name__


def clsof(cls:type, classes:type, cmp_names=False)->bool:
	''' Test if *cls* matches any in *classes* type tree. Compatible with builtin issubclass().
		:param cmp_names: When True compare classes using qualified names. Useful when tests need to reimport.
	'''
	# classes = [classes] if type(classes) is type else classes
	classes = [classes] if hasattr(classes, '__instancecheck__') else classes
	# classes = [classes] if type(classes) is type or not hasattr(classes, '__iter__') else classes
	cls_components = (cls,) + cls.__mro__
	if cmp_names:
		classes = list(map(qualname, classes))
		cls_components = list(map(qualname, cls_components))

	for cls_item in classes:
		if cls_item in cls_components:
			return True
	return False

def clslike(cls:type, classes:type)->bool:
	''' Test if *cls* name matches any name in *classes* type tree. Similar to builtin issubclass().'''
	return clsof(cls, classes, cmp_names=True)

def typeof(obj:object, classes:type, cmp_names=False)->bool:
	''' Test if *obj* type matches any in *classes* type tree. Compatible with builtin isinstance().
		:param cmp_names: When True compare classes using qualified names. Useful when tests need to reimport.
	'''
	return clsof(obj.__class__, classes, cmp_names=False)

def typelike(obj:object, classes:type)->bool:
	''' Test if *obj* type name matches any name in *classes* type tree. Similar to builtin isinstance(). '''
	return clsof(obj.__class__, classes, cmp_names=True)


dclsattrs			= lambda cls: cls.__annotations__.keys()
dclsannos			= lambda cls: cls.__annotations__.values()
dclsannomap			= lambda cls: cls.__annotations__


dataattrs		= lambda data: data.__class__.__annotations__.keys()
datambrs		= lambda data: (getattr(data, name) for name in data.__class__.__annotations__.keys())
datavars		= lambda data: ((name,getattr(data, name)) for name in data.__class__.__annotations__.keys())
dattrs, dmbrs, dvars = dataattrs, datambrs, datavars
dnames = dattrs

