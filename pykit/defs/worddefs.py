''' Word Defines:
	Defines additional programming keywords.
	Requires no external dependencies and incurs no overhead.
'''

class NamedConst(str): pass

# class NamedNil(str):
class NamedNil:
	def __init__(self, name):
		self.name = name
	def __str__(self): return self.name
	def __repr__(self): return f'{self.__class__.__name__}({self.name})'
	def fillin(self, arg, fill=None):
		result = fill if arg == self else arg
		return result
	def filler(self, arg, factory=None):
		result = factory() if arg == self else arg
		return result
	def __eq__(self, other):
		return self.__class__ == other.__class__
	def __hash__(self):
		return hash(self.__class__)
	
	''' Negation of all possible instance representations '''
	def __bool__(self):
		return False
	# def __iter__(self): raise AttributeError('__iter__')
	# def __getitem__(self, ind):
	# 	raise TypeError(f'Not allowed for {self.__class__.__name__} type.')
	# def __setitem__(self, ind):
	# 	raise TypeError(f'Not allowed for {self.__class__.__name__} type.')
	# def __getattribute__(self, name):
	# 	if name == '__iter__':
	# 		raise AttributeError(name)
	# 	return super().__getattribute__(name)
	# def __dir__(self):
	# 	return sorted( (set(dir(self.__class__))|set(self.__dict__.keys())) - {'__iter__'})
	
arg_missing = NamedNil('arg_missing')
not_given = NamedNil('not_given')
nothing = NamedNil('nothing')


# todo: RELOCATE functions below
asis		= lambda obj: obj
relation	= lambda obj, name: (name, getattr(obj, name))
relationarg	= lambda obj_name_arg: (obj_name_arg[0], getattr(*obj_name_arg))


def get__all__except(names, excludes=''):
	if isinstance(excludes, str):
		excludes = excludes.split(' ')
	if isinstance(names, dict):
		names = names.keys()
	result = [name for name in names if name not in excludes and not name.startswith('_')]
	return result

