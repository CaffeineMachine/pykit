# coding=utf-8
''' Character Set Defines:
	Provides multiple character sets and sequences.
	Requires no external dependencies and incurs no overhead.
'''

alpha_lower_26 	= 'abcdefghijklmnopqrstuvwxyz'
alpha_upper_26 	= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
number_16		= u' ▘▝▀▖▌▞▛▗▚▐▜▄▙▟█'
quad_16			= u' ▘▝▀▖▌▞▛▗▚▐▜▄▙▟█'
fill_e_w_8		= u'▏▎▍▌▋▊▉█'
fill_s_n_8		= u'▁▂▃▄▅▆▇█'
gradients_5 	= u' ░▒▓█'
monogram_3 		= u'⚊⚋𝌀'
digram_9		= u'⚌⚍⚎⚏𝌁𝌂𝌃𝌄𝌅'
trigram_8		= u'☰☱☲☳☴☵☶☷'
hexagram_64		= (
	u'䷀䷁䷂䷃䷄䷅䷆䷇䷈䷉䷊䷋䷌䷍䷎䷏'
	u'䷐䷑䷒䷓䷔䷕䷖䷗䷘䷙䷚䷛䷜䷝䷞䷟'
	u'䷠䷡䷢䷣䷤䷥䷦䷧䷨䷩䷪䷫䷬䷭䷮䷯'
	u'䷰䷱䷲䷳䷴䷵䷶䷷䷸䷹䷺䷻䷼䷽䷾䷿'  )
tetragram_81 = (
	u'𝌆𝌇𝌈𝌉𝌊𝌋𝌌𝌍𝌎𝌏𝌐𝌑𝌒𝌓𝌔𝌕'
	u'𝌖𝌗𝌘𝌙𝌚𝌛𝌜𝌝𝌞𝌟𝌠𝌡𝌢𝌣𝌤𝌥'
	u'𝌦𝌧𝌨𝌩𝌪𝌫𝌬𝌭𝌮𝌯𝌰𝌱𝌲𝌳𝌴𝌵'
	u'𝌶𝌷𝌸𝌹𝌺𝌻𝌼𝌽𝌾𝌿𝍀𝍁𝍂𝍃𝍄𝍅'
	u'𝍆𝍇𝍈𝍉𝍊𝍋𝍌𝍍𝍎𝍏𝍐𝍑𝍒𝍓𝍔𝍕𝍖'  )


