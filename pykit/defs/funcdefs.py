''' Function Defines:
	Provides fundamental function definitions.
	Requires no external dependencies and incurs no overhead.
'''

''' Basic Operations
'''

asis			= lambda arg: arg
aspa			= lambda *pa, **ka: pa
aska			= lambda *pa, **ka: ka
relation		= lambda obj, name: (name, getattr(obj, name))
relationarg		= lambda obj_name_arg: (obj_name_arg[0], getattr(*obj_name_arg))

def throw(exc):
	raise exc


''' Modify Function Operations
'''

neg		= lambda func: lambda *pa, **ka: not bool(func(*pa, **ka))	# not recommended for use in tight loops
star	= lambda func: lambda pa, ka=None: func(*pa, **ka or {})	# not recommended for use in tight loops
stars	= lambda func: lambda ka, pa=None: func(*pa or (), **ka)	# not recommended for use in tight loops

# aliases
aspargs = aspa
askargs = aska
unpack = star

