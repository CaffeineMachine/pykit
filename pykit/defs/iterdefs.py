''' Iterable Defines:
	Defines operations related to sequencing and iteration.
	Requires no external dependencies and incurs no overhead.
'''

key_ind, value_ind, item_ind = 0, 1, slice(0, 2)
item_key	= lambda item: item[key_ind]
item_value	= lambda item: item[value_ind]
item_item	= lambda item: item
not_all		= lambda items: not all(items)
not_any		= lambda items: not any(items)

def first(seq, *default):
	result = next(iter(seq), *default)
	return result

def last(seq, *default):
	''' Get last item of a sequence without iteration.
		Note: will depleate
	'''
	if hasattr(seq, '__reversed__'):
		result = next(iter(reversed(seq)), *default)
	elif default and len(seq) == 0:
		result = next(iter(default))
	else:
		result = seq[-1]
	return result

def ilast(seq, *default):
	''' Iterate to and return last item.
		Note: will depletes iterators. Always O(n).
	'''
	*front, result = *default, *seq
	return result

def canhash(obj):
	result = bool(getattr(obj, '__hash__', None))
	if result:
		try:
			hash(obj)
		except TypeError:
			result = False
	return result

def hashable(obj, default=None):
	result = obj if canhash(obj) else default
	return result

	
class Iterates:
	''' Has iterate as default __call__ behavior. References an iterable. '''
	__slots__ = ['_itr']
	
	def __init__(self, itr):
		self._itr = itr
	def __iter__(self):
		yield from self._itr
	
	# method aliases
	__call__ = __iter__
Iter = Iterates
